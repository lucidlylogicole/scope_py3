import os, sys, codecs
import commonmark

def generate(file=None,text=None,style='',custom=1):
    # Open File
    if file != None:
        f = codecs.open(file,'r','utf-8')
        rawhtml = f.read()
        f.close()
    else:
        rawhtml=text
    # Parse
    html = ''
    txtlines = rawhtml.replace('\r\n','\n').replace('\r','\n').split('\n')
    for t in txtlines:
            # Ignore comments
            html += t+'\n'

    # Get Markdown (Common Mark)
    parser = commonmark.Parser()
    renderer = commonmark.HtmlRenderer()
    md = renderer.render(parser.parse(html))
    
    mhtml=''
    
    # Custom Markdown modifications
    if custom:
        md = md.replace('<li>[ ]','<li class="checkbox"><input type="checkbox" disabled="disabled">')
        md = md.replace('<li>[]','<li class="checkbox"><input type="checkbox" disabled="disabled">')
        md = md.replace('<li>[x]','<li class="checkbox checked"><input type="checkbox" checked="checked" disabled="disabled">')
        md = md.replace('<li>[-]','<li class="cancelled"><input type="checkbox" checked="checked" disabled="disabled">')
        md = md.replace('<li>[f]','<li class="future"><input type="checkbox" disabled="disabled"> FUTURE')
        
        # Custom Style
        style = '''body {font-family:ubuntu,calibri,helvetica;font-size:1.1rem;font-weight:100;color:rgb(60,60,60);margin:0;}
            h1,h2,h3,h4 {color:rgb(40,40,40);font-weight:200;}
            button, input[type=button],input[type=submit],input[type=reset],select {background:rgb(230,230,230);background: linear-gradient(to bottom, rgb(240,240,240) 0%,rgb(230,230,23)100%);border:1px solid rgb(210,210,210);padding:6px 12px;box-sizing:border-box;border-radius:4px;font-size:1.1rem;line-height:1.1rem;cursor:pointer;margin:2px 0p;color:rgb(40,40,40);}
            button:hover,input[type=button]:hover,input[type=submit]:hover,input[type=reset]:hover {background:rgb(220,220,220);border:1px solid rgb(200,200,200);}
            select {padding:4px 8px;color:rgb(40,40,40);}
            input:not([type=checkbox]):not([type=radio]):not([type=button]), textarea {margin:2px 0px;padding:4px 8px;box-sizing:border-box;font-size:1.1rem;border:1px solid rg(210,210,210);border-radius:4px;}
            a, a:visited {color:rgb(50,117,190);text-decoration:none;}
            a:hover {color:rgb(29,79,134);}
            b {font-weight:500;}
            hr {border:0px;border-top: 1px solid rgb(180,180,180);}
            pre, code  {background:rgb(240,240,240);border-radius:4px;border:1px solid rgb(230,230,230);padding:8px;box-sizing:border-box;line-height:1.1rem;color:rgb(80,80,80);}
            pre {overflow-x:auto;}
            code {padding:0px 4px;}
            pre code {border:0;padding:0px;background:transparent;}
            .paper {background:white;box-sizing:border-box;padding:16px;max-width:960px;margin:16px auto;}
            img{max-width:100%;}
        '''+style
        
    # Add Style
    if style != '':
        mhtml += '<style>'+style+'</style>'
    
    mhtml += '<div class="paper">'+ md + '</div>'

    return mhtml

def compile_md(md_file,html_file=None,**kargs):
    if html_file == None:
        html_file = md_file.split('.')[0]+'.html'
    
    mhtml = generate(md_file,**kargs)
    with open(html_file,'w') as f:
        f.write(mhtml)

if __name__ == '__main__':
##    print(sys.argv)
    if len(sys.argv) > 1:
        if sys.argv[1] == '-c':
            md_file = sys.argv[2]
            html_file = None
            if len(sys.argv) > 3:
                html_file = sys.argv[3]
            compile_md(md_file,html_file)
        else:
            generate(sys.argv[1])

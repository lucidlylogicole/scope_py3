import socket

# Check for available ports

def get(port=None):
    if port != None:
        # Return port if available
        if not check(port):
            return port
    
    s = socket.socket()
    s.bind(('localhost', 0))            # Bind to a free port provided by the host.
    port = s.getsockname()[1]  # Return the port number assigned.
    s.close()
    return port
    
def check(port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex(('127.0.0.1',port))
    return result == 0
import os
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw

class Plugin(object):
    title = 'Notepad'
    location = 'app' # left, bottom, right, app
    widget = None  # The widget for the plugin (set at getWidget)
    
    def __init__(self,parent=None):
        self.parent = parent
    
    def load(self):
        self.btn = self.parent.addLeftBarButton(qtg.QIcon('icon.png'),tooltip=self.title)
        self.btn.clicked.connect(self.addNotepadWidget)
        # store widget with button (with addLeftBarButton.  if widget doesn't exist, it calls the getwidget)
        
    def loadWidget(self):
        from . import notepad
        curdir = os.path.abspath('.')
        os.chdir(os.path.dirname(__file__))
        self.widget = notepad.Notepad(self.parent)
        os.chdir(curdir)

        self.widget.pinned=1
        return self.widget
    
    def addNotepadWidget(self):
        if self.widget == None:
            self.loadWidget()
            ti = self.parent.ui.tab_right.addTab(self.widget,self.btn.icon(),'Notepad')
            self.parent.ui.tab_right.setTabToolTip(ti,'Notepad')
        else:
            ti = self.parent.ui.tab_right.indexOf(self.widget)
        self.parent.ui.tab_right.setCurrentIndex(ti)
        self.parent.toggleRightPlugin(show=1)

##    def toggle(self):
##        self.parent.changeTab(self.widget.id)

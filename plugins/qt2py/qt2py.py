from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw
from .qt2py_ui import Ui_Form
import os, datetime, subprocess, sys
import webbrowser
    
class Qt2Py(qtw.QWidget):
    def __init__(self,parent=None):
        qtw.QWidget.__init__(self,parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ide = parent
    
        self.ui.b_open.clicked.connect(self.open)
        self.ui.b_convert.clicked.connect(self.convert)
        self.ui.b_qt_designer.clicked.connect(self.open_qt_designer)
        self.ui.b_help.clicked.connect(self.search_help)
        
        # Load Settings
        self.settingsD = self.ide.loadPluginSettings('qt2py')
        
        if self.settingsD['default_version'] == 5:
            self.ui.cb_vers.setCurrentIndex(1)
        
    def open(self):
        
        # Get current file directory
        try:
            cdir = os.path.abspath(os.path.dirname(self.ide.currentEditor().filename))
        except:
            cdir = ''
        
        filename = qtw.QFileDialog.getOpenFileName(self,"Select File",cdir,"UI (*.ui)")[0]
        if filename!='':
            self.ui.le_path.setText(filename)
    
    def convert(self):
        filename = str(self.ui.le_path.text())
        vers = str(self.ui.cb_vers.currentText()).lower()
        if os.path.exists(filename):
            # Convert .ui to .py
            pth = os.path.dirname(str(filename))
            bname = os.path.basename(str(filename))
            curdir = os.path.abspath('.')
            os.chdir(pth)
##            if vers == 'Qt4':
##                os.system("pyuic4 "+bname+" > " +bname[:len(bname)-3] + "_ui.py")
##            else:
##                os.system("pyuic5 "+bname+" > " +bname[:len(bname)-3] + "_ui.py")
            os.system(self.settingsD[vers+'_paths']['pyuic']+" "+bname+" > " +bname[:len(bname)-3] + "_ui.py")
            os.chdir(curdir)
            self.ui.l_result.setText('Converted '+filename+' on '+datetime.datetime.now().ctime())
    
    def open_qt_designer(self):
        
        vers = str(self.ui.cb_vers.currentText()).lower()
        
        # Get current file directory
        try:
            cdir = os.path.abspath(os.path.dirname(self.ide.currentEditor().filename))
        except:
            cdir = None
        
        try:
            subprocess.Popen(self.settingsD[vers+'_paths']['designer'], stdout=subprocess.PIPE, shell=0,cwd=cdir)
##            if os.name == 'posix':
##                if vers == 'Qt4':
##                    subprocess.Popen('/usr/bin/designer-qt4', stdout=subprocess.PIPE, shell=0,cwd=cdir)
##                else:
##                    subprocess.Popen('/usr/lib/x86_64-linux-gnu/qt5/bin/designer', stdout=subprocess.PIPE, shell=0,cwd=cdir)
##            elif os.name == 'nt':
##    ##            pypth = os.path.dirname(sys.executable)
##                pypth = r'C:\python27'
##                
##                subprocess.Popen(pypth+'/Lib/site-packages/PyQt4/designer.exe', stdout=subprocess.PIPE, shell=0,cwd=cdir)
        except:
            qtw.QMessageBox.warning(self,'Qt Designer?','Scope could not find Qt Designer. Check to make sure it is installed')
    
    def qtHelp(self):
        i=self.ide.ui.sw_bottom.indexOf(self)
        self.ide.ui.tabbar_bottom.setCurrentIndex(i)
        self.ui.le_help.setFocus()
        self.ui.le_help.selectAll()
    
    def search_help(self):
        txt = str(self.ui.le_help.text()).lower()
        vers = str(self.ui.cb_vers.currentText()).lower()
        
        if not txt.startswith('q'):
            txt = 'q' +txt
        
##        if vers == 'Qt4':
##            lnk = 'http://doc.qt.io/qt-4.8/%s.html' %txt
##        else:
##            lnk = 'http://doc.qt.io/qt-5.6/%s.html' %txt
        
        lnk = self.settingsD[vers+'_paths']['help'] %txt
        webbrowser.open(lnk)
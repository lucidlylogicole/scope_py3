# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qt2py.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(559, 113)
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setContentsMargins(8, 4, 8, 0)
        self.gridLayout.setSpacing(4)
        self.gridLayout.setObjectName("gridLayout")
        self.l_result = QtWidgets.QLabel(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.l_result.sizePolicy().hasHeightForWidth())
        self.l_result.setSizePolicy(sizePolicy)
        self.l_result.setText("")
        self.l_result.setObjectName("l_result")
        self.gridLayout.addWidget(self.l_result, 2, 1, 1, 1)
        self.frame_3 = QtWidgets.QFrame(Form)
        self.frame_3.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.frame_3)
        self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_4.setSpacing(4)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.b_qt_designer = QtWidgets.QPushButton(self.frame_3)
        self.b_qt_designer.setObjectName("b_qt_designer")
        self.gridLayout_4.addWidget(self.b_qt_designer, 0, 3, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(26, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem, 0, 4, 1, 1)
        self.frame = QtWidgets.QFrame(self.frame_3)
        self.frame.setMinimumSize(QtCore.QSize(150, 0))
        self.frame.setMaximumSize(QtCore.QSize(250, 16777215))
        self.frame.setStyleSheet("QFrame#frame {\n"
"    background:rgb(30,30,30);\n"
"border-radius:4px;\n"
"padding-left:6px;\n"
"}")
        self.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setContentsMargins(1, 1, 1, 1)
        self.gridLayout_2.setSpacing(1)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.le_help = QtWidgets.QLineEdit(self.frame)
        self.le_help.setStyleSheet("QLineEdit {\n"
"background:transparent;\n"
"color:white;\n"
"border:0px;\n"
"}")
        self.le_help.setObjectName("le_help")
        self.gridLayout_2.addWidget(self.le_help, 0, 1, 1, 1)
        self.b_help = QtWidgets.QPushButton(self.frame)
        self.b_help.setMaximumSize(QtCore.QSize(26, 16777215))
        self.b_help.setStyleSheet("QPushButton {\n"
"background-color:rgba(0,0,0,0);\n"
"background:transparent;\n"
"border:0px;\n"
"}")
        self.b_help.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../../style/img/search.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_help.setIcon(icon)
        self.b_help.setObjectName("b_help")
        self.gridLayout_2.addWidget(self.b_help, 0, 2, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.frame)
        self.label_3.setStyleSheet("color:white;")
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 0, 0, 1, 1)
        self.gridLayout_4.addWidget(self.frame, 0, 5, 1, 1)
        self.label = QtWidgets.QLabel(self.frame_3)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.gridLayout_4.addWidget(self.label, 0, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem1, 0, 2, 1, 1)
        self.cb_vers = QtWidgets.QComboBox(self.frame_3)
        self.cb_vers.setObjectName("cb_vers")
        self.cb_vers.addItem("")
        self.cb_vers.addItem("")
        self.gridLayout_4.addWidget(self.cb_vers, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.frame_3, 0, 0, 1, 2)
        spacerItem2 = QtWidgets.QSpacerItem(2, 2, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem2, 3, 1, 1, 1)
        self.b_convert = QtWidgets.QPushButton(Form)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.b_convert.setFont(font)
        self.b_convert.setObjectName("b_convert")
        self.gridLayout.addWidget(self.b_convert, 2, 0, 1, 1)
        self.frame_2 = QtWidgets.QFrame(Form)
        self.frame_2.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.frame_2)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setSpacing(4)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_2 = QtWidgets.QLabel(self.frame_2)
        self.label_2.setObjectName("label_2")
        self.gridLayout_3.addWidget(self.label_2, 0, 0, 1, 1)
        self.b_open = QtWidgets.QPushButton(self.frame_2)
        self.b_open.setMaximumSize(QtCore.QSize(26, 16777215))
        self.b_open.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("../../style/img/file_open.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_open.setIcon(icon1)
        self.b_open.setObjectName("b_open")
        self.gridLayout_3.addWidget(self.b_open, 0, 1, 1, 1)
        self.le_path = QtWidgets.QLineEdit(self.frame_2)
        self.le_path.setObjectName("le_path")
        self.gridLayout_3.addWidget(self.le_path, 0, 2, 1, 1)
        self.gridLayout.addWidget(self.frame_2, 1, 0, 1, 2)

        self.retranslateUi(Form)
        self.le_help.returnPressed.connect(self.b_help.click)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.b_qt_designer.setToolTip(_translate("Form", "Launch Qt Designer"))
        self.b_qt_designer.setText(_translate("Form", "Qt Designer"))
        self.le_help.setPlaceholderText(_translate("Form", "search Qt Help"))
        self.label_3.setText(_translate("Form", "Q"))
        self.label.setText(_translate("Form", "UI to Python Converter"))
        self.cb_vers.setItemText(0, _translate("Form", "Qt4"))
        self.cb_vers.setItemText(1, _translate("Form", "Qt5"))
        self.b_convert.setText(_translate("Form", "Convert"))
        self.label_2.setToolTip(_translate("Form", "The .ui file generated by Qt Designer"))
        self.label_2.setText(_translate("Form", "Select .ui  File:"))


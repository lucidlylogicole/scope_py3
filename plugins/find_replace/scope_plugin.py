from . import find_replace
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw

class Plugin(object):
    title = 'Find / Replace'
    location = 'bottom'
    widget = None  # The widget for the plugin (set at getWidget)
    
    def __init__(self,parent=None):
        self.parent = parent
    
    def load(self):
        '''Called when loading the plugin'''
        
    def loadWidget(self):
        self.widget = find_replace.Find_Replace(self.parent)
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_R,self.parent,self.widget.replaceFocus) # Replace
        
        return self.widget
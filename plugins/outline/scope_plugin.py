from . import outline
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw

class Plugin(object):
    title = 'Outline'
    location = 'left'
    widget = None  # The widget for the plugin (set at getWidget)
    
    def __init__(self,parent=None):
        self.parent = parent
    
    def load(self):
        '''Called when loading the plugin'''
        
    def loadWidget(self):
        self.widget = outline.Outline(self.parent)
        
##        self.parent.Events.fileOpened.connect(self.widget.addOutline)
        self.parent.Events.editorAdded.connect(self.widget.addOutline)
        self.parent.Events.editorTabChanged.connect(self.widget.editorTabChanged)
        self.parent.Events.editorTabClosed.connect(self.widget.editorTabClosed)
##        self.parent.Events.fileOpened.connect(self.widget.updateOutlineToggle)
        
        qtw.QShortcut(qtc.Qt.ALT+qtc.Qt.Key_O,self.parent,self.widget.togglePlugin) 
        
##        # Add Button
##        self.icon = qtg.QIcon(self.parent.pluginPath+'outline/icon.png')
##        self.btn = self.parent.addLeftBarButton(icon,tooltip=self.title,loc='leftab')
##        self.btn.clicked.connect(self.widget.updateOutlineFocus)
##        self.btn.setCheckable(1)
##        self.btn.setAutoExclusive(1)
        return self.widget
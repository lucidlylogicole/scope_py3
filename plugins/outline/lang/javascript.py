import re

def analyzeLine(txtlines):
    outline = []
    lcnt = -1
    for line in txtlines:
        lcnt += 1
        typ = None
        itmText = None
    
        spc = (len(line) -len(line.lstrip()))*' '
        tls = line.lstrip()
        
        if tls.startswith('function ') :
            itmText =tls[9:].rstrip()
            if itmText.endswith('{'): itmText = itmText[:-1]
            typ = 'function'
        elif tls.startswith('async function '):
            itmText =tls[15:].rstrip()
            if itmText.endswith('{'): itmText = itmText[:-1]
            typ = 'function'
        elif tls.startswith('export function '):
            itmText =tls[16:].rstrip()
            if itmText.endswith('{'): itmText = itmText[:-1]
            typ = 'function'
        elif tls.startswith('constructor('):
            itmText =tls.rstrip()
            if itmText.endswith('{'): itmText = itmText[:-1]
            typ = 'function'
        elif tls.startswith('class '):
            itmText =tls[6:-1]
            typ = 'object'
        elif tls.startswith('//---'):
            if tls.startswith('//---f:'):
                typ = 'function'
                itmText = txtlines[lcnt+1]
                if itmText.endswith('{'): itmText = itmText[:-1]
                itmText = itmText.strip()
            elif tls.startswith('//---o:'):
                typ = 'object'
                itmText = txtlines[lcnt+1].split('=')[0].strip()
                if itmText.endswith('{'): itmText = itmText[:-1]
                if itmText.startswith('let ') or itmText.startswith('var '):
                    itmText = itmText[4:]
                elif itmText.startswith('const '):
                    itmText = itmText[6:]
                
                itmText = itmText.strip()
            else:
                itmText =tls[5:]
                typ = 'heading'
        elif ': function' in tls or ':function' in tls:
            typ = 'function'
            itmText=tls.split(':')[0]+tls.split('function')[-1]
            if itmText.endswith('{'): itmText = itmText[:-1]
        elif tls.startswith('static '):
            typ = 'function'
            itmText =tls[7:].split('=')[0].rstrip()
            if itmText.endswith('{'): itmText = itmText[:-1]
        
        if itmText != None:
            outline.append([spc+itmText,typ,lcnt])
    
    return outline
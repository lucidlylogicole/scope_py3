# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'outline_tree.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Outline_Tree(object):
    def setupUi(self, Outline_Tree):
        Outline_Tree.setObjectName("Outline_Tree")
        Outline_Tree.resize(196, 308)
        self.gridLayout = QtWidgets.QGridLayout(Outline_Tree)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.l_title = QtWidgets.QLabel(Outline_Tree)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.l_title.setFont(font)
        self.l_title.setStyleSheet("    background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(48, 85, 100, 255), stop:0.21267 rgba(61, 107, 127, 255), stop:0.831818 rgba(72, 127, 150, 255), stop:1 rgba(104, 166, 175, 255));\n"
"color:white;\n"
"border:0px;\n"
"border-bottom:1px solid gray;\n"
"padding:2px;\n"
"border-top-left-radius:3px;\n"
"border-top-right-radius:3px;")
        self.l_title.setText("")
        self.l_title.setWordWrap(True)
        self.l_title.setObjectName("l_title")
        self.gridLayout.addWidget(self.l_title, 0, 0, 1, 1)
        self.tr_outline = QtWidgets.QTreeWidget(Outline_Tree)
        self.tr_outline.setStyleSheet("QTreeWidget {\n"
"border-bottom-left-radius:5px;\n"
"border-bottom-right-radius:5px;\n"
"show-decoration-selected: 0;\n"
"}")
        self.tr_outline.setRootIsDecorated(False)
        self.tr_outline.setObjectName("tr_outline")
        self.tr_outline.headerItem().setText(0, "1")
        self.tr_outline.header().setVisible(False)
        self.gridLayout.addWidget(self.tr_outline, 2, 0, 1, 1)
        self.fr_find = QtWidgets.QFrame(Outline_Tree)
        self.fr_find.setStyleSheet("QFrame#fr_find{\n"
" background:transparent;\n"
"color:white;\n"
"border:0px;\n"
"margin:2px;\n"
"border-radius:4px;\n"
"background:rgb(40,40,40);\n"
"padding:2px 4px;\n"
"}")
        self.fr_find.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.fr_find.setFrameShadow(QtWidgets.QFrame.Raised)
        self.fr_find.setObjectName("fr_find")
        self.gridLayout_9 = QtWidgets.QGridLayout(self.fr_find)
        self.gridLayout_9.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_9.setSpacing(0)
        self.gridLayout_9.setObjectName("gridLayout_9")
        self.le_find = QtWidgets.QLineEdit(self.fr_find)
        self.le_find.setMinimumSize(QtCore.QSize(150, 24))
        self.le_find.setStyleSheet("background:transparent;\n"
"border:0px;\n"
"color:white;")
        self.le_find.setText("")
        self.le_find.setObjectName("le_find")
        self.gridLayout_9.addWidget(self.le_find, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.fr_find, 3, 0, 1, 1)

        self.retranslateUi(Outline_Tree)
        QtCore.QMetaObject.connectSlotsByName(Outline_Tree)

    def retranslateUi(self, Outline_Tree):
        _translate = QtCore.QCoreApplication.translate
        Outline_Tree.setWindowTitle(_translate("Outline_Tree", "Form"))
        self.le_find.setToolTip(_translate("Outline_Tree", "search the outline"))
        self.le_find.setPlaceholderText(_translate("Outline_Tree", "find"))


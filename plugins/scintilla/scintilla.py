from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw, Qsci
from .scintilla_ui import Ui_Form
import os,sys, time
from .scintilla_style import styleD, styleLightD # scintilla style

try:
    import jedi
    USE_JEDI = 1
except:
    USE_JEDI = 0

##lexD = {'Python':Qsci.QsciLexerPython(),
##    'JavaScript':Qsci.QsciLexerJavaScript(),
##    'HTML':Qsci.QsciLexerHTML(),
##    'CSS':Qsci.QsciLexerCSS(),
##    'XML':Qsci.QsciLexerXML(),
##    'UI':Qsci.QsciLexerXML(),
##    'YAML':Qsci.QsciLexerYAML(),
##    'Bash':Qsci.QsciLexerBash(),
##    'Batch':Qsci.QsciLexerBatch(),
##    'SQL':Qsci.QsciLexerSQL(),
##    }

commentD = {
    'python':'##',
    'javascript':'//',
    'yaml':'##',
    'perl':'##',
    'ruby':'##',
    'php':'#',
    'r':'##',
    'cpp':'//',
    'csharp':'//',
    'java':'//',
    'd':'//',
}

##def addEditor(parent,lang,filename):
##    lex = None
##    if lang in lexD:
##        lex = lexD[lang]
##    editor = Sci(parent,lex)
##    
##    return editor

class PyLexer(Qsci.QsciLexerPython):
    def keywords(self, index):
        '''Custom Python keywords'''
        keywords = Qsci.QsciLexerPython.keywords(self, index) or ''
        # identifier keywords
        if index == 2:
            return 'True False ' + keywords
        
        return keywords

class Events(qtc.QObject):
    editorChanged = qtc.pyqtSignal(qtw.QWidget)
    visibleLinesChanged = qtc.pyqtSignal(qtw.QWidget,tuple)
##    editingFinished = qtc.pyqtSignal(qtw.QWidget)

class Sci(qtw.QWidget):
    def __init__(self,parent,lex,lang=None):
        qtw.QWidget.__init__(self,parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ui.te_sci.ARROW_MARKER_NUM = 8
        self.ui.te_sci.setUtf8(True)
        self.ide = parent
        self.okedit = 1
        self.zoom = 0
        
        self.lex = lex
        
        self.error_marker = self.ui.te_sci.markerDefine('!')
        self.ui.te_sci.setMarkerBackgroundColor(QColor(255,116,106,50),self.error_marker)
        
        self.isPython = type(self.lex) == type(PyLexer())
        
        # Events
        self.Events = Events()
        self.ui.te_sci.textChanged.connect(self.editorTextChanged)
##        self.ui.te_sci.linesChanged.connect(self.visibleLinesChanged)
        self.ui.te_sci.verticalScrollBar().valueChanged.connect(self.visibleLinesChanged)

        self.ui.te_sci.keyPressEvent = self.keyPressEvent
        self.ui.te_sci.dropEvent = self.dropEvent
        self.ui.te_sci.wheelEvent = self._wheelEvent
        
        # Remove Green border
        self.ui.te_sci.setFrameShape(qtw.QFrame.NoFrame)
        
        # Default settings
        self.settings = {
##            'wrapBehaviours':1,
##            'behaviours':1,
##            'showPrintMargin':0,
            'fontFamily':'Courier',
##            'fontFamily':'Hack',
##            'fontFamily':'Ubuntu',
            'fontSize':10,
            'theme':'dark',
##            'newLineMode':'unix',
            'showWhitespace':0,
            'showFoldStyle':0,
        }
        
        # Load settings
        for ky in self.settings:
            if lang in self.ide.settings['prog_lang'] and ky in self.ide.settings['prog_lang'][lang]:
                self.settings[ky]=self.ide.settings['prog_lang'][lang][ky]
            elif ky in self.ide.settings['editors']['scintilla']:
                self.settings[ky]=self.ide.settings['editors']['scintilla'][ky]
                
        self.setup()
        
    def setup(self):
        # Font
        font = QFont()
##        font.setFamily('Ubuntu Mono')
##        font.setFamily('DejaVu Sans Mono')

        font.setFamily(self.settings['fontFamily'])
        font.setFixedPitch(True)
        
        xfont = self.settings['fontSize']
        
        self.default_font = font
        
        font.setPointSize(int(xfont))
        self.ui.te_sci.setFont(font)
        self.ui.te_sci.setMarginsFont(font)

        # Margin 0 for line numbers
        fontmetrics = QFontMetrics(font)
##        self.ui.te_sci.setMarginsFont(font)
        self.ui.te_sci.setMarginWidth(0, fontmetrics.width("00000"))
        self.ui.te_sci.setMarginWidth(1, 0)
        self.ui.te_sci.setMarginLineNumbers(0, True)
        self.ui.te_sci.setMarginsBackgroundColor(QColor("#dddddd"))

        # Default to autocomplete
        self.autocompletemode=0
        self.toggleAutoComplete()
        
        self.ui.te_sci.setBraceMatching(Qsci.QsciScintilla.SloppyBraceMatch)
        
        self.ui.te_sci.setCallTipsStyle(Qsci.QsciScintilla.CallTipsContext)
        self.ui.te_sci.setAnnotationDisplay(Qsci.QsciScintilla.AnnotationBoxed )
        
        # Indentation
        self.ui.te_sci.setIndentationsUseTabs(0)
        self.ui.te_sci.setAutoIndent(1)
        self.ui.te_sci.setIndentationGuides(1)
        self.ui.te_sci.setBackspaceUnindents(1)

        if self.settings['showFoldStyle']:
            self.ui.te_sci.setFolding(Qsci.QsciScintilla.BoxedTreeFoldStyle)
        else:
            self.ui.te_sci.setFolding(Qsci.QsciScintilla.NoFoldStyle)
            
        self.ui.te_sci.setTabWidth(4)
        
##        self.ui.te_sci.setWrapIndentMode(Qsci.QsciScintilla.WrapIndentIndented)

        # Current line visible with special background color
        self.ui.te_sci.setCaretLineVisible(True)
        self.ui.te_sci.setEolMode(Qsci.QsciScintilla.EolUnix)
        self.ui.te_sci.setEolVisibility(int(self.settings['showWhitespace']))
        
        self.wordwrapmode = 0
        
        if self.lex != None:
            self.lex.setDefaultFont(font)
            self.ui.te_sci.setLexer(self.lex)
        if sys.version_info.major==3:
            self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_STYLESETFONT, 1, bytes('Courier','utf-8'))
        else:
##            self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_STYLESETFONT, 1, 'Courier')
            self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_STYLESETFONT, 1, self.settings['fontFamily'])
        
        self.ui.te_sci.setCaretLineBackgroundColor(QColor(105,184,221,30))
        
        self.setupStyle()
        
    def setupStyle(self,*kargs):
        # Customize Python lexer
        if self.isPython:
            
##            self.lex.setIndentationWarning(1)
            
            if self.ide.theme == 'dark':
    ##            self.ui.te_sci.setCaretLineBackgroundColor(QColor(105,184,221,30))
                self.ui.te_sci.setCaretForegroundColor(QColor(255,255,255))
                
                shade=25
                self.lex.setDefaultPaper(QColor(shade,shade,shade))
                self.lex.setPaper(QColor(shade,shade,shade),self.lex.Default)
                self.ui.te_sci.setColor(QColor(255,255,255))
                
                self.ui.te_sci.setMarginsBackgroundColor(QColor(35,35,35))
##                self.ui.te_sci.setMarginsBackgroundColor(QColor(60,60,60))
                self.ui.te_sci.setWhitespaceBackgroundColor(QColor(180,80,80))
##                self.ui.te_sci.setFoldMarginColors(QColor(200,200,200),QColor(90,90,90))
##                self.ui.te_sci.setFoldMarginColors(QColor(60,60,60),QColor(60,60,60))
                self.ui.te_sci.setFoldMarginColors(QColor(45,45,45),QColor(45,45,45))
    ##            self.ui.te_sci.setPaper(QColor(80,80,80))
                self.ui.te_sci.setMarginsForegroundColor(QColor(200,200,200))
    ##            self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_STYLESETBACK,Qsci.QsciScintilla.STYLE_DEFAULT,QColor(150,150,150))
                
                self.ui.te_sci.setMatchedBraceBackgroundColor(QColor(shade,shade,shade))
##                self.ui.te_sci.setMarkerBackgroundColor(QColor(255,116,106),-1)
                self.ui.te_sci.setMatchedBraceForegroundColor(QColor(170,0,255))
                self.ui.te_sci.setUnmatchedBraceBackgroundColor(QColor(shade,shade,shade))
                
                # Set defaults for all:
                style_obj = set(styleD.keys()).intersection(dir(self.lex))
                style_obj.remove('Default')
                style_obj = set(['Default']).union(sorted(style_obj))
                
                for c in sorted(style_obj,reverse=1):
                    clr = styleD[c]
                    if clr == '':
    ##                    clr = '255,255,255'
                        clr = styleD['Default']
    ##                print c,clr
                    try:
##                        exec('self.lex.setPaper(QColor(230,30,30),self.lex.'+c+')',globals(),locals())
                        exec('self.lex.setPaper(QColor({0},{0},{0}),self.lex.{1})'.format(shade,c))
                        exec('self.lex.setColor(QColor('+clr+'),self.lex.'+c+')')
                    except:
                        print('no keyword',c)
                
                
                # Highlight Background for errors
                rclr = QColor(255,184,160,100)
##                self.lex.setPaper(rclr, self.lex.UnclosedString)
                
            else:
##                print 'light theme'
##                self.lex = Qsci.QsciLexerPython()
                
                self.lex.setDefaultFont(self.default_font)
                self.ui.te_sci.setLexer(self.lex)
                
                if sys.version_info.major==3:
                    self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_STYLESETFONT, 1, bytes('Courier','utf-8'))
                else:
        ##            self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_STYLESETFONT, 1, 'Courier')
                    self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_STYLESETFONT, 1, self.settings['fontFamily'])
                
                # Update From Light style
                shade=240
                self.lex.setDefaultPaper(QColor(shade,shade,shade))
##                print self.lex.Default
                self.lex.setPaper(QColor(shade,shade,shade),self.lex.Default)
                self.ui.te_sci.setColor(QColor(0,0,0))
                
               
##                self.ui.te_sci.setMarginsBackgroundColor(QColor(60,60,60))
##                self.ui.te_sci.setWhitespaceBackgroundColor(QColor(80,80,80))
##                self.ui.te_sci.setFoldMarginColors(QColor(200,200,200),QColor(90,90,90))
    ##            self.ui.te_sci.setPaper(QColor(80,80,80))
##                self.ui.te_sci.setMarginsForegroundColor(QColor(200,200,200))
    ##            self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_STYLESETBACK,Qsci.QsciScintilla.STYLE_DEFAULT,QColor(150,150,150))
                
                self.ui.te_sci.setMatchedBraceBackgroundColor(QColor(shade,shade,shade))
                self.ui.te_sci.setMatchedBraceForegroundColor(QColor(170,0,255))
                self.ui.te_sci.setUnmatchedBraceBackgroundColor(QColor(shade,shade,shade))
                
                style_obj = set(styleLightD.keys()).intersection(dir(self.lex))
                style_obj.remove('Default')
                style_obj = set(['Default']).union(sorted(style_obj))
                for c in sorted(style_obj,reverse=1):
                    clr = styleLightD[c]
                    if clr == '':
                        clr = styleLightD['Default']
                    try:
                        exec('self.lex.setPaper(QColor(240,240,240),self.lex.'+c+')')
                        exec('self.lex.setColor(QColor('+clr+'),self.lex.'+c+')')
                    except:
                        print('no keyword',c)
                
                self.ui.te_sci.setMarginsBackgroundColor(QColor(200,200,200))
                self.ui.te_sci.setMarginsForegroundColor(QColor(60,60,60))
                self.ui.te_sci.setFoldMarginColors(QColor(200,200,200),QColor(200,200,200))
    
    def keyPressEvent(self,event):
        ky = event.key()
        handled = 0
        if ky in [qtc.Qt.Key_Enter,qtc.Qt.Key_Return,qtc.Qt.Key_Tab,qtc.Qt.Key_Backtab,qtc.Qt.Key_Delete,qtc.Qt.Key_Backspace,qtc.Qt.Key_Z,qtc.Qt.Key_Y]:
            self.okedit = 0
        
        if event.modifiers() & qtc.Qt.ControlModifier:
            if event.key() == qtc.Qt.Key_Space:
                self.ui.te_sci.autoCompleteFromAll()
                handled = 1
            elif event.key() == qtc.Qt.Key_Delete:
                # Delete line
                self.okedit = 1
                self.removeLines()
##                self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_LINEDELETE)
                handled = 1
            elif event.key() == qtc.Qt.Key_Plus:
                # Zoom In
                self.zoom += 1
            elif event.key() == qtc.Qt.Key_Minus:
                # Zoom Out
                self.zoom -= 1
            elif event.key() == qtc.Qt.Key_0:
                # Reset Zoom
                self.resetZoom()
                handled=1
            elif event.key() == qtc.Qt.Key_P:
                if self.lang == 'python':
                    self.insertText('print()')
                    qtw.QApplication.processEvents()
                    pos = self.getCursorPosition()
                    self.ui.te_sci.setCursorPosition(pos[0],pos[1]-1)
                elif self.lang in ['javascript','html','handlebars']:
                    self.insertText('console.log()')
                    pos = self.getCursorPosition()
                    qtw.QApplication.processEvents()
                    self.ui.te_sci.setCursorPosition(pos[0],pos[1]-1)
        
        elif event.modifiers() & qtc.Qt.AltModifier:
            if event.key() == qtc.Qt.Key_Down:
                # Move Line Down
                self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_MOVESELECTEDLINESDOWN)
                handled = 1
            elif event.key() == qtc.Qt.Key_Up:
                # Move Line Down
                self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_MOVESELECTEDLINESUP)
                handled = 1
            elif event.key() == qtc.Qt.Key_C:
                # Python Autocomplete
                if USE_JEDI and self.ide.currentEditor().lang == 'python':
                    'run autocomplete'
                    self.jedi_autocomplete()
                    handled = 1

##        elif ky in [qtc.Qt.Key_Enter,qtc.Qt.Key_Return,qtc.Qt.Key_Colon]:
##            # Check for errors 
##            if self.isPython:
##                Qsci.QsciScintilla.keyPressEvent(self.ui.te_sci,event)
##                pos = self.getCursorPosition()[0]
##                if ky != qtc.Qt.Key_Colon:
##                    pos = pos-1
##                self.check_line(pos)
##                handled = 1

        if not handled:
            Qsci.QsciScintilla.keyPressEvent(self.ui.te_sci,event)
            qtw.QApplication.processEvents()
    ##        if ky in [qtc.Qt.Key_Enter,qtc.Qt.Key_Return,qtc.Qt.Key_Tab,qtc.Qt.Key_Backtab,qtc.Qt.Key_Delete,qtc.Qt.Key_Backspace]:
    ##            self.editingFinished()
            self.okedit = 1
            self.editorTextChanged()
    
    def _wheelEvent(self, event):
        steps = event.angleDelta().y()/120.
        handled = 0
        if event.modifiers() & qtc.Qt.ControlModifier:
            if steps > 0:
                self.ui.te_sci.zoomIn(steps)
            else:
                self.ui.te_sci.zoomOut(-steps)
            
            self.zoom += steps
            handled = 1
##        elif event.modifiers() & qtc.Qt.ShiftModifier:
##            handled = 1
##            self.ui.te_sci.scroll(steps,0)
##            event.accept()
##            self.ui.te_sci.scrollContentsBy(steps*10,10)
##            qtw.QApplication.processEvents()
##            print dir(self.ui.te_sci.horizontalScrollBar())
##            self.ui.te_sci.horizontalScrollBar().scroll(0,steps)
##            self.ui.te_sci.horizontalScrollBar().move(steps,0)
        
        if not handled:
            Qsci.QsciScintilla.wheelEvent(self.ui.te_sci,event)
##            qtw.QApplication.processEvents()
    
    def resetZoom(self):
        ##print 'reset zoom', self.zoom
        if self.zoom > 0:
            self.ui.te_sci.zoomOut(self.zoom)
        elif self.zoom < 0:
            self.ui.te_sci.zoomIn(-self.zoom)
        
        self.zoom = 0
    
    #---Editor Functions
    def setText(self,txt):
        self.ui.te_sci.setText(txt)
##        if self.settings['newLineMode']=='unix':
##            self.ui.te_sci.convertEols(Qsci.QsciScintilla.EolUnix)
        if 'outline' in self.ide.pluginD and hasattr(self,'id'):
            self.ide.pluginD['outline'].widget.updateOutline(wdg=self,toggle_view=self.ide.leftPluginVisible,txt=txt)
##    def updateText(self,txt):
##        # Update, but keep current undo
##        self.ui.te_sci.selectAll(select=1)
##        self.ui.te_sci.replaceSelectedText(txt)
    
    def getText(self):
##        if self.settings['newLineMode']=='unix':
##            self.ui.te_sci.convertEols(Qsci.QsciScintilla.EolUnix)
        txt = self.ui.te_sci.text()
        return txt
    
    def getSelectedText(self):
        return self.ui.te_sci.selectedText()
    
    def selectAll(self):
        self.ui.te_sci.selectAll()
    
    def insertText(self,txt):
##        self.ui.te_sci.removeSelectedText()
##        self.ui.te_sci.insert(txt)
        self.ui.te_sci.replaceSelectedText(txt)

    def find(self,txt,re=0,cs=0,wo=0):
        return self.ui.te_sci.findFirst(txt,re,cs,wo,1)
    
    def replace(self,ftxt,rtxt,re=0,cs=0,wo=0):
        stxt = str(self.ui.te_sci.selectedText())
        if stxt.lower() == str(ftxt).lower():
            self.ui.te_sci.replace(rtxt)
        self.ui.te_sci.findFirst(ftxt,re,cs,wo,1)
    
    def replaceAll(self,ftxt,rtxt,re=0,cs=0,wo=0):
        cnt = 0
        r = self.ui.te_sci.findFirst(ftxt,re,cs,wo,0,1,0,0)
        self.okedit = 0
        while r:
            cnt +=1
            self.ui.te_sci.replace(rtxt)
##            qtw.QApplication.processEvents()
            r = self.ui.te_sci.findFirst(ftxt,re,cs,wo,0)
        self.okedit = 1
        self.editorTextChanged()
        qtw.QMessageBox.information(self,'Replace All',str(cnt)+' occurrences replaced')
    
    def gotoLine(self,line):
        self.ui.te_sci.setCursorPosition(self.ui.te_sci.lines(),0)  # Send to bottom so cursor is at top of page
        self.ui.te_sci.setCursorPosition(line,0)
        #self.ui.te_sci.setSelection(line,0,line,-1)
        self.ui.te_sci.setFocus()
##        qtw.QApplication.processEvents()
##        self.visibleLinesChanged()
    
    def editorTextChanged(self):
        if self.okedit:
            self.Events.editorChanged.emit(self)
    
    def getVisibleLines(self):
        line_first = self.ui.te_sci.firstVisibleLine()
        line_last = line_first+self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_LINESONSCREEN)
        return line_first,line_last
    
    def visibleLinesChanged(self):
        self.Events.visibleLinesChanged.emit(self,self.getVisibleLines())
    
##    def editingFinished(self):
##        self.Events.editingFinished.emit(self)
    
    def getCursorPosition(self):
        return self.ui.te_sci.getCursorPosition()
    
    def removeLines(self):
        start = self.ui.te_sci.getSelection()[0]
        stop = self.ui.te_sci.getSelection()[2]
        self.ui.te_sci.setCursorPosition(start,0)
        for i in range(stop-start+1):
            self.ui.te_sci.SendScintilla(Qsci.QsciScintilla.SCI_LINEDELETE)
        self.ui.te_sci.setCursorPosition(start,0)
    
    def toggleComment(self):
        lang = self.ide.currentEditor().lang
        if lang in commentD:
            if self.ui.te_sci.getSelection()[0] != -1:
                start = self.ui.te_sci.getSelection()[0]
                stop = self.ui.te_sci.getSelection()[2]
                lines = range(start,stop+1)
                if start == stop: lines = [start]
            else:
                start = stop = self.ui.te_sci.getCursorPosition()[0]
                lines = [start]
            
            # First line text - check if adding comment or removing
##            txt1 = str(self.ui.te_sci.text(start)).lstrip()
            comment_start = self.ui.te_sci.text(start).strip().startswith(commentD[lang])
            
            self.ui.te_sci.setSelection(start,0,stop,self.ui.te_sci.lineLength(stop)-1)
            ntxt = ''
            self.okedit=0
            for i in lines:
##                if txt1.startswith(commentD[lang]): # Remove Comment
                if comment_start: # Remove Comment
##                    if str(self.ui.te_sci.text(i)).startswith(commentD[lang]):
                    if self.ui.te_sci.text(i).startswith(commentD[lang]):
                        ntxt += self.ui.te_sci.text(i)[len(commentD[lang]):]
                    else:
                        ntxt += self.ui.te_sci.text(i)
                else: # Add Comment
                    ntxt += commentD[lang]+self.ui.te_sci.text(i)
            self.okedit=1

            self.ui.te_sci.replaceSelectedText(ntxt[:-1])
            self.ui.te_sci.setSelection(start,0,stop,self.ui.te_sci.lineLength(stop)-1)

##            self.ui.te_sci.setSelection(start,0,stop,self.ui.te_sci.lineLength(stop))

    def indent(self):
        if self.ui.te_sci.getSelection()[0] != -1:
            start = self.ui.te_sci.getSelection()[0]
            stop = self.ui.te_sci.getSelection()[2]
            lines = range(start,stop+1)
            if start == stop: lines = [start]
        else:
            start = stop = self.ui.te_sci.getCursorPosition()[0]
            lines = [start]
        self.okedit=0
        for i in lines:
            self.ui.te_sci.indent(i)
        self.okedit=1
        
    def unindent(self):
        self.okedit=0
        if self.ui.te_sci.getSelection()[0] != -1:
            start = self.ui.te_sci.getSelection()[0]
            stop = self.ui.te_sci.getSelection()[2]
            lines = range(start,stop+1)
            if start == stop: lines = [start]
        else:
            start = stop = self.ui.te_sci.getCursorPosition()[0]
            lines = [start]
        for i in lines:
            self.ui.te_sci.unindent(i)
        self.okedit=1
    
    def toggleWordWrap(self):
        self.wordwrapmode = not self.wordwrapmode
        if self.wordwrapmode:
            self.ui.te_sci.setWrapMode(Qsci.QsciScintilla.WrapCharacter)
        else:
            self.ui.te_sci.setWrapMode(Qsci.QsciScintilla.WrapNone)
    
##            txt = self.ui.te_sci.selectedText()
##            print txt
##            ntxt = ''
##            for t in txt.split('\n'):
##                ntxt += commentD[lang]+t+'\n'
##            
##            self.ui.te_sci.replaceSelectedText(ntxt)

    def toggleWhitespace(self):
        v=not self.ui.te_sci.eolVisibility()
        self.ui.te_sci.setEolVisibility(v)
        if v:
            self.ui.te_sci.setWhitespaceVisibility(Qsci.QsciScintilla.WsVisible)
        else:
            self.ui.te_sci.setWhitespaceVisibility(Qsci.QsciScintilla.WsInvisible)
        

    def toggleAutoComplete(self):
        self.autocompletemode = not self.autocompletemode
        if self.autocompletemode:
            # Enabled autocompletion
##            self.ui.te_sci.setAutoCompletionFillupsEnabled(1)
            self.ui.te_sci.setAutoCompletionSource(Qsci.QsciScintilla.AcsAll)
##            self.ui.te_sci.setAutoCompletionThreshold(3)
            self.ui.te_sci.AutoCompletionUseSingle(Qsci.QsciScintilla.AcusExplicit)
        else:
            self.ui.te_sci.setAutoCompletionSource(Qsci.QsciScintilla.AcsNone)
    
    def dropEvent(self,event):
        self.ide.dropEvent(event)
    
    def setFocus(self):
        self.ui.te_sci.setFocus()

    #----Other Tools
    
##    def check_line(self,line_no):
##        
##        txt = str(self.ui.te_sci.text(line_no).toUtf8()).decode('utf-8')
##        
##        stripped    = txt.split('#')[0].strip()
##        firstWord   = stripped.split(" ")[0]
##        if stripped == '':
##            lastChar = ''
##        else:
##            lastChar=stripped[-1]
##        ok = 1
####        print firstWord,lastChar
##        if firstWord in ["for","if", "else", "def","class","elif", "try","except","finally","while"] and lastChar != ':':
##            ok = 0
##        
##        if not ok:
##            self.addLineError(line_no)
##        else:
##            self.addLineError(line_no,delete=1)
####        print ('line check',ok)
    
    def addLineError(self,line_no,delete=0):
        if not delete:
            self.ui.te_sci.markerAdd(line_no,self.error_marker)
        else:
            self.ui.te_sci.markerDelete(line_no,self.error_marker)
    
    def clearLineErrors(self):
        self.ui.te_sci.markerDeleteAll(self.error_marker)
    
    def jedi_autocomplete(self):
        # Setup jedi autocompletion
        pos = self.ui.te_sci.getCursorPosition()
        x,y = self.ui.te_sci.SendScintilla(self.ui.te_sci.SCI_POINTXFROMPOSITION,pos[0],pos[1]),self.ui.te_sci.SendScintilla(self.ui.te_sci.SCI_POINTYFROMPOSITION,pos[0],pos[1])
##        x,y = self.ui.te_sci.SendScintilla(self.ui.te_sci.SCI_POINTXFROMPOSITION,0,self.ui.te_sci.getCursorPosition()[0]),self.ui.te_sci.SendScintilla(self.ui.te_sci.SCI_POINTYFROMPOSITION,0,self.ui.te_sci.getCursorPosition()[1])
        
        
        line_first = self.ui.te_sci.firstVisibleLine()
        x=pos[1]*self.settings['fontSize']+30
        y = (pos[0]-line_first+2.5)*self.settings['fontSize']
##        x += 100
        y += 15
##        y = pos[1]
        
        sc = jedi.Script(self.getText(),pos[0]+1,pos[1])
        completions = sc.completions()
        
        menu = qtw.QMenu()
        menu.setStyleSheet("QMenu { menu-scrollable: 1; }")
        
        for c in range(len(completions)):
##            print c
            act = qtw.QAction(completions[c].name,menu)
            act.setData(c)
            menu.addAction(act)
        
##        print x,',',y
        act = menu.exec_(self.ui.te_sci.mapToGlobal(qtc.QPoint(x,y)))
##        act = menu.exec_(qtc.QPoint(x+30,y+10))
        if act != None:
            acttxt = str(act.text())
            c = int(act.data())
            self.insertText(completions[c].complete)

from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw
from .output_ui import Ui_Form
from .outputText_ui import Ui_OutWidget
import sys, os, re, webbrowser, time, codecs, datetime

##re_file     = re.compile('(\s*)(File "(.*))\n')
##re_loc = re.compile('File "([^"]*)", line (\d+)')
re_file = re.compile('File "([^"]*)", line (\d+)')

class Output(qtw.QWidget):
    def __init__(self,parent=None):
        qtw.QWidget.__init__(self,parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ide = parent
        
        self.wdgD = {}
        
        self.ui.split_pages.setSizes([200,self.ide.width()-200])
        
        self.ui.li_pages.contextMenuEvent = self.listMenuEvent
        
##        qtw.QShortcut(qtc.Qt.ALT+qtc.Qt.Key_5,self.ide,self.runCurrent) # Run current file
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_F5,self.ide,self.runCurrent) # Run current file
        
        # Load Settings
        self.settingsD = self.ide.loadPluginSettings('output')

    def editorTabChanged(self,wdg):
        if wdg.id in self.wdgD:
            owdg = self.wdgD[wdg.id]
            self.ui.li_pages.setCurrentRow(self.ui.sw_pages.indexOf(owdg))
    
    def editorTabClosed(self,wdg):
        if wdg.id in self.wdgD:
            owdg = self.wdgD[wdg.id]
            self.closeOutputWidget(owdg)

    def closeOutputWidget(self,owdg):
        ok=1
        if owdg.status != 'done':
            ok=0
            opentxt=os.path.split(owdg.filename)[1]
            resp=qtw.QMessageBox.warning(self,'Kill Running Proces','The following output process is still running:'+opentxt+'<br><br>Do you want to kill it?',qtw.QMessageBox.Yes | qtw.QMessageBox.No)
            if resp == qtw.QMessageBox.Yes:
                owdg.stopProcess()
                ok=1
        if ok:
            if hasattr(owdg,'file_id'):
                self.wdgD.pop(owdg.file_id)
            self.ui.li_pages.takeItem(self.ui.li_pages.row(owdg.listItem))
            self.ui.sw_pages.removeWidget(owdg)
    
    def runProcess(self,cmd,wdg,text='',typ='run',args={},justset=0):
        if cmd == 'webbrowser':
            # If webbrowser - launch in webbrowser
            webbrowser.open(wdg.filename)
        else:
            # Setup the Command
            full_cmd = cmd
            if '{{filename}}' in cmd:
                full_cmd = cmd.replace('{{filename}}',' "'+wdg.filename+'"')
            else:
                if wdg.filename != None and wdg.runCommand == None:
                    full_cmd = cmd+' "'+wdg.filename+'"'
            if 'new_file' in args and args['new_file']!=None:
                full_cmd = full_cmd.replace('{{new_file}}', '"'+args['new_file']+'"')
            
            if '{{scope}}' in full_cmd:
                full_cmd = full_cmd.replace('{{scope}}',self.ide.scopePath)
            
            if '{{python}}' in full_cmd:
                syfull_cmd = full_cmd.replace('{{python}}',sys.executable)
            
            if cmd != 'preview' or justset:
                i = self.ide.ui.sw_bottom.indexOf(self.ide.pluginD['output'].widget)
                self.ide.ui.tabbar_bottom.setCurrentIndex(i)
            if wdg.id in self.wdgD:
                # Process was run
                owdg = self.wdgD[wdg.id]
                self.ui.li_pages.setCurrentRow(self.ui.sw_pages.indexOf(owdg))
                if owdg.process_type != typ:
                    # Update information if process is different
                    if owdg.status != 'done':
                        owdg.stopProcess()
                    owdg.ui.le_cmd.setText(full_cmd)
                    
            else:
                owdg = self.newProcess(wdg.title,wdg.filename,full_cmd,icon=wdg.icon)
                self.wdgD[wdg.id] = owdg
                owdg.file_id = wdg.id
            owdg.process_type = typ
            
            # Toggle/Run Process
            if cmd=='preview' and not justset:
                owdg.setOutputText(text=text)
                if wdg.filename==None:
                    title = wdg.title
                else:
                    title = os.path.split(wdg.filename)[1]
                owdg.ui.l_title.setText('<b>&nbsp;'+title+'</b>')
            else:
                # Just set the command, don't run
                if justset:
##                    owdg.ui.le_cmd.setText(cmd)
                    owdg.ui.b_cmd.setChecked(1)
                    if not owdg.ui.b_stop.isEnabled():
                        owdg.ui.b_run.setEnabled(1)
                else:
                    owdg.toggleProcess()
        
    def newProcess(self,title,filename=None,full_cmd='',icon=None,cwd=None):
        
        # Create new process
        owdg = OutputPage(parent=self,ide=self.ide,filename=filename,cwd=cwd)
        owdg.ui.le_cmd.setText(full_cmd)
        sw_ind = self.ui.sw_pages.count()
        self.ui.sw_pages.insertWidget(sw_ind,owdg)
        itm = qtw.QListWidgetItem(title)
        if icon != None:
            itm.setIcon(icon)
        if filename != None:
            itm.setToolTip(filename)
        self.ui.li_pages.addItem(itm)

##        self.wdgD[wdg.id] = owdg
##        owdg.file_id = wdg.id
##        owdg.process_type = typ
        
        self.ui.li_pages.setCurrentRow(sw_ind)
        qtw.QApplication.processEvents()
        owdg.listItem = self.ui.li_pages.item(sw_ind)
        return owdg

    def newBlankProcess(self,title=None,filename=None,full_cmd='',icon=None,cwd=None):
        ok = 1
        if title == None: title=''
        if icon == None: icon = qtg.QIcon(self.ide.iconPath+'console.png')
        title,ok = qtw.QInputDialog.getText(self,'New Process','Title for new process',qtw.QLineEdit.Normal, title)
        if ok:
            filename=title
            
            if cwd == None:
                cwd = self.ide.currentPath
            
            owdg = self.newProcess(title,filename,full_cmd,icon,cwd)
            owdg.ui.b_cmd.setChecked(1)
            if not owdg.ui.b_stop.isEnabled():
                owdg.ui.b_run.setEnabled(1)
            else:
                owdg.toggleProcess()
            
            owdg.ui.b_cmdw.click()
            
            return owdg

    def runCurrent(self):
        cr = self.ui.li_pages.currentRow()
        if cr > -1:
            owdg = self.ui.sw_pages.currentWidget()
            if owdg.ui.b_stop.isEnabled():
                owdg.ui.b_stop.click()
            else:
                owdg.ui.b_run.click()

    def killAll(self):
        opentxt = ''
        resp = qtw.QMessageBox.Yes
        for wdg.id in self.wdgD:
            owdg = self.wdgD[wdg.id]
            if owdg.status != 'done':
                opentxt+='<br>'+'&nbsp;'*5+os.path.split(owdg.filename)[1]
                break
                
        if opentxt != '':
            resp=qtw.QMessageBox.warning(self,'Kill Running Processes','The following output processes are still running:'+opentxt+'<br><br>Do you want to kill all running processes?',qtw.QMessageBox.Yes | qtw.QMessageBox.No)
            if resp == qtw.QMessageBox.Yes:
                for wdg.id in self.wdgD:
                    owdg = self.wdgD[wdg.id]
                    if owdg.status != 'done':
                        owdg.stopProcess()
        
        # Close all Tabs
        if resp == qtw.QMessageBox.Yes:
            for wdg.id in self.wdgD:
                owdg = self.wdgD[wdg.id]
                self.ui.sw_pages.removeWidget(owdg)
            self.ui.li_pages.clear()
            
    def listMenuEvent(self,event):
        cwdg = self.ui.sw_pages.currentWidget()
        menu = qtw.QMenu()
        if cwdg != None:
            if cwdg.status != 'done':
                menu.addAction(qtg.QIcon(self.ide.pluginPath+'output/stop.png'),'Stop')
            else:
                menu.addAction(qtg.QIcon(self.ide.iconPath+'tri_right.png'),'Run')
                menu.addAction(qtg.QIcon(self.ide.iconPath+'close.png'),'Close')
        
        menu.addSeparator()
        menu.addAction(qtg.QIcon(self.ide.iconPath+'add.png'),'New Process')
        
        i=-1
        for cmds in self.settingsD['commands']:
            i += 1
            act = menu.addAction(qtg.QIcon(self.ide.iconPath+'add.png'),'New '+cmds['title'])
            act.setData(i)
        
        resp = menu.exec_(event.globalPos())
        if resp != None:
            txt = str(resp.text())
            if txt == 'Close':
                self.closeOutputWidget(cwdg)
            elif txt == 'Stop':
                cwdg.stopProcess()
            elif txt == 'Run':
                cwdg.startProcess()
            elif txt == 'New Process':
                self.newBlankProcess()
            elif txt.startswith('New '):
                cmdD = self.settingsD['commands'][resp.data()]
                cwd=None
                if 'cwd' in cmdD:
                    cwd = cmdD['cwd']
                owdg = self.newBlankProcess(title=cmdD['title'],full_cmd=cmdD['cmd'],cwd=cwd)
                owdg.startProcess()
                owdg.ui.b_cmd.click()
                owdg.ui.le_cmdw.setFocus()

class OutputPage(qtw.QWidget):
    def __init__(self,parent=None,ide=None,filename=None,cwd=None):
        qtw.QWidget.__init__(self,parent)
        curdir = os.path.abspath('.')
        os.chdir(os.path.abspath(os.path.dirname(__file__)))
        self.ui = Ui_OutWidget()
        self.ui.setupUi(self)
        os.chdir(curdir)
        self.ide = ide
        self.parent = parent
        self.filename = filename
        self.current_dir = cwd
        
        self.process = None
        self.status = 'done'
        self.process_type = 'run'
        self.ui.fr_cmd.hide()
        self.ui.fr_cmdw.hide()
        
        self.ui.tb_out.setOpenLinks(0)
        self.ui.tb_out.anchorClicked.connect(self.urlClick)
        
        self.ui.b_run.setEnabled(0)
        self.ui.b_stop.setEnabled(0)
        
        # Command Settings
        self.history = []
        self.pointer = 0
        self.ui.le_cmdw.keyPressEvent = self.cmdKeyPress
        
        # Signals
        self.ui.b_run.clicked.connect(self.startProcess)
        self.ui.b_stop.clicked.connect(self.stopProcess)
        
##        self.ui.b_save_cmd.clicked.connect(self.saveRunCommand)
        self.ui.b_menu.clicked.connect(self.commandMenu)
        
        self.ui.le_cmdw.returnPressed.connect(self.writeProcess)
        
        self.ui.tb_out.contextMenuEvent = self.outtxtContextMenuEvent
    
    def urlClick(self,url):
        pth = url.toString().replace('%22','"').replace('%5C','\\')
        try:
            fileName,lineno = pth.split('#line')
            self.ide.openFile(fileName)
            self.ide.currentEditor().gotoLine(int(lineno)-1)
##            match   = re_loc.match(pth)
##            fileName    = match.group(1)
##            lineno      = int(match.group(2))
##            self.ide.openFile(fileName)
##            self.ide.currentEditor().gotoLine(lineno-1)
        except:
            print('error: could not goto file')
    
    def readOutput(self):
##        outbyt = bytearray(self.process.readAllStandardOutput()).decode('utf-8')
        outbyt_temp  = bytearray(self.process.readAllStandardOutput())
        try:
            outbyt = outbyt_temp.decode('utf-8')
        except:
            try:
                outbyt = outbyt_temp.decode('latin-1')
            except:
                outbyt = '<Scope Error: getting output text>'
        txt=outbyt.replace('<','&lt;').replace('>','&gt;').replace('  ','&nbsp;&nbsp;')
        self.appendText(txt,plaintext=1)
        
    def readErrors(self):
##        outbyt = bytearray(self.process.readAllStandardError()).decode('utf-8')
        outbyt_temp  = bytearray(self.process.readAllStandardError())
        try:
            outbyt = outbyt_temp.decode('utf-8')
        except:
            try:
                outbyt = outbyt_temp.decode('latin-1')
            except:
                outbyt = '<Scope Error: getting error text>'
        txt = '<font style="color:rgb(255,112,99);">' + outbyt.replace('<','&lt;').replace('>','&gt;').replace('  ','&nbsp;&nbsp;')+"</font>"
        txt = re_file.sub(r'<a style="color:rgb(121,213,255);" href="\g<1>#line\g<2>">\g<1></a>, line \g<2>',txt)
        self.appendText(txt)

    def processError(self,err):
##        print('process error',err,self.status,self.process.state())
        if self.dispError:
            errD = {0:'Failed to Start',1:'Crashed',2:'Timedout',3:'Read Error',4:'Write Error',5:'Unknown Error'}
            errtxt = errD[err]
            txt = '<font style="color:rgb(255,112,99);">Error: Process '+errtxt+'</font>'
            if err==0:
                txt += "<br>Check to make sure command is correct:<pre>"+self.actual_command+'</pre>'
            self.appendText(txt)
            if self.status != 'done' and self.process.state()==0:
                self.finished()
            self.status = 'done'
        
    def appendText(self,txt,plaintext=0):
        # Append to end without extra line space
        self.ui.tb_out.moveCursor(qtg.QTextCursor.End)
        self.ui.tb_out.textCursor().insertHtml(txt.replace('\n','<br>'))
        self.ui.tb_out.moveCursor(qtg.QTextCursor.End)
       
    def finished(self):
        if self.status != 'done':
            txt = self.ui.l_title.text()
            self.ui.l_title.setText(txt+'&nbsp;&nbsp;<b>Finished:</b>&nbsp;'+datetime.datetime.now().strftime('%I:%M:%S.%f'))
        self.status = 'done'
        self.ui.b_run.setEnabled(1)
        self.ui.b_stop.setEnabled(0)
##        self.ui.fr_title.setStyleSheet('QFrame#fr_title {background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(50, 50, 50, 255), stop:0.831818 rgba(80, 80, 80, 255), stop:1 rgba(100, 100, 100, 255));color:white;border:1px solid rgba(130,130,130,200);}')
        self.ui.fr_title.setStyleSheet('QFrame#fr_title {background-color: transparent;padding:2px;}')
        self.ui.tb_out.setStyleSheet('QTextBrowser{background-color:rgb(40,40,40);color:white;border-bottom-left-radius:5px;border-bottom-right-radius:5px;} a {color:rgb(121,213,255);}')
    
        # Update list pages
        self.listItem.setForeground(qtg.QColor(qtg.QColor(0,0,0)))
        fnt = self.listItem.font()
        fnt.setItalic(0)
        self.listItem.setFont(fnt)
        
    def toggleProcess(self):
        if self.status != 'done':
            self.stopProcess()
        else:
            self.startProcess()
    
    def setOutputText(self,text):
        self.ui.tb_out.setPlainText(text)
    
    def startProcess(self):
        self.ui.b_run.setEnabled(0)
        self.ui.b_stop.setEnabled(1)
        self.dispError = 1
        
        flname=''
        if os.path.exists(self.filename):
            flname = os.path.split(self.filename)[1]
        
        self.ui.l_title.setText('<b>&nbsp;'+flname+'&nbsp;&nbsp;&nbsp;&nbsp;</b><font color=#ccc><b>Started:</b> '+datetime.datetime.now().strftime('%I:%M:%S.%f'))
##        self.ui.fr_title.setStyleSheet('QFrame#fr_title {background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(48, 85, 100, 255), stop:0.21267 rgba(61, 107, 127, 255), stop:0.831818 rgba(72, 127, 150, 255), stop:1 rgba(104, 166, 175, 255));color:white;border-bottom:1px solid rgba(30,30,30,200);padding:2px;}')
        self.ui.fr_title.setStyleSheet('QFrame#fr_title {background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(35,75,117, 200), stop:1 rgba(24,69,125, 200));color:white;border-bottom:1px solid rgba(30,30,30,200);padding:2px;}')
        self.ui.tb_out.setStyleSheet('background-color:rgb(30,30,30);border-bottom-left-radius:5px;border-bottom-right-radius:5px;')
        self.ui.tb_out.setText('')
        self.listItem.setForeground(qtg.QColor(qtg.QColor(48, 85, 100)))
        fnt = self.listItem.font()
        fnt.setItalic(1)
        self.listItem.setFont(fnt)
        
        self.process = qtc.QProcess()
        self.process.waitForStarted(100)
        self.process.setReadChannel(qtc.QProcess.StandardOutput)
        
        if self.current_dir != None:
            self.process.setWorkingDirectory(self.current_dir)
        elif os.path.exists(self.filename):
            self.process.setWorkingDirectory(os.path.dirname(self.filename))
        
        self.process.readyReadStandardOutput.connect(self.readOutput)
        self.process.readyReadStandardError.connect(self.readErrors)
        self.process.finished.connect(self.finished)
        self.process.error.connect(self.processError)
        
        self.actual_command = str(self.ui.le_cmd.text())
        self.process.start(self.ui.le_cmd.text())
        self.status = 'running'
    
    def stopProcess(self):
        self.dispError = 0
        self.process.kill()
        self.finished()
    
    def writeProcess(self):
        txt = self.ui.le_cmdw.text()+'\n'
        self.history.append(self.ui.le_cmdw.text())
        self.pointer = len(self.history)
        self.appendText('<b>'+txt+'</b>')   # somehow process doesn't work after this
        self.process.write(bytearray(txt.encode('utf-8')))
        self.ui.le_cmdw.clear()

    def cmdKeyPress(self,e):
        key   = e.key()
        handled = 0

        if key == qtc.Qt.Key_Up:
            # Up
            if len(self.history):
                if self.pointer > 0:
                    self.pointer -= 1
                    self.ui.le_cmdw.setText(self.history[self.pointer])
                    handled = 1
        elif key == qtc.Qt.Key_Down:
            # Down
            if len(self.history) and self.pointer<len(self.history):
                if self.pointer == len(self.history)-1:
                    self.ui.le_cmdw.setText('')
                    self.pointer += 1
                    handled = 1
                else:
                    self.pointer += 1
                    self.ui.le_cmdw.setText(self.history[self.pointer])
                    handled = 1
        
        if not handled:
            qtw.QLineEdit.keyPressEvent(self.ui.le_cmdw,e)

    def saveFile(self):
        if self.filename == None:
            fname = ''
        else:
            fname = os.path.split(self.filename)[-1].split('.')[0]
        fileext = ''
        if os.name =='nt':
            fileext = 'Text (*.txt);;All (*.*)'
        filename = qtw.QFileDialog.getSaveFileName(self,"Save Output",fname,fileext)[0]
        if filename!='':
            txt = self.ui.tb_out.toPlainText()
##            txt = self.ui.tb_out.toPlainText().toUtf8()).decode('utf-8')
            f = codecs.open(filename,'w','utf8')
            f.write(txt)
            f.close()
    
##    def saveRunCommand(self):
##        self.parent.ide.getEditorWidget(self.file_id).runCommand = str(self.ui.le_cmd.text())
    
    def outtxtContextMenuEvent(self,event):
        menu = qtw.QMenu('menu',self)

        icn = qtg.QIcon(self.parent.ide.iconPath+'/copy.png')
        a=menu.addAction(icn,'Copy')

        icn = qtg.QIcon(self.parent.ide.iconPath+'/clear.png')
        a=menu.addAction(icn,'Clear')
##        menu.addAction('Clear')
        icn = qtg.QIcon(self.parent.ide.iconPath+'/save.png')
        a=menu.addAction(icn,'Save output')
        
        act = menu.exec_(self.cursor().pos())
        if act != None:
            acttxt = str(act.text())
            if acttxt == 'Clear':
                self.ui.tb_out.clear()
            elif acttxt == 'Save output':
                self.saveFile()
            elif acttxt == 'Copy':
                self.ui.tb_out.copy()
    
    def commandMenu(self):
        menu = qtw.QMenu('menu',self)
        
        icn = qtg.QIcon(self.parent.ide.iconPath+'/save.png')
        a=menu.addAction(icn,'Save command for file')
##        menu.addAction('Clear')
        icn = qtg.QIcon(self.parent.ide.iconPath+'/delete.png')
        a=menu.addAction(icn,'Remove command for file')
        
        act = menu.exec_(self.cursor().pos())
        if act != None:
            acttxt = str(act.text())
            if acttxt.startswith('Save'):
                self.parent.ide.getEditorWidget(self.file_id).runCommand = str(self.ui.le_cmd.text())
            elif acttxt.startswith('Remove'):
                self.parent.ide.getEditorWidget(self.file_id).runCommand = None
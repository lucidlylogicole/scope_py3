# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'outputText.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_OutWidget(object):
    def setupUi(self, OutWidget):
        OutWidget.setObjectName("OutWidget")
        OutWidget.resize(535, 153)
        self.gridLayout = QtWidgets.QGridLayout(OutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.fr_cmd = QtWidgets.QFrame(OutWidget)
        self.fr_cmd.setStyleSheet("QFrame#fr_cmd{\n"
"background:black;\n"
"color:white;\n"
"border:0px;\n"
"border-bottom:1px solid rgba(130,130,130,200);\n"
"}")
        self.fr_cmd.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.fr_cmd.setFrameShadow(QtWidgets.QFrame.Raised)
        self.fr_cmd.setObjectName("fr_cmd")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.fr_cmd)
        self.gridLayout_2.setContentsMargins(4, 1, 0, 1)
        self.gridLayout_2.setHorizontalSpacing(4)
        self.gridLayout_2.setVerticalSpacing(0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.le_cmd = QtWidgets.QLineEdit(self.fr_cmd)
        self.le_cmd.setStyleSheet("QLineEdit {background:transparent;\n"
"border:0px;\n"
"color:white;\n"
"}")
        self.le_cmd.setText("")
        self.le_cmd.setObjectName("le_cmd")
        self.gridLayout_2.addWidget(self.le_cmd, 0, 1, 1, 1)
        self.label = QtWidgets.QLabel(self.fr_cmd)
        self.label.setStyleSheet("color:rgb(180,180,180);")
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)
        self.b_menu = QtWidgets.QPushButton(self.fr_cmd)
        self.b_menu.setEnabled(True)
        self.b_menu.setMaximumSize(QtCore.QSize(30, 16777215))
        self.b_menu.setStyleSheet("QPushButton,QToolButton {\n"
"background:transparent;\n"
"    border:0px;\n"
"    padding:3px;\n"
"    padding-left:6px;\n"
"    padding-right:6px;\n"
"    width:30px;\n"
"}\n"
"QPushButton:hover,QToolButton:hover {\n"
"background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:1, stop:0 rgba(75, 75, 75, 100), stop:0.154545 rgba(106, 106, 106, 100), stop:0.831818 rgba(134, 134, 134, 100), stop:1 rgba(142, 142, 142, 100));\n"
"}")
        self.b_menu.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../../style/img/menu.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_menu.setIcon(icon)
        self.b_menu.setIconSize(QtCore.QSize(16, 16))
        self.b_menu.setCheckable(True)
        self.b_menu.setObjectName("b_menu")
        self.gridLayout_2.addWidget(self.b_menu, 0, 2, 1, 1)
        self.gridLayout.addWidget(self.fr_cmd, 2, 1, 1, 1)
        self.tb_out = QtWidgets.QTextBrowser(OutWidget)
        font = QtGui.QFont()
        font.setFamily("FreeMono")
        self.tb_out.setFont(font)
        self.tb_out.setStyleSheet("QTextBrowser {\n"
"background:rgb(50,50,50);\n"
"color:white;\n"
"}\n"
"a {color:rgb(121,213,255);}")
        self.tb_out.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.tb_out.setObjectName("tb_out")
        self.gridLayout.addWidget(self.tb_out, 4, 1, 1, 1)
        self.fr_cmdw = QtWidgets.QFrame(OutWidget)
        self.fr_cmdw.setStyleSheet("QFrame#fr_cmdw{\n"
"background:black;\n"
"color:white;\n"
"border:0px;\n"
"   background-color:rgb(30,30,30);\n"
"   border-top:1px solid rgb(60,60,60);\n"
"}")
        self.fr_cmdw.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.fr_cmdw.setFrameShadow(QtWidgets.QFrame.Raised)
        self.fr_cmdw.setObjectName("fr_cmdw")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.fr_cmdw)
        self.gridLayout_4.setContentsMargins(4, 1, 4, 1)
        self.gridLayout_4.setHorizontalSpacing(4)
        self.gridLayout_4.setVerticalSpacing(0)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.le_cmdw = QtWidgets.QLineEdit(self.fr_cmdw)
        self.le_cmdw.setStyleSheet("background:transparent;\n"
"border:0px;\n"
"color:white;\n"
"padding:2px;")
        self.le_cmdw.setText("")
        self.le_cmdw.setObjectName("le_cmdw")
        self.gridLayout_4.addWidget(self.le_cmdw, 0, 2, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.fr_cmdw)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setStyleSheet("color:rgb(38,90,150);")
        self.label_4.setObjectName("label_4")
        self.gridLayout_4.addWidget(self.label_4, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.fr_cmdw, 5, 1, 1, 1)
        self.fr_title = QtWidgets.QFrame(OutWidget)
        self.fr_title.setStyleSheet("QFrame#fr_title {\n"
"background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(50, 50, 50, 255), stop:0.831818 rgba(80, 80, 80, 255), stop:1 rgba(100, 100, 100, 255));\n"
"border-bottom:1px solid rgba(30,30,30,200);\n"
"padding:2px;\n"
"}")
        self.fr_title.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.fr_title.setFrameShadow(QtWidgets.QFrame.Raised)
        self.fr_title.setObjectName("fr_title")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.fr_title)
        self.gridLayout_5.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_5.setSpacing(0)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.l_title = QtWidgets.QLabel(self.fr_title)
        self.l_title.setStyleSheet("color:white;border:0;")
        self.l_title.setText("")
        self.l_title.setWordWrap(True)
        self.l_title.setObjectName("l_title")
        self.gridLayout_5.addWidget(self.l_title, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.fr_title, 1, 1, 1, 1)
        self.frame_2 = QtWidgets.QFrame(OutWidget)
        self.frame_2.setStyleSheet("QFrame#frame_2 {\n"
"border-left2:1px solid rgba(40,40,40,255);\n"
"}\n"
"QPushButton,QToolButton {\n"
"background:rgba(0,0,0,0);\n"
"    border:0px;\n"
"    padding:3px;\n"
"    padding-left:6px;\n"
"    padding-right:6px;\n"
"    width:30px;\n"
"}\n"
"QPushButton:hover,QToolButton:hover {\n"
"background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:1, stop:0 rgba(75, 75, 75, 100), stop:0.154545 rgba(106, 106, 106, 100), stop:0.831818 rgba(134, 134, 134, 100), stop:1 rgba(142, 142, 142, 100));\n"
"}")
        self.frame_2.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.frame_2)
        self.gridLayout_3.setContentsMargins(1, 0, 1, 2)
        self.gridLayout_3.setSpacing(0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.b_stop = QtWidgets.QPushButton(self.frame_2)
        self.b_stop.setMinimumSize(QtCore.QSize(0, 30))
        self.b_stop.setMaximumSize(QtCore.QSize(30, 16777215))
        self.b_stop.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("stop.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_stop.setIcon(icon1)
        self.b_stop.setObjectName("b_stop")
        self.gridLayout_3.addWidget(self.b_stop, 1, 0, 1, 1)
        self.b_cmd = QtWidgets.QPushButton(self.frame_2)
        self.b_cmd.setMaximumSize(QtCore.QSize(30, 16777215))
        self.b_cmd.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("textfield.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_cmd.setIcon(icon2)
        self.b_cmd.setCheckable(True)
        self.b_cmd.setObjectName("b_cmd")
        self.gridLayout_3.addWidget(self.b_cmd, 2, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(2, 2, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem, 4, 0, 1, 1)
        self.b_run = QtWidgets.QPushButton(self.frame_2)
        self.b_run.setMinimumSize(QtCore.QSize(0, 30))
        self.b_run.setMaximumSize(QtCore.QSize(30, 16777215))
        self.b_run.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("../../style/img/tri_right.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_run.setIcon(icon3)
        self.b_run.setObjectName("b_run")
        self.gridLayout_3.addWidget(self.b_run, 0, 0, 1, 1)
        self.b_cmdw = QtWidgets.QPushButton(self.frame_2)
        self.b_cmdw.setMaximumSize(QtCore.QSize(30, 16777215))
        self.b_cmdw.setStyleSheet("color:white;")
        self.b_cmdw.setCheckable(True)
        self.b_cmdw.setObjectName("b_cmdw")
        self.gridLayout_3.addWidget(self.b_cmdw, 5, 0, 1, 1)
        self.gridLayout.addWidget(self.frame_2, 1, 0, 5, 1)

        self.retranslateUi(OutWidget)
        self.b_cmd.toggled['bool'].connect(self.fr_cmd.setVisible)
        self.b_cmdw.clicked['bool'].connect(self.fr_cmdw.setVisible)
        QtCore.QMetaObject.connectSlotsByName(OutWidget)

    def retranslateUi(self, OutWidget):
        _translate = QtCore.QCoreApplication.translate
        OutWidget.setWindowTitle(_translate("OutWidget", "Form"))
        self.label.setText(_translate("OutWidget", "Command:"))
        self.b_menu.setToolTip(_translate("OutWidget", "Save run command to workspace"))
        self.tb_out.setHtml(_translate("OutWidget", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'FreeMono\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.label_4.setText(_translate("OutWidget", "<html>&rsaquo;</html>"))
        self.b_stop.setToolTip(_translate("OutWidget", "Stop"))
        self.b_cmd.setToolTip(_translate("OutWidget", "Show/Hide Command"))
        self.b_run.setToolTip(_translate("OutWidget", "Run"))
        self.b_cmdw.setToolTip(_translate("OutWidget", "Show/Hide Interactive Command"))
        self.b_cmdw.setText(_translate("OutWidget", ">"))


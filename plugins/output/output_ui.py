# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'output.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(538, 114)
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.split_pages = QtWidgets.QSplitter(Form)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.split_pages.sizePolicy().hasHeightForWidth())
        self.split_pages.setSizePolicy(sizePolicy)
        self.split_pages.setOrientation(QtCore.Qt.Horizontal)
        self.split_pages.setObjectName("split_pages")
        self.li_pages = QtWidgets.QListWidget(self.split_pages)
        self.li_pages.setStyleSheet("QListWidget#li_pages {\n"
"background:transparent;\n"
"}\n"
"QListWidget::item {\n"
"color:white;\n"
"border-bottom2:1px solid rgba(130,130,130,200);\n"
"border-right2:1px solid rgba(130,130,130,200);\n"
"}\n"
"QListWidget2::item:selected {\n"
"    background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:0, stop:0 rgba(48, 85, 100, 255), stop:0.21267 rgba(61, 107, 127, 255), stop:0.831818 rgba(72, 127, 150, 255), stop:1 rgba(104, 166, 175, 255));\n"
"color:white;\n"
"}")
        self.li_pages.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.li_pages.setObjectName("li_pages")
        self.sw_pages = QtWidgets.QStackedWidget(self.split_pages)
        self.sw_pages.setObjectName("sw_pages")
        self.gridLayout.addWidget(self.split_pages, 0, 0, 1, 1)

        self.retranslateUi(Form)
        self.li_pages.currentRowChanged['int'].connect(self.sw_pages.setCurrentIndex)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))


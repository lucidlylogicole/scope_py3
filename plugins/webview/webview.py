from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw, QtWebEngineWidgets as qte
import os

# Custom Webpage class
class WebPage(qte.QWebEnginePage):
    def __init__(self,parent=None):
        self.parent = parent
        qte.QWebEnginePage.__init__(self,parent)

    def acceptNavigationRequest(self,url,typ,*args):
        # Override link
        if typ == qte.QWebEnginePage.NavigationTypeLinkClicked:
            return self.parent.urlClicked(url)
        return 1

    def javaScriptConsoleMessage(self, code, msg, line, *args):
        '''QWebEnginePage that prints Javascript errors to stderr.'''
        typ = ''
        if code != 0: typ=' ERROR'
        print('js>>%s %s  (line %s)' % (typ, msg, line))

class WebView(qte.QWebEngineView):
    def __init__(self,parent=None,baseurl=None):
        qte.QWebEngineView.__init__(self,parent)
        self.parent = parent
        web_page = WebPage(self)
        #web_page.setLinkDelegationPolicy(QtWebKit.QWebPage.DelegateAllLinks)
        self.setPage(web_page)
        self.baseurl = baseurl
        
        self.zoomValue = 1.
        self.zoomIncrement = 0.1
        
##        self.settings().setObjectCacheCapacities(0,0,0) # Make sure images are reloaded
    
    def setText(self,txt,baseurl=None):
        if baseurl != None:
            baseurl = qtc.QUrl(baseurl)
        if baseurl == None:
            baseurl=qtc.QUrl()
        
        self.setHtml(txt,baseurl)

    def find(self,txt,*args,**kargs):
        self.findText(txt,QtWebKit.QWebPage.FindWrapsAroundDocument)
    
    def load_link(self,url):
        # Custom load for handling markdown urls and external
        lnk = url.toString()
            # Markdown
        if lnk.startswith('file:') and lnk.endswith('.md'):
            filename = str(url.toLocalFile())
            import site_pkg.commonmark_mod
            
            html = site_pkg.commonmark_mod.generate(filename,custom=1)
            burl = url
            self.setText(html,burl)
            
        elif lnk.startswith('http') or lnk.startswith('www'):
            # External links
            import webbrowser
            webbrowser.open(lnk)
        else:
            self.load(url)

    def load_help(self,url):
        lnk = str(url.toString())
        if lnk.startswith('http'):
            self.load_link(url)
        else:
            # Read filename html
            filename = url.toLocalFile()
            chtml = ''
            if os.path.isfile(os.path.abspath(filename)):
                with open(os.path.abspath(filename),'r') as f:
                    chtml = f.read()
            
            # Get Main html
            with open(self.parent.scopePath+'/docs/main.html','r') as f:
                mhtml = f.read()
            
            if os.name =='nt':
                pfx="file:///"
            else:
                pfx="file://"
                            
            ind_fld = pfx+self.parent.scopePath+'/docs/'
            mhtml = mhtml.replace('{{contents}}',chtml).replace('{{fld}}',ind_fld)
            self.setText(mhtml,url)

    #---Events
    def keyPressEvent(self,event):
        ky = event.key()
        handled = 0

        if event.modifiers() & qtc.Qt.ControlModifier:
            if event.key() == qtc.Qt.Key_Plus:
                self.zoom(self.zoomIncrement)
                handled = 1
            elif event.key() == qtc.Qt.Key_Minus:
                self.zoom(-self.zoomIncrement)
                handled = 1
            elif event.key() == qtc.Qt.Key_0:
                self.resetZoom()
                handled=1
                
        if not handled:
            QtWebKit.QWebView.keyPressEvent(self,event)
        qtw.QApplication.processEvents()

    def wheelEvent(self, event):
        steps = event.angleDelta().y()/120
        handled = 0
        if event.modifiers() & qtc.Qt.ControlModifier:
            # Zoom
            if steps > 0:
                self.zoom(self.zoomIncrement)
            else:
                self.zoom(-self.zoomIncrement)
            handled = 1
        
        if not handled:
            qte.QWebEngineView.wheelEvent(self,event)
            qtw.QApplication.processEvents()

    def dropEvent(self,event):
        self.parent.dropEvent(event)

    def dropEvent(self,event):
        self.parent.dropEvent(event)
    
    #---Inspector/Debugging
    def setupInspector(self):
        page = self.page()
        page.settings().setAttribute(QtWebKit.QWebSettings.DeveloperExtrasEnabled, True)
        self.webInspector = QtWebKit.QWebInspector(self)
        self.webInspector.setPage(page)

        shortcut = qtw.QShortcut(self)
        shortcut.setKey(qtc.Qt.Key_F12)
        shortcut.activated.connect(self.toggleInspector)
        self.webInspector.setVisible(False)

    def toggleInspector(self):
        self.webInspector.setVisible(not self.webInspector.isVisible())

    #---Zoom
    def zoom(self, zx):
        self.zoomValue += zx
        self.setZoomFactor(self.zoomValue)
        qtw.QApplication.processEvents()

    def resetZoom(self):
        self.setZoomFactor(1)
        self.zoomValue = 1
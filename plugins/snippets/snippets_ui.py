# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'snippets.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(573, 163)
        Form.setMinimumSize(QtCore.QSize(0, 20))
        self.gridLayout_3 = QtWidgets.QGridLayout(Form)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setSpacing(0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.split_main = QtWidgets.QSplitter(Form)
        self.split_main.setOrientation(QtCore.Qt.Horizontal)
        self.split_main.setObjectName("split_main")
        self.frame = QtWidgets.QFrame(self.split_main)
        self.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setProperty("class", "")
        self.frame.setObjectName("frame")
        self.gridLayout = QtWidgets.QGridLayout(self.frame)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setHorizontalSpacing(4)
        self.gridLayout.setVerticalSpacing(2)
        self.gridLayout.setObjectName("gridLayout")
        self.le_search = QtWidgets.QLineEdit(self.frame)
        self.le_search.setObjectName("le_search")
        self.gridLayout.addWidget(self.le_search, 0, 2, 1, 1)
        self.cb_ext = QtWidgets.QComboBox(self.frame)
        self.cb_ext.setObjectName("cb_ext")
        self.cb_ext.addItem("")
        self.gridLayout.addWidget(self.cb_ext, 0, 1, 1, 1)
        self.b_reload = QtWidgets.QPushButton(self.frame)
        self.b_reload.setMaximumSize(QtCore.QSize(26, 16777215))
        self.b_reload.setStyleSheet("QPushButton,QToolButton {\n"
"border:0px;\n"
"background:transparent;\n"
"}")
        self.b_reload.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../../style/img/refresh.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_reload.setIcon(icon)
        self.b_reload.setObjectName("b_reload")
        self.gridLayout.addWidget(self.b_reload, 0, 0, 1, 1)
        self.li_snips = QtWidgets.QListWidget(self.frame)
        self.li_snips.setStyleSheet("QListWidget {background:transparent;\n"
"background:rgb(40,40,40);\n"
"}")
        self.li_snips.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.li_snips.setProperty("isWrapping", False)
        self.li_snips.setResizeMode(QtWidgets.QListView.Adjust)
        self.li_snips.setViewMode(QtWidgets.QListView.ListMode)
        self.li_snips.setObjectName("li_snips")
        self.gridLayout.addWidget(self.li_snips, 1, 0, 1, 4)
        self.frame_2 = QtWidgets.QFrame(self.split_main)
        self.frame_2.setStyleSheet("QPushButton,QToolButton {\n"
"    background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:0, y2:1, stop:0.00995025 rgba(94, 94, 94, 255), stop:0.174129 rgba(125, 125, 125, 255), stop:0.890547 rgba(164, 164, 164, 255), stop:1 rgba(188, 188, 188, 255));\n"
"    border:0px;\n"
"    padding:3px;\n"
"    padding-left:6px;\n"
"    padding-right:6px;\n"
"}\n"
"QPushButton:hover,QToolButton:hover {\n"
"    background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:0, y2:1, stop:0.00995025 rgba(114, 114, 114, 255), stop:0.174129 rgba(145, 145, 145, 255), stop:0.890547 rgba(184, 184, 184, 255), stop:1 rgba(208, 208, 208, 255));\n"
"}")
        self.frame_2.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setProperty("class", "")
        self.frame_2.setObjectName("frame_2")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame_2)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setSpacing(0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.te_code = QtWidgets.QPlainTextEdit(self.frame_2)
        self.te_code.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.te_code.setReadOnly(True)
        self.te_code.setObjectName("te_code")
        self.gridLayout_2.addWidget(self.te_code, 0, 2, 6, 1)
        self.frame_3 = QtWidgets.QFrame(self.frame_2)
        self.frame_3.setStyleSheet("QFrame#frame_3 {\n"
"\n"
"}\n"
"QPushButton,QToolButton {\n"
"background:transparent;\n"
"    border:0px;\n"
"    padding:3px;\n"
"    padding-left:6px;\n"
"    padding-right:6px;\n"
"    width:30px;\n"
"}\n"
"QPushButton:hover,QToolButton:hover {\n"
"background-color: qlineargradient(spread:pad, x1:1, y1:1, x2:1, y2:1, stop:0 rgba(75, 75, 75, 100), stop:0.154545 rgba(106, 106, 106, 100), stop:0.831818 rgba(134, 134, 134, 100), stop:1 rgba(142, 142, 142, 100));\n"
"}")
        self.frame_3.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.frame_3)
        self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_4.setSpacing(0)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.b_insert = QtWidgets.QPushButton(self.frame_3)
        self.b_insert.setMaximumSize(QtCore.QSize(28, 16777215))
        self.b_insert.setStyleSheet("QPushButton,QToolButton {\n"
"    border-top-right-radius:5px;\n"
"    border-top-left-radius:5px;\n"
"}")
        self.b_insert.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("../../style/img/up.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_insert.setIcon(icon1)
        self.b_insert.setObjectName("b_insert")
        self.gridLayout_4.addWidget(self.b_insert, 0, 0, 1, 1)
        self.b_new = QtWidgets.QPushButton(self.frame_3)
        self.b_new.setMaximumSize(QtCore.QSize(28, 16777215))
        self.b_new.setStyleSheet("")
        self.b_new.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("../../style/img/new.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_new.setIcon(icon2)
        self.b_new.setObjectName("b_new")
        self.gridLayout_4.addWidget(self.b_new, 2, 0, 1, 1)
        self.b_edit = QtWidgets.QPushButton(self.frame_3)
        self.b_edit.setMaximumSize(QtCore.QSize(28, 16777215))
        self.b_edit.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("../../style/img/edit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_edit.setIcon(icon3)
        self.b_edit.setObjectName("b_edit")
        self.gridLayout_4.addWidget(self.b_edit, 1, 0, 1, 1)
        self.b_fldr = QtWidgets.QPushButton(self.frame_3)
        self.b_fldr.setMaximumSize(QtCore.QSize(28, 16777215))
        self.b_fldr.setText("")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("../../style/img/folder_open.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_fldr.setIcon(icon4)
        self.b_fldr.setObjectName("b_fldr")
        self.gridLayout_4.addWidget(self.b_fldr, 3, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 2, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_4.addItem(spacerItem, 4, 0, 1, 1)
        self.gridLayout_2.addWidget(self.frame_3, 1, 0, 1, 2)
        self.gridLayout_3.addWidget(self.split_main, 0, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Snippets"))
        self.split_main.setProperty("class", _translate("Form", "pluginHorizontal"))
        self.le_search.setPlaceholderText(_translate("Form", "search"))
        self.cb_ext.setItemText(0, _translate("Form", "All"))
        self.b_reload.setToolTip(_translate("Form", "reload snippets directory"))
        self.b_insert.setToolTip(_translate("Form", "insert into current editor"))
        self.b_new.setToolTip(_translate("Form", "new snippet"))
        self.b_edit.setToolTip(_translate("Form", "edit snippet"))
        self.b_fldr.setToolTip(_translate("Form", "open snippet directory"))


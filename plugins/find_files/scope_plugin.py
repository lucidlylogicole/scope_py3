import os
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw

class Plugin(object):
    title = 'Search in Files'
    location = 'app' # left, bottom, right, app
    widget = None  # The widget for the plugin (set at getWidget)
    
    def __init__(self,parent=None):
        self.parent = parent
    
    def load(self):
        self.btn = self.parent.addLeftBarButton(qtg.QIcon('icon.png'),tooltip=self.title)
        self.btn.clicked.connect(self.addFindFilesWidget)
        # store widget with button (with addLeftBarButton.  if widget doesn't exist, it calls the getwidget)
        
    def loadWidget(self):
        from . import find_files
        curdir = os.path.abspath('.')
        os.chdir(os.path.dirname(__file__))
        self.widget = find_files.Find_Files(self.parent)
        os.chdir(curdir)

##        self.parent.addMainWidget(self.widget,self.title,icon=self.btn.icon(),typ='app')
        self.parent.Events.workspaceChanged.connect(self.widget.changeWorkspace)
        
        self.widget.pinned=1
        return self.widget
    
    def addFindFilesWidget(self):
        if self.widget == None:
            self.loadWidget()
            ti = self.parent.ui.tab_right.addTab(self.widget,self.btn.icon(),'File Search')
            self.parent.ui.tab_right.setTabToolTip(ti,'Search in Files')
##        self.toggle()
        else:
##        self.parent.ui.tab_right.setVisible(1)
            ti = self.parent.ui.tab_right.indexOf(self.widget)
        self.parent.ui.tab_right.setCurrentIndex(ti)
        self.parent.toggleRightPlugin(show=1)

    def toggle(self):
        self.parent.changeTab(self.widget.id)

import os,sys,fnmatch
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw
from .find_files_ui import Ui_Form
import re

class Find_Files(qtw.QWidget):
    def __init__(self,parent=None,pth=None):
        qtw.QWidget.__init__(self,parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.IDE = parent
        
        self.ui.fr_settings.hide()
        
        # Set Default Path
        if pth == None:
            pth = os.path.expanduser('~')
        self.ui.le_path.setText(pth)
        
        # Load IDE Settings
        if self.IDE != None:
            # Set default extensions
            if 'default_ext' in self.IDE.settings['plugins']['find_files']:
                self.ui.le_ext.setText(self.IDE.settings['plugins']['find_files']['default_ext'])
            
            if 'ignore_dirs' in self.IDE.settings['plugins']['find_files']:
                self.ui.le_ignore_dirs.setText(self.IDE.settings['plugins']['find_files']['ignore_dirs'])
            if 'ignore_files' in self.IDE.settings['plugins']['find_files']:
                self.ui.le_ignore_files.setText(self.IDE.settings['plugins']['find_files']['ignore_files'])
        
        if parent != None:
            # IDE specific code
            if self.IDE.currentWorkspace != None and self.IDE.workspaces[self.IDE.currentWorkspace]['basefolder'] != None:
                self.ui.le_path.setText(self.IDE.workspaces[self.IDE.currentWorkspace]['basefolder'])
        else:
            self.ui.tr_results.setStyleSheet('')
        
        # Signals
        self.ui.b_search.clicked.connect(self.search_click)
        self.ui.b_browse.clicked.connect(self.browse)
        self.ui.b_copy.clicked.connect(self.copyToClipboard)
        self.ui.tr_results.itemDoubleClicked.connect(self.itmDClick)
        
        self.ui.tr_results.resizeColumnToContents(1)
        self.ui.tr_results.sortByColumn(0,0)
    
    def search_click(self):
        if self.ui.b_search.isChecked():
            self.ui.b_search.setIcon(qtg.QIcon('style/img/stop.png'))
            self.search()
        else:
            self.ui.b_search.setIcon(qtg.QIcon('style/img/search.png'))
    
    def search(self):
        
        pth = str(self.ui.le_path.text())
        ext = tuple(str(self.ui.le_ext.text()).split(','))
        
        self.currentPath = pth
        
        stxt = str(self.ui.le_search.text())
        # Check Type
        search_type = 0
        if self.ui.ckbx_reg.isChecked():
            search_type = 2
        elif self.ui.ckbx_case.isChecked():
            search_type = 1
        else:
            stxt = stxt.lower() # no case match
        
        # Ignore Stuff
        ignore_dirs = str(self.ui.le_ignore_dirs.text()).split(',')
        ignore_files = str(self.ui.le_ignore_files.text()).split(',')
        
        self.ui.tr_results.clear()
        
        if not os.path.exists(pth):
            qtw.QMessageBox.warning(self,'Invalid Path','The path is invalid<br><br>'+pth)
        else:
            for root, dirnames, filenames in os.walk(pth):
                
                if not root in ignore_dirs:
                    for d in dirnames[:]:
                        if d in ignore_dirs:
                            dirnames.remove(d)
                    
                    self.ui.l_cur_file.setText(root)
                    qtw.QApplication.processEvents()
                    if not self.ui.b_search.isChecked(): break
                    
                    for filename in filenames:
                        qtw.QApplication.processEvents()
                        if not filename in ignore_files:
                            filepath = os.path.join(root,filename)
                            if not self.ui.b_search.isChecked(): break
                            
                            # Search in file
                            if ext == '' or filename.endswith(ext):
                                self.ui.l_cur_file.setText(filepath)
                                qtw.QApplication.processEvents()
                                self.searchFile(filepath,stxt,search_type)

        self.ui.l_cur_file.setText('')
        self.ui.b_search.setChecked(0)
        
        self.ui.tr_results.resizeColumnToContents(0)
        self.ui.tr_results.resizeColumnToContents(2)
        self.ui.b_search.setIcon(qtg.QIcon('style/img/search.png'))
    
    def searchFile(self,filepath,search_text,search_type):
        stxt = search_text
        itm = None
        
        if stxt in filepath or stxt == '':
            itm = self.addFileItem(filepath)
        
        line_cnt = ''
        
        if stxt != '':
            # Check in file
            cnt = 0
            line_cnt = 0
            try:
                with open(filepath, 'rU') as searchfile:
                    for line in searchfile:
                        cnt +=1
                        found = 0
                        if search_type == 0: # no options
                            if stxt in line.lower():
                                found = 1
                        elif search_type == 1: # match case
                            if stxt in line:
                                found = 1
                        elif search_type == 2: # regular expression
                            m = re.search(stxt, line)
                            if m != None:
                                found = 1
                        if found:
                            line_cnt +=1
                            if itm == None: itm = self.addFileItem(filepath)
                            l = line.replace('\n','').replace('\r','')
                            litm = qtw.QTreeWidgetItem(['',str(cnt),l])
                            for i in range(3):
                                litm.setBackground(i,qtg.QColor(qtg.QColor(30,30,30)))
                            litm.setForeground(2,qtg.QColor(qtg.QColor(255,255,255)))
                            litm.setForeground(1,qtg.QColor(qtg.QColor(150,150,150)))
                            litm.setTextAlignment(1,2)
                            itm.addChild(litm)
                        if not self.ui.b_search.isChecked(): break
            except:
                pass
##                print('ERROR opening: '+filepath)
        
        if itm != None:
            itm.setText(1,str(line_cnt))
    
    def addFileItem(self,filename,lines = 0):
        pth,f = os.path.split(filename)
        if self.ui.ckbx_relpath.isChecked():
            relpath = os.path.relpath(pth,self.currentPath)
            if relpath == '.':
                relpath = ''
        else:
            relpath = pth
        itm = qtw.QTreeWidgetItem([f,str(lines),relpath])
        itm.path = filename
##        itm = qtw.QTreeWidgetItem([f,str(lines),pth])
        
        # Set icon
        if self.IDE != None:
            ext = f.split('.')[-1]
            if ext in ['png','jpg','jpeg','gif','bmp','ico']:
                ipth = filename
            elif ext in self.IDE.settings['extensions']:
                ext = self.IDE.settings['extensions'][ext]
                ipth = self.IDE.iconPath+'files/'+ext+'.png'
            else:
                ipth = self.IDE.iconPath+'files/source.png'
            
            itm.setIcon(0,qtg.QIcon(ipth))

        self.ui.tr_results.addTopLevelItem(itm)
        
        return itm
    
    def browse(self):
        npth = qtw.QFileDialog.getExistingDirectory(self,'Select directory to search',self.ui.le_path.text())
        if not npth == '': self.ui.le_path.setText(npth)
    
    def copyToClipboard(self):
        txt = ''
        for t in range(self.ui.tr_results.topLevelItemCount()):
            itm = self.ui.tr_results.topLevelItem(t)
            if txt != '': txt += '\n'
            txt += os.path.join(str(itm.text(2)),str(itm.text(0)))
        clip = qtw.QApplication.clipboard()
        clip.setText(txt)
        
    
    #---IDE Functions
    def itmDClick(self,itm,col):
        if self.IDE != None:
            if itm.parent() != None:
##                pth = os.path.join(str(itm.parent().text(2)),str(itm.parent().text(0)))
                ok = self.IDE.openFile(itm.parent().path)
                if ok: 
                    self.IDE.currentEditor().gotoLine(int(str(itm.text(1)))-1)
##            elif col > 0:
            else:
##                pth = os.path.join(str(itm.text(2)),str(itm.text(0)))
                self.IDE.openFile(itm.path)
            
    def changeWorkspace(self,wksp):
        if wksp != 'None':
            wksp = str(wksp)
            if self.IDE.workspaces[wksp]['basefolder'] != None:
                self.ui.le_path.setText(self.IDE.workspaces[wksp]['basefolder'])

def runui():
    app = qtw.QApplication(sys.argv)
    appui = Find_Files(pth = os.path.dirname(__file__))
    appui.ui.le_ext.setText('.py')
    appui.ui.le_search.setText('outline')
    appui.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    runui()

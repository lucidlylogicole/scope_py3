from . import spellcheck
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw

class Plugin(object):
    title = 'Spellcheck'
    location = None
    widget = None  # The widget for the plugin (set at getWidget)
    
    def __init__(self,parent=None):
        self.parent = parent
    
    def load(self):
        self.loadWidget()
        # Add button 
        btn = qtw.QPushButton()
        btn.setIcon(qtg.QIcon('icon.png'))
        btn.setProperty("class",  "toolbar toolbar-right toolbar-left")
        btn.setToolTip('Spellcheck the selected text')
        btn.setSizePolicy(qtw.QSizePolicy.Preferred,qtw.QSizePolicy.Expanding)
        layout = self.parent.ui.fr_toolbar.layout()
        layout.addWidget(btn,0,layout.columnCount()-1,qtc.Qt.AlignLeft,1)
        btn.clicked.connect(self.widget.toggle)
        
    def loadWidget(self):
        self.widget = spellcheck.SpellChecker(self.parent)

        return self.widget
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw
from PyQt5 import QtWebEngineWidgets as qte
from PyQt5.QtWebChannel import QWebChannel
import os, json, time

# QWebChannel Handler
class CallHandler(qtc.QObject):
    def __init__(self,parent):
        self.parent = parent
        qtc.QObject.__init__(self,parent)
        
    @qtc.pyqtSlot(bool)
    def textChanged(self,changed):
##        print('text changed1',self.parent)
        if self.parent.TEXT_CHANGE_EVENT:
            self.parent.parent.editorTextChanged(wdg=self.parent,check_text=0,changed=changed)
##        self.parent.editorTextChanged()
##        self.parent.Events.editorChanged.emit(self.parent)

    @qtc.pyqtSlot(list)
    def visibleLinesChanged(self,lines):
        self.parent.visibleLinesChanged(tuple(lines))

    @qtc.pyqtSlot(int)
    def zoom(self,dir):
        if dir == 0:
            self.parent.resetZoom()
        else:
            self.parent.zoom(self.parent.zoomIncrement*dir)

class Events(qtc.QObject):
    editorChanged = qtc.pyqtSignal(qtw.QWidget)
    visibleLinesChanged = qtc.pyqtSignal(qtw.QWidget,tuple)
    editorSaved = qtc.pyqtSignal(qtw.QWidget,tuple)

# Custom Webpage class
class WebPage(qte.QWebEnginePage):
    def __init__(self,parent=None):
        self.parent = parent
        qte.QWebEnginePage.__init__(self,parent)

##    def acceptNavigationRequest(self,url,typ,*args):
##        # Override link
##        if typ == qte.QWebEnginePage.NavigationTypeLinkClicked:
##            if url.toString().startswith('http'):
##                return 1
##            else:
##                return self.parent.urlClicked(url)
####                return 0
##        return 1

    def javaScriptConsoleMessage(self, code, msg, line, *args):
        '''QWebEnginePage that prints Javascript errors to stderr.'''
        typ = ''
        if code != 0: typ=' ERROR'
        print('js>>%s %s  (line %s)' % (typ, msg, line))

class Ace(qte.QWebEngineView):
    def __init__(self,parent=None,lang=None):
        qte.QWebEngineView.__init__(self,parent)
        self.parent = parent
        self.Events = Events() # Events
        self.waitD = {} # waiting dictionary
        self.waitD['loaded'] = 0
        self.initial_text = None
        self.zoomValue = 1.
        self.zoomIncrement = 0.1
        self.lastText = ''
        self.TEXT_CHANGE_EVENT = 1
        
        self.parent.Events.editorSaved.connect(self.saved)
        
##        self.event_filter = EventFilter(self)
####        self.event_filter.keyPress.connect(self.keyPressEvent)
##        self.installEventFilter(self.event_filter)
##        self.setFocusPolicy(qtc.Qt.StrongFocus)
####        self.installEventFilter(self)
        
        # Initial Variables
        self.wordwrapmode = 0
##        fnt=qtg.QFont()
##        fnt.setFamily('FreeMono')
##        self.setFont(fnt)
        
        # Setup Web Page
        self.web_page = WebPage(self)
        #web_page.setLinkDelegationPolicy(QtWebKit.QWebPage.DelegateAllLinks)
        self.setPage(self.web_page)
        
        # Javascript Inspector Troubleshooting
        if 0:
            self.web_page.settings().setAttribute(QtWebKit.QWebSettings.DeveloperExtrasEnabled, True)
            self.webInspector = QtWebKit.QWebInspector(self)
            self.webInspector.setPage(web_page)
            self.webInspector.setVisible(True)
        
        
        if os.name =='nt':
            pfx="file:///"
        else:
            pfx="file://"
        url = qtc.QUrl(pfx+os.path.abspath(os.path.dirname(__file__)).replace('\\','/')+'/editor.html')

##        self.setUrl(url)
##        qtw.QApplication.processEvents()
        
        f = open(os.path.abspath(os.path.dirname(__file__)).replace('\\','/')+'/editor.html','r')
        html = f.read()
        f.close()
        html = html.replace('$mode',lang)
        self.setHtml(html,url)
##        qtw.QApplication.processEvents()
        
        # Setup QWebChannel on web page
        self.channel = QWebChannel()
        self.handler = CallHandler(self)
        self.channel.registerObject('py_ace', self.handler)
        self.page().setWebChannel(self.channel)

        qtw.QApplication.processEvents()
        
        # Default settings
        self.settings = {
            'wrapBehaviours':1,
            'behaviours':1,
            'showPrintMargin':0,
            'fontSize':13,
            'theme':'twighlight',
            'newLineMode':'unix',
            'showWhitespace':'false',
            'fontFamily':'Liberation Mono,Courier',
            'autocomplete':'true',
            'enableSnippets':'true',
            'writerMode':0,
            'options':{},
        }
        
        # Load settings
        for ky in self.settings:
            if lang in self.parent.settings['prog_lang'] and ky in self.parent.settings['prog_lang'][lang]:
                self.settings[ky]=self.parent.settings['prog_lang'][lang][ky]
            elif ky in self.parent.settings['editors']['ace']:
                self.settings[ky]=self.parent.settings['editors']['ace'][ky]
##        # Setup Editor
##        js = "editor.getSession().on('change',function (e) {pythonjs.textChanged()});"
##        js += "editor.getSession().on('changeScrollTop',function (e) {pythonjs.visibleLinesChanged()});"
##        self.page().runJavaScript(js)
##        
##        self.setup()
##
##        recipient = self.focusProxy()
##        qtw.QApplication.postEvent(recipient, self.keyPressEvent)

        self.loadFinished.connect(self.setup)
    
    def setup(self):
        js=''
        self.waitD['loaded'] = 1
        if self.initial_text != None:
            self.setText(self.initial_text)
        
##            # Update Outline
##            if 'outline' in self.parent.pluginD:
##                self.parent.pluginD['outline'].widget.updateOutline()
        
        # Setup Editor
        js += "editor.getSession().on('change',textChanged);"
##        js += "editor.getSession().on('changeScrollTop',function (e) {window.py_ace.visibleLinesChanged()});"

        # Set Javascript settings
        js += 'editor.setTheme("ace/theme/'+self.settings['theme']+'");'
        js += 'editor.setWrapBehavioursEnabled('+['false','true'][self.settings['wrapBehaviours']]+');'
        js += 'editor.setBehavioursEnabled('+['false','true'][self.settings['behaviours']]+');'
        js += 'editor.setShowPrintMargin('+['false','true'][self.settings['showPrintMargin']]+');'
        js += 'editor.setFontSize('+str(self.settings['fontSize'])+');'
        js += 'editor.getSession().getDocument().setNewLineMode("'+str(self.settings['newLineMode'])+'");'
        js += 'editor.setShowInvisibles('+str(self.settings['showWhitespace'])+');'
        js += 'editor.focus();'
        js += 'editor.setOptions({fontFamily:"'+self.settings['fontFamily']+'"});'
        js += 'editor.setOptions({enableBasicAutocompletion: "'+self.settings['autocomplete']+'"});'
        js += 'editor.setOptions({enableSnippets: "'+self.settings['enableSnippets']+'"});'
        if self.settings['options'] != {}:
            js += 'editor.setOptions('+json.dumps(self.settings['options'])+');'
        
        self.page().runJavaScript(js)
        
        if self.settings['writerMode']:
            self.settings['writerMode']=0
            self.toggleWriterMode()
        
        self.gotoLine(0)

##        if self.initial_text != None:
##            # Update Outline
##            if 'outline' in self.parent.pluginD:
##                self.parent.pluginD['outline'].widget.updateOutline(toggle_view=1,txt=txt)


    def WAIT(self,var,val=None):
        # Wait for variable
        cnt = 1
        while self.waitD[var] == val:
            time.sleep(.01)
            cnt += 1
            if cnt > 100:break
##            print('sleeping',var)
            qtw.QApplication.processEvents()

    def editorTextChanged(self):
        self.Events.editorChanged.emit(self)
    
##    def eventFilter(self,obj, event):
##        print('event1',event)
##        return 1
    
##    def keyPressEvent(self,event):
##        # This is not firing
##        ky = event.key()
##        handled = 0
##        if ky in [qtc.Qt.Key_Enter,qtc.Qt.Key_Return,qtc.Qt.Key_Tab,qtc.Qt.Key_Backtab,qtc.Qt.Key_Delete,qtc.Qt.Key_Backspace,qtc.Qt.Key_Z,qtc.Qt.Key_Y]:
##            self.okedit = 0
##
##        if event.modifiers() & qtc.Qt.ControlModifier:
####            if event.key() == qtc.Qt.Key_C:
####                self.copy()
####                handled = 1
####            elif event.key() == qtc.Qt.Key_V:
####                self.paste()
####                handled = 1
####            elif event.key() == qtc.Qt.Key_X:
####                self.copy()
####                self.cut()
####                handled = 1
##            if event.key() == qtc.Qt.Key_D:
##                self.copyLinesDown()
##                handled = 1
##            elif event.key() == qtc.Qt.Key_Delete:
##                self.removeLines()
##                handled = 1
##            elif event.key() == qtc.Qt.Key_Plus:
##                self.zoom(self.zoomIncrement)
##                handled = 1
##            elif event.key() == qtc.Qt.Key_Minus:
##                self.zoom(-self.zoomIncrement)
##                handled = 1
##            elif event.key() == qtc.Qt.Key_0:
##                self.resetZoom()
##                handled=1
##        
##        if event.key() == qtc.Qt.Key_Meta:
##            # Ignore Windows key
##            handled = 1
##        
##        if event.modifiers() & qtc.Qt.AltModifier:
##            if event.key() == qtc.Qt.Key_Z:
##                self.toggleWriterMode()
##        
##        if not handled:
##            qte.QWebEngineView.keyPressEvent(self,event)
##        qtw.QApplication.processEvents()
##        self.okedit = 1
##        self.editorTextChanged()
##        return handled

    def wheelEvent(self, event):
        steps = event.angleDelta().y()/120
        handled = 0
        if event.modifiers() & qtc.Qt.ControlModifier:
            # Zoom
            if steps > 0:
                self.zoom(self.zoomIncrement)
            else:
                self.zoom(-self.zoomIncrement)
            handled = 1
        
        if not handled:
            qte.QWebEngineView.wheelEvent(self,event)
            qtw.QApplication.processEvents()

    def contextMenuEvent(self,event):
        menu = qtw.QMenu('ace menu',self.parent)
        
        # Edit Menu
        menuD=[['Cut','Ctrl+X'],['Copy','Ctrl+C'],['Paste','Ctrl+V'],['Select All','Ctrl+A']]
        for rw in menuD:
            if rw[0] in ['Select All']:
                menu.addSeparator()
            act = qtw.QAction(rw[0],menu)
            act.setShortcut(qtg.QKeySequence(rw[1],0))
            menu.addAction(act)

        menu.addSeparator()
        # Settings Menu
        smenu = qtw.QMenu('Ace',menu)
##        tmenu = qtw.QMenu('theme',smenu)
##        fld = os.path.abspath(os.path.dirname(__file__)).replace('\\','/')+'/src-noconflict/'
##        for f in sorted(os.listdir(fld)):
##            if f.startswith('theme'):
##                a=tmenu.addAction(qtg.QIcon(),f[6:-3])
##                a.setData('theme')
##        smenu.addMenu(tmenu)
        menu.addMenu(smenu)
        
        # Extra Settings
        smenu.addAction('Jump To Matching')
        act = qtw.QAction(qtg.QIcon(),'Behaviours Enabled',menu)
        act.setCheckable(1)
        act.setChecked(self.settings['behaviours'])
        smenu.addAction(act)
        act = qtw.QAction(qtg.QIcon(),'Wrap Behaviour Enabled',menu)
        act.setCheckable(1)
        act.setChecked(self.settings['wrapBehaviours'])
        smenu.addAction(act)
        
        act = qtw.QAction(qtg.QIcon(),'Toggle Writer Mode',menu)
        act.setCheckable(1)
        act.setChecked(self.settings['writerMode'])
        smenu.addAction(act)
        
        smenu.addSeparator()
        smenu.addAction('Settings')
        
        for act in menu.actions():  # Set Icon to visible
            act.setIconVisibleInMenu(1)
        
        # Launch Menu
        act = menu.exec_(self.cursor().pos())
        if act != None:
            acttxt = act.text()
            actdta = act.data()
            if actdta == 'theme':
                js = 'editor.setTheme("ace/theme/'+acttxt+'");'
                self.page().runJavaScript(js)
            elif acttxt == 'Copy':
                self.copy()
            elif acttxt == 'Cut':
                self.cut()
            elif acttxt == 'Paste':
                self.paste()
            elif acttxt == 'Select All':
                self.selectAll()
            elif acttxt == 'Jump To Matching':
                self.jumpToMatching()
            elif acttxt == 'Behaviours Enabled':
                self.toggleBehaviours()
            elif acttxt == 'Wrap Behaviour Enabled':
                self.toggleWrapBehaviours()
            elif acttxt == 'Toggle Writer Mode':
                self.toggleWriterMode()
            elif acttxt == 'Settings':
                self.page().runJavaScript('editor.execCommand("showSettingsMenu")')
        
    def copy(self):
        self.waitD['copying'] = 1
        js = "editor.session.getTextRange(editor.getSelectionRange())"
        self.page().runJavaScript(js,self._copy)
        
    def _copy(self,txt):
##        print('_copy',txt)
        clip = qtw.QApplication.clipboard()
        clip.setText(txt)
        self.waitD['copying'] = 0
    
    def cut(self):
        # Copy first
        self.copy()
        self.WAIT('copying',0)
        self.page().runJavaScript("editor.insert('')")
       
    def paste(self):
        clip = qtw.QApplication.clipboard()
        txt = clip.text()
        if txt != '':
            self.page().runJavaScript('editor.insert({0}.text);'.format(json.dumps({'text':txt})))

    def getText(self):
##        if not self.waitD['loaded']:
##            return ''
        self.waitD['get_text'] = None
        self.page().runJavaScript("getText()",self._getText)
        
        self.WAIT('get_text',None)

        return self.waitD['get_text']
    
    def _getText(self,result):
        # JS Callback for getText
        self.waitD['get_text'] = result
    
    def getSelectedText(self):
        self.waitD['selected_text'] = None
        self.page().runJavaScript("editor.getSelectedText()",self._getSelectedText)
        self.WAIT('selected_text',None)
        return self.waitD['selected_text']
    
    def _getSelectedText(self,txt):
        self.waitD['selected_text'] = txt 
    
    def saved(self):
        self.page().runJavaScript('saved()')
    
    def selectAll(self):
        self.page().runJavaScript('editor.selectAll();')
    
    def setText(self,txt):
        if not self.waitD['loaded']:
##            self.WAIT('loaded',0)
            self.initial_text = txt
            return
        self.page().runJavaScript(
        '''editor.session.setValue({0}.text);
        editor.clearSelection();
        editor.moveCursorTo(0,0);
        PREV_TEXT=editor.getValue();
        editor.focus();'''.format(json.dumps({'text':txt})))
        
        if 'outline' in self.parent.pluginD and hasattr(self,'id'):
            self.parent.pluginD['outline'].widget.updateOutline(wdg=self,toggle_view=self.parent.leftPluginVisible,txt=txt)

    def insertText(self,txt):
        self.page().runJavaScript(
        '''editor.insert({0}.text);'''.format(json.dumps({'text':txt})))

    def toggleWordWrap(self):
        self.wordwrapmode = not self.wordwrapmode
        ww = {1:'true',0:'false'}
        js = "editor.getSession().setUseWrapMode("+ww[self.wordwrapmode]+");"
        self.page().runJavaScript(js)
    
    # handled by ace
    def toggleComment(self):
        js = "editor.toggleCommentLines();"
        self.page().runJavaScript(js)
    
    def toggleWhitespace(self):
        js = "editor.setShowInvisibles(! editor.getShowInvisibles());"
        self.page().runJavaScript(js)
    
    def indent(self):
        js = "editor.indent();"
        self.page().runJavaScript(js)
    
    def unindent(self):
        js = "editor.blockOutdent();"
        self.page().runJavaScript(js)
    
    def copyLinesDown(self):
        js = "editor.copyLinesDown();"
        self.page().runJavaScript(js)
        
    def removeLines(self):
        js = "editor.removeLines();"
        self.page().runJavaScript(js)
        
    def find(self,txt,re=0,cs=0,wo=0):
    
        tre=tcs=two = 'false'
        if re: tre='true'
        if cs: tcs='true'
        if wo: two='true'
    
        js='''editor.find([txt].txt,{backwards:false,wrap:true,caseSensitive:'''.replace('[txt]',json.dumps({'txt':txt}))
        js += tcs+",wholeWord:"+two+",regExp:"+tre+"});"
        self.page().runJavaScript(js)
    
    def replace(self,ftxt,rtxt,re=0,cs=0,wo=0):
        self.replaceD = {
            'ftxt':ftxt,
            'rtxt':rtxt,
            're':re,
            'cs':cs,
            'wo':wo,
        }
        js = "editor.session.getTextRange(editor.getSelectionRange())"
        self.page().runJavaScript(js,self._replace)
    
    def _replace(self,ctxt):
        if ctxt.lower() == self.replaceD['ftxt'].lower():
            js = "editor.replace({0}.txt);".format(json.dumps({'txt':self.replaceD['rtxt']}))
            self.page().runJavaScript(js,self._replace_find)
    
    def _replace_find(self,val=None):
        self.find(self.replaceD['ftxt'],self.replaceD['re'],self.replaceD['cs'],self.replaceD['wo'])

    def replaceAll(self,ftxt,rtxt,re=0,cs=0,wo=0):
        tre=tcs=two = 'false'
        if re: tre='true'
        if cs: tcs='true'
        if wo: two='true'
        js = 'var repl_data = {0};'.format(json.dumps({'ftxt':ftxt,'rtxt':rtxt}))
        js += "editor.find(repl_data.ftxt,{backwards:false,wrap:true,caseSensitive:"+tcs+",wholeWord:"+two+",regExp:"+tre+"});"
        js += "editor.replaceAll(repl_data.rtxt);"
        self.page().runJavaScript(js)
        
    def gotoLine(self,line):
##        print('gotoline')
        js = "editor.gotoLine("+str(line+1)+");"
##        js = "editor.gotoLine("+str(line+1)+");window.py_ace.visibleLinesChanged([editor.getFirstVisibleRow(),editor.getLastVisibleRow()])"
##        js = "editor.scrollToLine("+str(line+1)+",0,0,function(){window.py_ace.visibleLinesChanged([editor.getFirstVisibleRow(),editor.getLastVisibleRow()])});"
##        print(js)
        self.page().runJavaScript(js)
##        qtw.QApplication.processEvents()
##        self.visibleLinesChanged()
##        self.getVisibleLines()
    
    def jumpToMatching(self):
        js = "editor.jumpToMatching();"
        self.page().runJavaScript(js)
    
    def toggleBehaviours(self):
        self.settings['behaviours'] = abs(self.settings['behaviours']-1)
        js = 'editor.setBehavioursEnabled('+['false','true'][self.settings['behaviours']]+');'
        self.page().runJavaScript(js)

    def toggleWrapBehaviours(self):
        self.settings['wrapBehaviours'] = abs(self.settings['wrapBehaviours']-1)
        js = 'editor.setWrapBehavioursEnabled('+['false','true'][self.settings['wrapBehaviours']]+');'
        self.page().runJavaScript(js)
    
    def toggleWriterMode(self):
        self.settings['writerMode'] = abs(self.settings['writerMode']-1)
        self.wordwrapmode = self.settings['writerMode']
        js = 'writerMode('+['false','true'][self.settings['writerMode']]+');'
        
        self.page().runJavaScript(js)
    
    def dropEvent(self,event):
        self.parent.dropEvent(event)
    
    #---Visible Lines
    def getVisibleLines(self):
        self.waitD['visible_lines'] = None
        if self.waitD['loaded']:
    ##        self.page().runJavaScript("pythonjs.getLines(getVisibleLines());")
            self.page().runJavaScript("getVisibleLines();",self._getVisibleLines)
            self.WAIT('visible_lines',None)
        else:
            return (0,0)
        return self.waitD['visible_lines']
        
    def _getVisibleLines(self,result):
        line_first = 0
        line_last = 0
        if result != None:
            res = result.split(',')
            line_first = int(res[0])
            line_last = int(res[1])
            self.Events.visibleLinesChanged.emit(self,(line_first,line_last))
        self.waitD['visible_lines'] = (line_first,line_last)
    
    def visibleLinesChanged(self,lines):
        self.Events.visibleLinesChanged.emit(self,lines)
    
    def getCursorPosition(self):
        self.waitD['curs_pos'] = None
        self.page().runJavaScript("getCurrentPosition()",self._getCursorPosition)
        self.WAIT('curs_pos',None)
        return self.waitD['curs_pos']
        
    def _getCursorPosition(self,pos):
        self.waitD['curs_pos'] = pos

    def zoom(self, zx):
        self.zoomValue += zx
        self.setZoomFactor(self.zoomValue)
        qtw.QApplication.processEvents()

    def resetZoom(self):
        self.setZoomFactor(1)
        self.zoomValue = 1

##class EventFilter(qtc.QObject):
##    keyPress = qtc.pyqtSignal(object)
##    
##    def __init__(self,parent):
##        self.parent = parent
##        qtc.QObject.__init__(self)
##    
##    def eventFilter(self, object, event):
####        print('event',event,event.type())
##        if event.type() == 51:
####            print('key')
##            return self.parent.keyPressEvent(event)
####            return 1
##        else:
##            return qtc.QObject.eventFilter(self, object, event)
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw
from .filebrowser_ui import Ui_Form
import os,sys, subprocess, time, webbrowser

class FileBrowser(qtw.QStackedWidget):
    def __init__(self,parent=None):
        self.ide = parent
        qtw.QStackedWidget.__init__(self,parent)
        self.workspaceD = {}
##        self.defaultPath = self.ide.settings['plugins']['filebrowser']['defaultPath']
##        ntree = self.addFilePage(self.defaultPath)

        ntree = self.addFilePage(self.ide.settings['plugins']['filebrowser']['defaultPath'])
        self.setCurrentWidget(ntree)

        # Set up Tool Menu
        self.ToolMenu = qtw.QMenu('Tools')
        self.ToolMenu.addAction(qtg.QIcon(self.ide.iconPath+'console.png'),'Open Console',self.openConsole)
        self.ToolMenu.addAction(qtg.QIcon(self.ide.iconPath+'console.png'),'Start Simple HTTP Server',self.startSimpleServer)

    def addFilePage(self,pth=None, expand_folders=[]):
##        print 'addfilepage',pth
        curdir = os.path.abspath('.')
        os.chdir(os.path.dirname(__file__))
        ntree = DirTree(self.ide,self)
        os.chdir(curdir)
        self.addWidget(ntree)
        if pth == None:
            # Grab 1st filebrowser path
            pth = self.widget(0).ui.le_root.text()
##            pth = self.defaultPath
        if not os.path.exists(pth):
            pth = os.path.expanduser('~')
        ntree.ui.le_root.setText(pth)
        ntree.item_folder_path = pth
        ntree.loadRoot(expand_folders)
        return ntree
        
    def changeWorkspace(self,wksp):
        wksp = str(wksp)
        if wksp == 'None':
            self.setCurrentIndex(0)
        else:
            if wksp in self.workspaceD:
                self.setCurrentWidget(self.workspaceD[wksp])
        
    def openWorkspace(self,wksp):
        wksp = str(wksp)
        wD=self.ide.workspaces[wksp]

        # Expand Folders
        expand_folders = []
        if 'filebrowser' in wD:
            if 'expanded' in wD['filebrowser']:
                expand_folders = wD['filebrowser']['expanded']
        
        self.workspaceD[wksp]=self.addFilePage(wD['basefolder'],expand_folders)
        self.workspaceD[wksp].wksp_id = wksp
        self.setCurrentWidget(self.workspaceD[wksp])
    
    def closeWorkspace(self,wksp):
        wksp = str(wksp)
        if wksp in self.workspaceD:
            wdg = self.workspaceD[wksp]
            self.removeWidget(wdg)
            self.workspaceD.pop(wksp)
            wdg.deleteLater()
            del wdg

    def beforeSaveWorkspace(self,wksp,wD):
        wksp = str(wksp)
        # Check for open top levels
        if str(wksp) in self.workspaceD:
            wdg = self.workspaceD[wksp]
            root_open = []
            for i in range(wdg.ui.tr_dir.topLevelItemCount()):
                titm = wdg.ui.tr_dir.topLevelItem(i)
                if titm.isExpanded():
                    root_open.append(str(titm.text(0)))
            
            if root_open != []:
                wD['filebrowser']={'expanded':root_open}

    def togglePlugin(self):
##        if self.ide.ui.fr_left.isHidden():
####            self.ide.ui.fr_left.setVisible(1)
##            self.ide.toggleLeftPlugin(show=1,check_button=0)
##        i=self.ide.ui.tab_left.indexOf(self.ide.pluginD['filebrowser'].widget)
##        self.ide.ui.tab_left.setCurrentIndex(i)
        self.ide.showLeftPlugin('filebrowser')
        wdg = self.currentWidget()
        wdg.ui.tr_dir.setFocus()
##        self.ide.pluginD['filebrowser'].btn.setChecked(1)

    def menuEvent(self,event):
        wdg = self.currentWidget()
        wdg.fileMenu(event)

    #---Tool Functions
    def openConsole(self):
        self.ide.launchInConsole(cmd='',path=self.currentWidget().item_folder_path)
    
    def startSimpleServer(self):
        port = '8000'
        res,ok = qtw.QInputDialog.getText(self,'Start Simple Server','Enter Port',qtw.QLineEdit.Normal,port)
        if ok:
            port = str(res)
            py_path = sys.executable
            if py_path.endswith('pythonw.exe'):
                py_path = py_path.replace('pythonw.exe','python.exe')
            self.ide.launchInConsole(cmd=py_path+' -m http.server '+port+' --bind 127.0.0.1',path=self.currentWidget().item_folder_path)
            
            time.sleep(0.5)
            
            webbrowser.open('http://127.0.0.1:'+port)

class DirTree(qtw.QWidget):
    def __init__(self,parent,MainFileBrowser):
        qtw.QWidget.__init__(self,parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ide = parent
        self.MainFileBrowser = MainFileBrowser
        self.wksp_id = None
        self.extD = self.ide.settings['extensions']
        
        # Show All
        self.showAll=0
        if 'showAll' in self.ide.settings['plugins']['filebrowser']:
            self.showAll = self.ide.settings['plugins']['filebrowser']['showAll']
        
        self.ui.tr_dir.itemDoubleClicked.connect(self.itmClicked)
        self.ui.tr_dir.itemExpanded.connect(self.itmExpanded)
        self.ui.tr_dir.mousePressEvent = self.mousePressEvent
        self.ui.le_root.returnPressed.connect(self.loadRoot)
        self.ui.b_browse.clicked.connect(self.browse)
        
        # Set default path
##        self.ui.le_root.setText(os.path.expanduser('~')) # Start with home directory
##        self.loadRoot()
        
        self.ui.tr_dir.contextMenuEvent = self.fileMenu
        self.ui.tr_dir.keyPressEvent = self.ikeyPressEvent

    def viewFileBrowser(self):
        if self.ide.ui.fr_left.isHidden():
##            self.ide.ui.fr_left.setVisible(1)
            self.ide.toggleLeftPlugin(show=1)
        i=self.ide.ui.tab_left.indexOf(self)
        self.ide.ui.tab_left.setCurrentIndex(i)
    
    def browse(self):
        npth = qtw.QFileDialog.getExistingDirectory(self,'Select the root directory',self.ui.le_root.text())
        if not npth == '': 
            self.ui.le_root.setText(npth)
            self.loadRoot()
    
    def loadRoot(self,expanded_folders=[]):
        newpath = os.path.abspath(str(self.ui.le_root.text())).replace('\\','/')
        if not newpath.endswith('/'): newpath +='/'
        if os.path.exists(newpath):
            self.rootpath = newpath
        
        self.ui.le_root.setText(self.rootpath)
        self.ui.tr_dir.clear()
        
        # Set Scope path
        self.ide.currentPath = self.rootpath
        if self.ide.currentWorkspace != None:
            self.ide.workspaces[self.ide.currentWorkspace]['basefolder'] = self.rootpath
        
        # Add up directory
        citm = qtw.QTreeWidgetItem(['..','..'])
        citm.setIcon(0,qtg.QIcon(self.ide.iconPath+'up.png'))
        self.ui.tr_dir.addTopLevelItem(citm)
        
        # Add to Tree
        dircontents,filecontents = self.getDirContents(self.rootpath)
        for citm in dircontents:
            self.ui.tr_dir.addTopLevelItem(citm)
            if str(citm.text(0)) in expanded_folders:
                self.itmClick(citm,0)
        
        for citm in filecontents:
            self.ui.tr_dir.addTopLevelItem(citm)
        
        if self.wksp_id != None:
            self.ide.workspaces[self.wksp_id]['basefolder'] = self.rootpath
        
    def itmClicked(self,itm,col):
        # Tree Click Signals
        self.itmClick(itm,col)
    
    def itmClick(self,itm,col,toggleExpanded=1):
        pth = str(itm.text(1))
        
        if pth == '..':
            # Go up a directory
            self.ui.le_root.setText(self.rootpath+'../')
            self.loadRoot()
        else:
            itm.takeChildren()
            if os.path.isdir(pth):
                dircontents,filecontents = self.getDirContents(pth)
                for citm in dircontents:
                    itm.addChild(citm)
                
                for citm in filecontents:
                    itm.addChild(citm)
                
                if toggleExpanded==2:
                    itm.setExpanded(1)
                elif toggleExpanded:
                    itm.setExpanded(not itm.isExpanded())
                
                # Toggle Image
                icn = qtg.QIcon(self.ide.iconPath+'folder.png')
                
                if itm.isExpanded():
                    icn = qtg.QIcon(self.ide.iconPath+'folder_open.png')
                
                itm.setIcon(0,icn)
                
            else:
                if self.ide != None:
                    if toggleExpanded == 2:
                        if itm.parent() != None:
                            self.itmClick(itm.parent(),0,toggleExpanded=toggleExpanded)
                        else:
                            self.loadRoot()
                    else:
                        self.ide.openFile(pth)
    
    def mousePressEvent(self, event):
        self.ui.tr_dir.clearSelection()
        self.ui.tr_dir.setCurrentItem(None)
        qtw.QTreeView.mousePressEvent(self.ui.tr_dir, event)
    
    def itmExpanded(self,itm):
        pth = str(itm.text(1))
        
    def getDirContents(self,pth):
        # Return [dirlist,filelist]
        dircontents = []
        filecontents = []
        
        if os.path.isdir(pth):
            if not pth.endswith('/'): pth += '/'
            try:
                dirlist = sorted(os.listdir(os.path.abspath(pth)))
            except:
                qtw.QMessageBox.warning(self,'No Access','The folder does not exist or you do not have access to open it')
                dirlist = []
            # Add Folders
            for f in dirlist:
                citm = qtw.QTreeWidgetItem([f,pth+f])
                if not f.startswith('.') and  os.path.isdir(pth+f) and f != '__pycache__':
                    citm.setIcon(0,qtg.QIcon(self.ide.iconPath+'folder.png'))
                    dircontents.append(citm)
            # Add Files
            for f in sorted(dirlist, key=lambda s: s.lower()):
                citm = qtw.QTreeWidgetItem([f,pth+f])
                citm.setToolTip(0,f)
                
                ext = os.path.splitext(f)[1][1:]
                if not os.path.isdir(pth+f) and ((not f.startswith('.') and ext in self.extD) or self.showAll):
                    
                    ipth = ''
                    if ext in ['png','jpg','jpeg','gif','bmp','ico']:
                        ipth = pth+f
                    elif ext in self.extD:
                        ipth = self.ide.iconPath+'files/'+self.extD[ext]+'.png'
                    if os.path.exists(ipth):
                        citm.setIcon(0,qtg.QIcon(ipth))
                    elif os.path.exists(self.ide.iconPath+'files/'+ext+'.png'):
                        citm.setIcon(0,qtg.QIcon(self.ide.iconPath+'files/'+ext+'.png'))
                    else:
                        citm.setIcon(0,qtg.QIcon(self.ide.iconPath+'files/source.png'))
                    filecontents.append(citm)

        return dircontents,filecontents

    def ikeyPressEvent(self,event):
        ky = event.key()
        handled = 0
##        print ky
        if ky in [qtc.Qt.Key_Enter,qtc.Qt.Key_Return]:
            self.itmClicked(self.ui.tr_dir.currentItem(),0)
            handled = 1
##        elif ky == qtc.Qt.Key_F2:
##            # Doesn't work cause of other shortcuts
##            self.ui.tr_dir.editItem(self.ui.tr_dir.currentItem(),0)
##            handled = 1
        if not handled:
            qtw.QTreeWidget.keyPressEvent(self.ui.tr_dir,event)
    
    def filename_edit(self):
        self.ui.tr_dir.editItem(self.ui.tr_dir.currentItem(),0)
        
    def fileMenu(self,event):
        menu = qtw.QMenu('file menu')
        open_icon = qtg.QIcon(self.ide.iconPath+'folder_go.png')
        citm = self.ui.tr_dir.currentItem()
        fitm = None
        isfile = 0
##        self.item_file_path = None
        open_txt = 'View &Folder'
        if citm != None:
            pth = str(citm.text(1))
            self.item_file_path = pth
            fpth = pth # Folder path
            fitm = citm
            ext = os.path.splitext(pth)[1][1:]
            if os.path.isfile(pth):
                fpth = os.path.dirname(pth)
                fitm = citm.parent()
                isfile = 1
            
                open_icon = qtg.QIcon(self.ide.iconPath+'file_go.png')
                open_txt = '&Open (external)'
        else:
            fpth = self.ui.le_root.text()
            pth = fpth

        self.item_folder_path = fpth

        menu.addAction(open_icon,open_txt)
        menu.addSeparator()

        # New File
        menu.addAction(qtg.QIcon(self.ide.iconPath+'new.png'),'&New File')
        menu.addAction(qtg.QIcon(self.ide.iconPath+'folder_add.png'),'New &Folder')
        menu.addSeparator()

        if citm != None:

            omenu = qtw.QMenu('Edit With')
            for edtr in sorted(self.ide.editorD.keys()):
                omenu.addAction(qtg.QIcon(self.ide.editorPath+'/'+edtr+'/'+edtr+'.png'),edtr)
            menu.addMenu(omenu)
            
        # Tool Menu
        menu.addMenu(self.MainFileBrowser.ToolMenu)
        
        if citm != None:
            if os.path.isfile(pth):
                menu.addSeparator()
                menu.addAction(qtg.QIcon(self.ide.iconPath+'edit.png'),'&Rename')
                menu.addAction(qtg.QIcon(self.ide.iconPath+'delete.png'),'&Delete File')
            
            for act in menu.actions():  # Set Icon to visible
                act.setIconVisibleInMenu(1)

        menu.addSeparator()
        menu.addAction(qtg.QIcon(self.ide.iconPath+'refresh.png'),'Refresh')
        # Show All files
        showAct=menu.addAction(qtg.QIcon(),'Show All Files')
        showAct.setCheckable(1)
        showAct.setChecked(self.showAll)
        if not isfile:
            menu.addAction('Set As Root Path')
        
        # Launch Menu
        act = menu.exec_(self.ui.tr_dir.cursor().pos())
        if act != None:
            #---Menu Actions
            acttxt = str(act.text())
            if acttxt == 'Edit':
                # Open File
                self.openFile()
            elif acttxt == '&Open (external)' or acttxt == 'View &Folder':
                externalFileBrowser = None
                fbsD = self.ide.settings['plugins']['filebrowser']
                if os.path.isdir(pth) and 'externalFileBrowser' in fbsD and fbsD['externalFileBrowser']!='':
                    externalFileBrowser = fbsD['externalFileBrowser']
                self.ide.openFileExternal(pth,externalFileBrowser)
            elif acttxt == 'Show All Files':
                self.showAll = showAct.isChecked()
                if citm != None:
                    self.itmClick(citm,0,toggleExpanded=2)
                else:
                    self.loadRoot()
            elif acttxt=='Refresh':
                if citm != None:
                    self.itmClick(citm,0,toggleExpanded=2)
                else:
                    self.loadRoot()
            elif acttxt == 'Set As Root Path':
                if citm != None:
                    pth = citm.text(1)
                    self.ui.le_root.setText(pth)
                    self.loadRoot()
            elif acttxt == '&New File':
                # New File
                resp,ok = qtw.QInputDialog.getText(self.ide,'New File','Enter the file name and extension.')
                if ok and not resp == '':
                    if os.path.exists(fpth+'/'+resp):
                        qtw.QMessageBox.warning(self,'File Exists','That file already exists')
                    else:
                        f = open(fpth+'/'+resp,'w')
                        f.close()
                        if fitm != None:
                            self.itmClick(fitm,0,toggleExpanded=0)
                            fitm.setExpanded(1)
                        else:
                            self.loadRoot()
            elif acttxt == 'New &Folder':
                # New Folder
                resp,ok = qtw.QInputDialog.getText(self.ide,'New Folder','Enter the folder name.')
                if ok and not resp == '':
                    if os.path.exists(fpth+'/'+resp):
                        qtw.QMessageBox.warning(self,'Folder Exists','That folder already exists')
                    else:
                        os.mkdir(fpth+'/'+resp)
                        if fitm != None:
                            self.itmClick(fitm,0,toggleExpanded=0)
                            fitm.setExpanded(1)
                        else:
                            self.loadRoot()
            elif acttxt == '&Rename':
                # Rename the file
                fileopen = self.ide.isFileOpen(pth)
                rename = 1
                if fileopen != -1:
                    rename = 0
                    resp = qtw.QMessageBox.warning(self,'File is open','The file is currently open and needs to close in order to rename.<br><br>Do you want to close the file now?',qtw.QMessageBox.Yes | qtw.QMessageBox.No)
                    if resp == qtw.QMessageBox.Yes:
                        rename = 1
                        self.ide.closeTab(fileopen)
                
                if rename:
                    resp,ok = qtw.QInputDialog.getText(self.ide,'Rename File','Enter the file name and extension.',qtw.QLineEdit.Normal,os.path.split(pth)[1])
                    if ok and not resp == '':
                        newpth = fpth+'/'+resp
                        if os.path.exists(newpth):
                            qtw.QMessageBox.warning(self,'File Exists','That file already exists')
                        else:
                            os.rename(pth,newpth)
                            if fitm != None:
                                self.itmClick(fitm,0,toggleExpanded=0)
                                fitm.setExpanded(1)
                            else:
                                self.loadRoot()
            elif acttxt == '&Delete File':
                # Delete File
                fileopen = self.ide.isFileOpen(pth)
                delfile = 1
                if fileopen != -1:
                    delfile = 0
                    resp = qtw.QMessageBox.warning(self,'File is open','The file is currently open and needs to close in order to delete.<br><br>Do you want to close the file now?',qtw.QMessageBox.Yes | qtw.QMessageBox.No)
                    if resp == qtw.QMessageBox.Yes:
                        delfile = 1
                        self.ide.closeTab(fileopen)
                
                if delfile:
                    resp = qtw.QMessageBox.warning(self,'Delete File','Are you sure you want to delete the file:<br><br>'+pth,qtw.QMessageBox.Yes | qtw.QMessageBox.No)
                    if resp == qtw.QMessageBox.Yes:
                        os.remove(pth)
                        if fitm != None:
                            self.itmClick(fitm,0,toggleExpanded=0)
                            fitm.setExpanded(1)
                        else:
                            self.loadRoot()
            elif acttxt in self.ide.editorD:
                self.ide.openFile(pth,editor=acttxt)
    
    def openFile(self):
        itm = self.ui.tr_dir.currentItem()
        if itm != None:
            pth = str(itm.text(1))
            self.itmClicked(itm,0)

def runui():
    app = qtw.QApplication(sys.argv)
    appui = DirTree()
    appui.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    runui()

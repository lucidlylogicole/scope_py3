from . import filebrowser
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw

class Plugin(object):
    title = 'Files'
    location = 'left'
    widget = None  # The widget for the plugin (set at loadWidget)
    
    def __init__(self,parent=None):
        self.parent = parent
    
    def load(self):
        '''Called when loading the plugin'''
        
    def loadWidget(self):
        self.widget = filebrowser.FileBrowser(self.parent)

        self.parent.Events.workspaceOpened.connect(self.widget.openWorkspace)
        self.parent.Events.workspaceChanged.connect(self.widget.changeWorkspace)
        self.parent.Events.workspaceClosed.connect(self.widget.closeWorkspace)
        self.parent.Events.workspaceBeforeSave.connect(self.widget.beforeSaveWorkspace)
        
        qtw.QShortcut(qtc.Qt.ALT+qtc.Qt.Key_F,self.parent,self.widget.togglePlugin) 
        
        return self.widget
    
    def toggle(self):
        self.widget.togglePlugin()
    
    def contextMenu(self, event):
        self.widget.menuEvent(event)
import sys, os
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw

class WorkspaceWidget(qtw.QListWidget):
    def __init__(self,parent=None):
        qtw.QListWidget.__init__(self)
        self.ide = parent
        self.setFlow(0)
        self.setWrapping(1)
        self.setResizeMode(1)
        self.setDragDropMode(4) # internalmove
        self.setDragEnabled(1) # enable drag drop
        self.setProperty("class",'editor_tab')
        self.setSpacing(0)
        self.clicked.connect(self.select)
        self.tabD = {}
    
    def addEditorTab(self,file_id,title,filename,editor=''):
        if not file_id in self.tabD:
            etab = EditorTab(self.ide,file_id,title,filename,editor)
            itm = qtw.QListWidgetItem()
            itm.setSizeHint(etab.sizeHint())
            if filename != None:
                itm.setToolTip(filename)
            
            self.addItem(itm)
            etab.item = itm
            self.setItemWidget(itm,etab)
            itm.setSizeHint(etab.sizeHint())
            self.tabD[file_id]=etab
            tab = etab
            tab.run_command = None
        else:
            tab = self.tabD[file_id]
            
        return tab
    
    def select(self,m_ind,hide_tabs=1):
        itm = self.itemFromIndex(m_ind)
        wdg = self.itemWidget(itm)
        file_id=wdg.id

        ok = self.ide.openFile(file_id=file_id)
        if not ok:
            resp = qtw.QMessageBox.warning(self,'File not Found','This file no longer exists or there was an error opening it<br><br>Do you want to remove the tab?',qtw.QMessageBox.Yes | qtw.QMessageBox.No)
            if resp == qtw.QMessageBox.Yes:
                wdg.closeClick()
            self.ide.TabWindow.toggle(1)
        else:
            if hide_tabs:
                self.ide.TabWindow.toggle(0)
    
    def rightClick(self,event):
        ind = self.indexAt(event.pos())
        itm = self.itemFromIndex(ind)
        current_ind = self.currentIndex()
        
        # Right Click Menu
        menu = qtw.QMenu()
        
        if itm != None:
            wdg = self.itemWidget(itm)
            
            icn = qtg.QIcon(self.ide.iconPath+'tri_right.png')
            menu.addAction(icn,'&Run')

            icn = qtg.QIcon(self.ide.iconPath+'compile.png')
            menu.addAction(icn,'&Compile')

            icn = qtg.QIcon(self.ide.iconPath+'file_go.png')
            menu.addAction(icn,'&Open (external)')
            
            icn = qtg.QIcon(self.ide.iconPath+'close_blue.png')
            menu.addAction(icn,'Clos&e')
            
            menu.addSeparator()
        menu.addAction('Close All')
        
        act = menu.exec_(self.cursor().pos())
        if act != None:
            acttxt = str(act.text())
            
            if acttxt == 'Clos&e':
                wdg.closeClick()
            elif acttxt == '&Run':
                if wdg.id not in self.ide.fileOpenD:
                    self.select(ind)
                self.ide.editorRun(self.ide.fileOpenD[wdg.id])
                if current_ind != None:
                    self.select(current_ind)
            elif acttxt == '&Compile':
                if wdg.id not in self.ide.fileOpenD:
                    self.select(ind)
                self.ide.editorCompile(self.ide.fileOpenD[wdg.id])
                if current_ind != None:
                    self.select(current_ind)
            elif acttxt == '&Open (external)':
                self.ide.TabWindow.toggle(0)
                self.ide.openFileExternal(wdg.filename)
            elif acttxt == 'Close All':
                for fid in self.tabD.keys():
                    self.tabD[fid].closeClick()

    def mousePressEvent(self, event):
        handled = 0
        ind = self.indexAt(event.pos())
        btn = event.button()
        if btn == 1: #left
            handled = 0
        elif btn == 4: # middle
            itm = self.itemFromIndex(ind)
            wdg = self.itemWidget(itm)
##            wdg.close()
            wdg.closeClick()
            handled = 1
        elif btn == 2: # right
            self.rightClick(event)
            handled = 1
        
        if not handled:
            qtw.QListWidget.mousePressEvent(self,event)

    def keyPressEvent(self,event):
        ky = event.key()
        handled = 0
        cind = self.currentRow()
        current_ind = self.currentIndex()
        tind = self.ide.TabWindow.tabs.currentIndex() # Current workspace tab
        itm = self.itemFromIndex(self.currentIndex())
        wdg = self.itemWidget(itm)
        if event.modifiers() & qtc.Qt.AltModifier:
            if ky == qtc.Qt.Key_Left:
                # Change workspaces
                tind -=1
                if tind <0:
                    tind = self.ide.TabWindow.tabs.count()-1
                self.ide.TabWindow.tabs.setCurrentIndex(tind)
                self.ide.TabWindow.tabs.currentWidget().setFocus()
                handled = 1
            elif ky == qtc.Qt.Key_Right:
                tind +=1
                if tind >= self.ide.TabWindow.tabs.count():
                    tind = 0
                self.ide.TabWindow.tabs.setCurrentIndex(tind)
                self.ide.TabWindow.tabs.currentWidget().setFocus()
                handled = 1
        if event.modifiers() & qtc.Qt.ControlModifier:
            if event.modifiers() & qtc.Qt.ShiftModifier and ky == qtc.Qt.Key_W:
                self.ide.TabWindow.closeWorkspaceTab(self.ide.TabWindow.tabs.currentIndex())
                handled = 1
            elif ky == qtc.Qt.Key_W:
                wdg.closeClick()
                # Close current tab
                handled = 1
            elif ky == qtc.Qt.Key_PageDown:
                self.ide.TabWindow.nextTab()
                handled = 1
            elif ky == qtc.Qt.Key_PageUp:
                self.ide.TabWindow.previousTab()
                handled = 1
                
        elif ky in [qtc.Qt.Key_Enter,qtc.Qt.Key_Return]:
            handled = 1
            self.select(self.currentIndex())
        elif ky == qtc.Qt.Key_Right:
            cind +=1
            if cind >= self.count():
                cind = 0
            self.setCurrentRow(cind)
            handled = 1
        elif ky == qtc.Qt.Key_Left:
            cind -=1
            if cind < 0:
                cind = self.count()-1
            self.setCurrentRow(cind)
            handled = 1
            
        if not handled:
            qtw.QListWidget.keyPressEvent(self,event)

class EditorTab(qtw.QWidget):
    def __init__(self,ide,file_id,title,filename,editor=''):
        qtw.QWidget.__init__(self)
        self.id = file_id
        self.filename = filename
        self.ide = ide
        self.pluginEditor = editor
        
        # Layout
        layout = qtw.QHBoxLayout()
        layout.setContentsMargins(4,2,2,2)
        layout.setSpacing(4)
        
        if file_id in self.ide.fileOpenD:
            wdg = self.ide.fileOpenD[file_id]
            img = wdg.pic
        else:
            img = qtg.QPixmap(self.ide.getIconPath(filename))
        img2 = img.scaledToHeight(20,qtc.Qt.SmoothTransformation)
        icn_lbl = qtw.QLabel()
        icn_lbl.setPixmap(img2)
        layout.addWidget(icn_lbl)
        
        # File Text
        lbl = qtw.QLabel(title)
        lbl.setProperty("class",'editor_tab')
        lbl.setSizePolicy(qtw.QSizePolicy(qtw.QSizePolicy.MinimumExpanding, qtw.QSizePolicy.Preferred))
        lbl.updateGeometry()
        layout.addWidget(lbl)
        self.titleLabel = lbl
        
        # Close Button
        cls_btn = qtw.QPushButton()
        cls_btn.clicked.connect(self.closeClick)
        cls_btn.setMaximumWidth(22)
        cls_btn.setProperty("class",'editor_tab_cls_btn')
        layout.addWidget(cls_btn)
        self.closeButton = cls_btn
        
        self.setLayout(layout)
    
    def setTitle(self,title):
        self.titleLabel.setText(title)
        self.item.setSizeHint(self.sizeHint())
    
    def closeClick(self):
        li = self.parent().parent()
        id = self.id
        ok = li.ide.closeTab(id,remove_from_workspace=1)
        if ok:
            if li.currentRow() >-1:
                li.select(li.currentIndex(),hide_tabs=0)
            li.ide.TabWindow.toggle(1)
        else:
            li.ide.TabWindow.toggle(1)
            
    def close(self):
        li = self.parent().parent()
        
        id = self.id

        li.takeItem(li.row(self.item))
        if id in li.tabD:
            li.tabD.pop(id)
        
class TabWindow(object):
    def __init__(self,parent=None,hidden_mode=0):
        self.tabs = qtw.QTabWidget(parent)
        self.tabs.setProperty("class","tabWindow")
        self.tabs.tabBar().setProperty("class","tabWindow")

        self.ide = parent
        self.hidden_mode = hidden_mode  # Auto hide = 1 or always visible = 0
        
        self.RECENT_TAB_INDEX = 0  # Keep track of current recent tab index
        self.TAB_MODE = 0  # Tab mode is where ctrl tab is toggling through recent files
        
        # Setup tab widget
        self.tabs.setTabPosition(qtw.QTabWidget.South)
        self.tabs.setTabsClosable(True)
        self.tabs.setMovable(True)
        self.tabs.setObjectName('editorTabBar')
        self.tabs.setSizePolicy(qtw.QSizePolicy(qtw.QSizePolicy.Expanding,qtw.QSizePolicy.Preferred))
    
        self.tabs.setWindowFlags(qtc.Qt.FramelessWindowHint | qtc.Qt.Popup)
        self.tabs.setWindowModality(qtc.Qt.NonModal)
    
        # Signals
        self.tabs.currentChanged.connect(self.changeWorkspace)
        self.tabs.tabCloseRequested.connect(self.closeWorkspaceTab)
        
        self.tabs.keyPressEvent = self.keyPressEvent
        self.tabs.keyReleaseEvent = self.keyReleaseEvent
        self.tabs.closeEvent = self.closeDialog
        
        # sign up for events
        self.ide.Events.workspaceClosed.connect(self.closeWorkspace)
    
    def keyPressEvent(self,event):
        handled = 0
        ky = event.key()
        if ky == qtc.Qt.Key_F1 or ky == qtc.Qt.Key_Escape or ((ky == qtc.Qt.Key_1 or ky == qtc.Qt.Key_QuoteLeft) and event.modifiers() & qtc.Qt.ControlModifier):
            self.toggle(0)
            handled = 1
        elif event.modifiers() & qtc.Qt.ControlModifier and ky == qtc.Qt.Key_Tab:
            self.nextRecentTab()
            handled = 1
        elif event.modifiers() & qtc.Qt.ControlModifier & qtc.Qt.ShiftModifier and ky == qtc.Qt.Key_W:
            self.closeWorkspaceTab(self.tabs.currentIndex())
            handled = 1
        
        if not handled:
            qtw.QTabWidget.keyPressEvent(self.tabs,event)

    def keyReleaseEvent(self,event):
        handled = 0
        ky = event.key()
        if ky == qtc.Qt.Key_Control and self.TAB_MODE:
            self.tabs.currentWidget().select(self.tabs.currentWidget().currentIndex())
            handled = 1
        
        if not handled:
            qtw.QTabWidget.keyReleaseEvent(self.tabs,event)
            
    def closeDialog(self,event):
        pass
##        self.ide.ui.b_show_tabs.setChecked(0)
    
    def addWorkspace(self,name):
        ww = WorkspaceWidget(parent=self.ide)
        self.tabs.addTab(ww,qtg.QIcon(self.ide.iconPath+'workspace.png'),name)
        self.tabs.setCurrentWidget(ww)
        return ww

    def changeWorkspace(self,ind,change_event=1):
        if ind == -1:
            self.ide.workspaceChange(None)
        else:
            self.ide.workspaceChange(str(self.tabs.tabText(ind)), change_tab=0)
            if not self.TAB_MODE:
                wwdg = self.tabs.widget(ind)
                if wwdg.currentRow() >-1:
                    wwdg.select(wwdg.currentIndex(),hide_tabs=0)
                else:
                    self.ide.showHome()

    def closeWorkspaceTab(self,ind):
        wksp = str(self.tabs.tabText(ind))
        ok = self.ide.workspaceClose(wksp)
    
    def closeWorkspace(self,wksp):
        for i in range(self.tabs.count()):
            if str(self.tabs.tabText(i)) == wksp:
                self.tabs.removeTab(i)
                break
        
        if self.tabs.count()==0:
            if self.hidden_mode: self.tabs.hide()
##            self.ide.ui.b_show_tabs.setChecked(0)
            if 'home' in self.ide.pluginD:
                self.ide.showHome()
    
    def show(self):
        h=self.ide.settings['tabWindow']['height'] # default height
        if self.ide != None:
            g=self.ide.geometry()
            
            if h > g.height():
                h = g.height()
            if self.ide.ui.fr_topbar.isVisible():
                dy = self.ide.ui.fr_topbar.height()
            else:
                dy = 0
            if self.ide.ui.fr_leftbar.isVisible():
                tlw = self.ide.ui.fr_leftbar.width()  # Left toolbar width
            else:
                tlw = 0
            self.tabs.setGeometry(g.x()+tlw,g.y()+dy,g.width()-tlw,h)
        else:
            self.tabs.setGeometry(20,20,500,h)
        
        self.tabs.show()
    
    def toggle(self,mode=None,ignore_button=0):
        self.RECENT_TAB_INDEX = 0
        if mode == None:
            mode = not self.tabs.isVisible()
            
        if mode:
            if self.hidden_mode: self.show()
            
            if self.tabs.currentWidget() != None:
                self.highlightCurrent()
        elif self.hidden_mode:
            self.tabs.hide()
            self.TAB_MODE = 0
        
        # Check button
##        if not ignore_button:
##            self.ide.ui.b_show_tabs.setChecked(mode)

    def highlightCurrent(self):
        # Highlight current file
        if self.ide.currentEditor() != None:
            self.tabs.currentWidget().setFocus()
            fid = self.ide.currentEditor().id
            
            li = self.tabs.currentWidget()
            if li != None:
                # Look for file to highlight
                for i in range(li.count()):
                    litm = li.itemWidget(li.item(i))
                    if litm.id == fid:
                        li.setCurrentRow(i)
                        break

    def nextTab(self):
        wdg = self.tabs.currentWidget()
        cind = wdg.currentRow()
        # Next Tab
        cind +=1
        if cind >= wdg.count():
            cind = 0
        self.TAB_MODE = 1
        wdg.setCurrentRow(cind)
        
    def previousTab(self):
        wdg = self.tabs.currentWidget()
        cind = wdg.currentRow()
        # Previous Tab
        cind -=1
        if cind < 0:
            cind = wdg.count()-1
        self.TAB_MODE = 1
        wdg.setCurrentRow(cind)

    def nextRecentTab(self):
        if self.ide.currentEditor().type != 'app' or self.TAB_MODE:
            self.RECENT_TAB_INDEX+=1
        if self.RECENT_TAB_INDEX > len(self.ide.recentTabs)-1:
            self.RECENT_TAB_INDEX = 0
        if len(self.ide.recentTabs) >1 or (self.ide.currentEditor().type == 'app' and len(self.ide.recentTabs) >0):
            self.TAB_MODE = 1
            self.selectEditorTab(self.ide.recentTabs[self.RECENT_TAB_INDEX*-1-1])

    def selectEditorTab(self,file_id):
        found_editor = None
        for w in range(self.tabs.count()):
            li = self.tabs.widget(w)
            # Look for file to highlight
            for i in range(li.count()):
                litm = li.itemWidget(li.item(i))
                if litm.id == file_id:
                    found_editor = litm
                    self.tabs.setCurrentIndex(w)
                    li.setCurrentRow(i)
                    
                    break
            if found_editor != None:
                break
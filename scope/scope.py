# --------------------------------------------------------------------------------
# Scope IDE
#
# developed by lucidlylogicole
#
# Scope is licensed under the GNU General Public License (GPL 3)
# --------------------------------------------------------------------------------

# VERSION
__version__ = '2.6.3'

import os,sys, json, codecs, time, importlib, subprocess

os.chdir(os.path.abspath(os.path.dirname(__file__)))
sys.path.append(os.path.abspath('../'))
sys.path.append(os.path.abspath('../site_pkg'))

SETTING_PATH = os.path.expanduser('~').replace('\\','/')+'/.scope_py3'

# Setup WebEngine Debugger
qt_debug_port = 0
if os.path.exists(SETTING_PATH+'/scope.json'):
    with open(SETTING_PATH+'/scope.json','r') as f:
        settings = json.loads(f.read())
    if 'qt_debug_port' in settings:
        qt_debug_port = settings['qt_debug_port']
if qt_debug_port > 0:
    import site_pkg.ports
    os.environ['QTWEBENGINE_REMOTE_DEBUGGING']=str(site_pkg.ports.get(qt_debug_port))

os.environ['QT_QUICK_BACKEND'] = 'software' # fix for intel driver issue
os.environ['QT_AUTO_SCREEN_SCALE_FACTOR'] = '1' # fix for screen scaling

from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw
try:
    from PyQt5 import QtWebEngineWidgets as qte
except:
    print('QtWebEngineWidgets is not installed!')

from .menus import *
import shutil,datetime,sys,traceback
from collections import OrderedDict

class Scope(qtw.QWidget):
    def __init__(self, parent=None, minimal=0):

        # Version
        self.version = __version__
        self.minimal = minimal
        
        # Setup UI
        qtw.QWidget.__init__(self, parent)
        from .scope_ui import Ui_Form
        self.ui = Ui_Form()
        self.ui.setupUi(self)
    
        # Hid For Now
        self.ui.le_cmd.hide()
    
    def load(self):
        #--- Paths
        rpath = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0].replace('\\','/')
        self.iconPath=rpath+'/style/img/'
        self.settingPath = SETTING_PATH
        self.pluginPath = rpath+'/plugins/'
        self.editorPath = rpath+'/plugins/'
        self.scopePath = rpath
        self.currentPath = os.path.expanduser('~')
        if os.name =='nt':
            self.pathPrefix="file:///"
        else:
            self.pathPrefix="file://"
        
        # File Variables
        self.fileOpenD = {} # keep track of open files and associated widgets
        self.fileD = {}     # keep track of all filenames and new files
        self.recentTabs = [] # Keep track of most recent tabs
        self.fileCount = -1
        self.ui.b_closetab.hide()
        self.saveEnabled = 1   # Enable saving to local file
        self.last_editable_file = None # Last file from the editor
        self.runCommands = {}
        self.previousEditorType = None
        
        # Workspace
        self.currentWorkspace = None
        self.workspaceCount = 0
        self.workspaces = {}
        
        #--- Editor Events
        class Events(qtc.QObject):
            editorAdded = qtc.pyqtSignal(qtw.QWidget)
            editorTabChanged = qtc.pyqtSignal(qtw.QWidget)
            editorSaved = qtc.pyqtSignal(qtw.QWidget)
            editorBeforeSave = qtc.pyqtSignal(qtw.QWidget)
            
            editorVisibleLinesChanged = qtc.pyqtSignal(qtw.QWidget,tuple)
            close=qtc.pyqtSignal()
            editorTabClosed = qtc.pyqtSignal(qtw.QWidget)
            fileOpened = qtc.pyqtSignal(qtw.QWidget)
            resized = qtc.pyqtSignal()
            
            settingsLoaded = qtc.pyqtSignal()
            
            themeChanged = qtc.pyqtSignal(str)
            
            # Workspace Events
            workspaceChanged = qtc.pyqtSignal(str)
            workspaceOpened = qtc.pyqtSignal(str)
            workspaceClosed = qtc.pyqtSignal(str)
            workspaceBeforeSave = qtc.pyqtSignal(str,dict)
            
        self.Events = Events()
        
        # Settings
        self.loadSettings()

        if self.settings['widgetstyle'] != 'None':
            qtw.QApplication.setStyle(self.settings['widgetstyle'])
        
        self.setAcceptDrops(1)

        # Style
        self.setTheme(self.settings['theme'])
        
        #--- Window Setup
        
        # Default zen mode
        self.fullscreen_mode = 0
        self.editor_fullmode = 0
        self.toolbar_mode = 1
        
        # Screen Setup
        screen = qtw.QApplication.desktop().screenNumber(qtw.QApplication.desktop().cursor().pos())
        coords= qtw.QApplication.desktop().screenGeometry(screen).getCoords() 
        if self.settings['window']['openMode']==1:
            # get margins from settings
            dl=self.settings['window']['margin']['left']
            dr=self.settings['window']['margin']['right']
            dt=self.settings['window']['margin']['top']
            db=self.settings['window']['margin']['bottom']
            
            # set up width and height
            ws=coords[2]-coords[0] # screen width
            hs=coords[3]-coords[1] # screen height
            wd = self.settings['window']['size']['width'].strip()
            hd = self.settings['window']['size']['height'].strip()
            if wd.endswith('%'):
                w=float(wd[:-1])/100.0*ws
            else:
                try:
                    w=int(wd.replace('px','').strip())
                except:
                    w=ws
            if hd.endswith('%'):
                h=float(hd[:-1])/100.0*hs
            else:
                try:
                    h=int(hd.replace('px','').strip())
                except:
                    h=hs
            # Set screen size
            self.setGeometry(coords[0]+dl,coords[1]+dt,(w-dl-dr),(h-dt-db))
            qtw.QApplication.processEvents()
        elif self.settings['window']['openMode']==2:
            # Fullscreen
            self.showMaximized()
            qtw.QApplication.processEvents()

        self.ui.b_back.hide()

        # Setup plugin views
        # Bottom
        h=self.settings['pluginBottom']['height']
        self.ui.split_bottom.setSizes([self.height()-h,h])
        if self.settings['pluginBottom']['visible']!=1:
            self.ui.sw_bottom.setHidden(1)
        
        # Left
        lw=self.settings['pluginLeft']['width']
        self.ui.split_left.setSizes([lw,self.width()-lw])
        if self.settings['pluginLeft']['visible']!=1:
            self.toggleLeftPlugin(show=0)
        
        # Right
        rw=self.settings['pluginRight']['width']
        if rw.endswith('%'):
            rw = float(rw[:-1])/100*(self.width()-lw)
        else:
            rw=int(rw)
        
        self.ui.split_right.setSizes([self.width()-rw-lw,rw])
        if self.settings['pluginRight']['visible']!=1:
            self.ui.tab_right.setVisible(0)

        # Tab Direction
        tabLocD = {'top':qtw.QTabWidget.North,'bottom':qtw.QTabWidget.South,'left':qtw.QTabWidget.West,'right':qtw.QTabWidget.East}
        self.ui.tab_right.setTabPosition(tabLocD[self.settings['pluginRight']['tabPosition']])

        #--- Signals
        self.ui.b_closetab.clicked.connect(lambda: self.closeTab())
##        self.ui.b_show_tabs.clicked.connect(self.toggleTabWindow)
        
        self.ui.b_new.clicked.connect(self.showNewMenu)
        self.ui.b_left_toggle.clicked.connect(self.toggleLeftPlugin)
        
        # Top Toolbar
        self.ui.l_title_prefix.linkActivated.connect(self.titleLinkClicked)
        self.ui.b_save.clicked.connect(self.saveClick)
        self.ui.fr_tab.mousePressEvent = self.tabClick
        self.ui.l_filename.mousePressEvent = self.filenameClick

##        self.ui.b_indent.clicked.connect(self.editorIndent)
##        self.ui.b_unindent.clicked.connect(self.editorUnindent)
##        self.ui.b_comment.clicked.connect(self.editorToggleComment)
        self.ui.b_color_picker.clicked.connect(self.colorPicker)

        self.ui.b_compile.clicked.connect(self.editorCompile)
        self.ui.b_run.clicked.connect(self.editorRun)
        
        self.ui.b_find.clicked.connect(self.editorFind)
        self.ui.le_goto.returnPressed.connect(self.editorGoto)

        self.ui.b_back.clicked.connect(self.backToEditor)
        
        self.ui.l_statusbar.linkActivated.connect(self.statusBarClick)
        
        self.ui.tab_right.currentChanged.connect(self.rightPluginChange)
        
        # Create Tab Window
        self.createTabWindow()
        
        #--- Key Shortcuts
        qtw.QShortcut(qtc.Qt.ALT+qtc.Qt.Key_E,self,self.editorSetFocus) # Editor Set Focus
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_E,self,self.editorToggleComment) #Toggle Comment
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_F,self.ui.sw_main,self.findFocus) # Find
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_G,self,self.gotoFocus) # Goto
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_M,self,self.ui.b_menu.click) # Menu
        qtw.QShortcut(qtc.Qt.ALT+qtc.Qt.Key_M,self,self.ui.b_menu.click) # Menu
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_N,self,self.ui.b_new.click) # New
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_O,self,self.openFile) # Open File Dialog
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_S,self,self.editorSave) # Save
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_W,self,self.editorWordWrap) # Toggle Wordwrap
##        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_P,self,self.editorInsertPrint) # Insert print
        qtw.QShortcut(qtc.Qt.ALT+qtc.Qt.Key_S,self,self.editorStats) # Editor Stats
        qtw.QShortcut(qtc.Qt.ALT+qtc.Qt.Key_C,self,self.colorPicker) # Color Picker
        
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_Tab,self,self.nextRecentTab) # Next Recent Tab
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_PageDown,self,self.nextTab) # Next Tab
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_PageUp,self,self.previousTab) # Previous Tab

        qtw.QShortcut(qtc.Qt.Key_F2,self,self.toggleLeftPlugin) # Toggle Left Plugin
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_F2,self,self.nextLeftPlugin) # show next left plugin
        
        qtw.QShortcut(qtc.Qt.Key_F5,self,self.editorRun) # Run
        qtw.QShortcut(qtc.Qt.SHIFT+qtc.Qt.Key_F5,self,self.editorRunSetCmd) # Run - just set the command
        qtw.QShortcut(qtc.Qt.Key_F6,self,self.editorCompile) # Compile
        qtw.QShortcut(qtc.Qt.SHIFT+qtc.Qt.Key_F6,self,self.editorCompileSetCmd) # Compile - just set the command
        qtw.QShortcut(qtc.Qt.Key_F9,self,self.toggleRightPluginFull) # Expand Right plugin
        qtw.QShortcut(qtc.Qt.Key_F3,self,self.toggleRightPlugin) # Toggle RIght Plugins
        
        qtw.QShortcut(qtc.Qt.Key_F4,self,self.toggleBottomPlugin) # Hide Bottom Tab
        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_F4,self,self.nextBottomPlugin) # Show next bottom tab
        qtw.QShortcut(qtc.Qt.Key_F1,self,self.toggleTabWindow) # Show Tabbar
##        qtw.QShortcut(qtc.Qt.CTRL + qtc.Qt.Key_QuoteLeft,self,self.toggleTabWindow) # Show Tabbar
##        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_1,self,self.toggleTabWindow) # Show Tabbar
        qtw.QShortcut(qtc.Qt.Key_F10,self,self.toggleFullEditor) # Editor full screen, but keep tabs
        qtw.QShortcut(qtc.Qt.Key_F11,self,self.toggleFullscreen) # Fullscreen Zen
        
        qtw.QShortcut(qtc.Qt.ALT+qtc.Qt.Key_T,self,self.toggleTheme) # Toggle Theme
        
        #--- Load Editors (and languages)
        self.editorD = {}
        self.Editors = {}
        for e in self.settings['activeEditors']:
            try:
                mod = importlib.import_module('plugins.'+e+'.scope_editor')
                self.Editors[e] = mod.Editor(self)
                ld = self.Editors[e].getLang()
            except:
                qtw.QMessageBox.warning(self,'Failed to Load Editor','The editor, '+e+' failed to load')
                ld = []
                print(sys.exc_info()[1])
            
            if ld != []:
                self.editorD[e] = ld

                for l in ld:
                    if l not in self.settings['extensions']:
                        self.settings['extensions'][l]=l

        #--- Add Menus
        # New Button Menu
        self.newMenu = NewMenu(self)

        # Workspace Button Menu
        self.workspaceMenu = WorkspaceMenu(self)

        # add Main Button to tabbar
        self.editorMenu = EditorMenu(self)
        self.ui.b_menu.setMenu(self.editorMenu)
        
        #--- Plugins
        self.HomeWidget = None
        self.pluginD = {}
        plugin_errors = []
        self.prevPlugin=1
        if self.minimal:
            #---Minimal Settings
            self.ui.fr_bottom.hide()
            self.ui.sw_bottom.hide()
            self.ui.fr_left.hide()
            self.ui.b_run.hide()
            self.ui.b_compile.hide()
            self.leftPluginVisible = False
        else:
            self.leftPluginVisible = self.settings['pluginLeft']['visible']
            # Plugin tab bar
            self.ui.tabbar_bottom = qtw.QTabBar()
            self.ui.tabbar_bottom.setObjectName('tabbar_bottom')
            self.ui.fr_bottom.layout().addWidget(self.ui.tabbar_bottom)
            self.ui.tabbar_bottom.currentChanged.connect(self.pluginBottomChange)
            self.ui.tabbar_bottom.setShape(1)
            self.ui.tabbar_bottom.setExpanding(0)
            # Add down arrow
            self.ui.tabbar_bottom.addTab(qtg.QIcon(self.iconPath+'tri_down.png'),'')
            lbl = qtw.QLabel('  ')
            self.ui.tabbar_bottom.setTabButton(0,qtw.QTabBar.LeftSide,lbl) # add label to center button
            self.ui.tabbar_bottom.setTabToolTip(0,'Hide bottom plugins')
            
            # Add Plugins
            curdir = os.path.abspath('.')
            
            for plug in self.settings['activePlugins']:
                try:
                    self.loadPlugin(plug)
                except Exception as e:
                    plugin_errors.append(plug)
                    print('ERROR LOADING PLUGIN: %s' %plug)
                    print("".join(traceback.format_exc()), file=sys.stderr)
            self.ui.l_statusbar.setText('')

            os.chdir(curdir)
            self.ui.tabbar_bottom.setCurrentIndex(0)
        
        # Add Right Plugin Hide Button
        self.ui.b_right_tab_hide = qtw.QPushButton()
        self.ui.b_right_tab_hide.setIcon(qtg.QIcon(self.iconPath+'tri_right.png'))
        self.ui.b_right_tab_hide.clicked.connect(self.toggleRightPlugin)
        self.ui.b_right_tab_hide.setToolTip('Hide right plugins')
        self.ui.b_right_tab_hide.setProperty('class','toolbar')
        self.ui.tab_right.setCornerWidget(self.ui.b_right_tab_hide)
        
        #--- Other Setup

        # Open file if in sys arg
        if len(sys.argv)>1:
            if os.path.exists(sys.argv[1]) and os.path.isfile(sys.argv[1]):
                self.openFile(sys.argv[1])
        
        self.setFocus()
        
        # Add Start/Home
        qtw.QApplication.processEvents()
        self.showHome()
        
        # Setup File Checking
        self.fileCheck_ignore = [] # List of file_ids to ignore checking
        if self.settings['checkFileChanges']:
            self.fileCheckTimer = qtc.QTimer()
            self.fileCheckTimer.setInterval(1000*self.settings['checkFileChanges'])
            self.fileCheckTimer.timeout.connect(self.checkFileChanges)
            self.fileCheckTimer.start()

        if plugin_errors:
            self.ui.l_statusbar.setText('<font color=red>Plugins failed to load:</font> '+','.join(plugin_errors))
        
        #---Startup
##        if os.path.exists(os.path.join(self.settingPath,'startup.py')):
####            qtw.QApplication.processEvents()
##            sys.path.append(self.settingPath)
####            curdir = os.path.abspath('.')
####            os.chdir(self.settingPath)
##            time.sleep(2)
##            print(self.settingPath)
##            import startup
##            startup.run(self)
##            print('ran startup')
        
    #---Events
    def closeEvent(self,event):
        cancelled = 0
        # Close all workspaces
        for wksp in list(self.workspaces.keys()):
            ok = self.workspaceClose(wksp)
            if not ok:
                cancelled=1
                break

        if cancelled:
            event.ignore()
        else:
            # Save Settings
            self.saveSettings()
            
            # Other Events
            self.Events.close.emit()

    def dropEvent(self,event):
        handled=False
        if event.mimeData().urls():
            for f in event.mimeData().urls():
                self.openFile(str(f.toLocalFile()))
            handled=True

        if not handled:
            qtw.QWidget.dropEvent(self,event)

    def dragEvent(self,event):
        event.accept()
    
    def dragEnterEvent(self,event):
        event.accept()
    
    def resizeEvent(self,event):
        self.Events.resized.emit()

    def visibleLinesChanged(self,wdg,lines):
        self.Events.editorVisibleLinesChanged.emit(wdg,lines)

    def tabClick(self,event):
##        print event
        self.toggleTabWindow()
    
    def filenameClick(self,event):
        self.toggleTabWindow()

    #---Fullscreen Modes
    def toggleFullEditor(self,fullscreen=0):
        self.editor_fullmode = not self.editor_fullmode
        zen=self.editor_fullmode
        self.ui.l_statusbar.setVisible(not zen)

        self.toggleLeftPlugin(show=not zen)

        if fullscreen == 1 or self.fullscreen_mode:
            self.ui.sw_bottom.setVisible(not zen)
            self.ui.fr_bottom.setVisible(not zen)
            self.ui.fr_topbar.setVisible(not zen)
            self.ui.fr_leftbar.setVisible(not zen)
            
        if zen:
            self.pluginBottomChange(0)
        else:
            self.pluginBottomChange(self.ui.tabbar_bottom.currentIndex())
        
        if self.editor_fullmode:
            self.editorMenu.fullEditorAction.setText('Exit Full Editor Mode')
        else:
            self.editorMenu.fullEditorAction.setText('Full Editor Mode (F10)')
            
    def toggleFullscreen(self):
        self.fullscreen_mode = not self.fullscreen_mode
        if self.fullscreen_mode:
            self.showFullScreen()
            self.editor_fullmode=0
            self.toggleFullEditor(fullscreen=1)
            
            self.editorMenu.fullScreenAction.setText('Exit Full Screen Mode (F11)')
        else:
            self.editorMenu.fullScreenAction.setText('Full Screen (F11)')
            self.showNormal()
            if self.editor_fullmode:
                self.toggleFullEditor(fullscreen=1)
    
    def toggleAppScreen(self):
        if self.currentEditor().type == 'app':
            self.toggleLeftPlugin(0)
        elif self.leftPluginVisible:
            self.toggleLeftPlugin(1)
    
    def toggleToolbar(self):
        self.toolbar_mode = not self.toolbar_mode
        self.ui.fr_toolbar.setVisible(self.toolbar_mode)
    
    #---File
    def openFile(self,filename=None,editor=None,file_id=None,path=None):
        txt = None
        run_cmd = None
        
        # Check if file is open
        if file_id != None:
            if file_id in self.fileOpenD:
                self.changeTab(file_id)
                return 1
            else:
                filename = self.fileD[file_id]['filename']
                if editor == None:
                    editor = self.fileD[file_id]['editor']

        if not filename:
            # New File - Ask for filename if not specified
            opth = self.currentPath
            if path != None: opth = path
            filename = qtw.QFileDialog.getOpenFileName(self,"Select File",opth," (*.*)")[0]
            if filename=='':
                filename = None
            else:
                filename = str(filename)
        
        if filename != None:
            if os.path.isfile(filename):
                filename = filename.replace('\\','/')
                # Check if file already open
                file_open = self.isFileOpen(filename)
                if file_open !=-1:
                    self.changeTab(file_open)
                    return 1
                else:
                    ext = os.path.splitext(str(filename))[1][1:]
                    lang = ext

                    if ext in self.settings['extensions']:
                        lang = self.settings['extensions'][ext]
                    
                    # Load Image
                    if lang in ['png','jpg','bmp','gif','ico']:
                        w = self.ui.sw_main.width()-20
                        if os.name =='nt':
                            pfx="file:///"
                        else:
                            pfx="file://"
                        pth=pfx+os.path.abspath(filename).replace('\\','/')
                        html='<img src="'+pth+'" style="max-width:'+str(w)+';">'
                        self.webviewPreview(html,filename)
                    else:
                        title = os.path.basename(filename)

                        try:
                            with codecs.open(filename,'r','utf-8') as f:
                                txt = f.read()
                            
                        except:
                            qtw.QMessageBox.warning(self,'Error Opening File','The following file could not be read.  Make sure it is ascii or utf-8 encoded<br><br>'+filename)
                            txt = None
                        
                        if txt != None:
                            # Create Widget
                            wdg = self.addEditorWidget(lang,title,str(filename),editor=editor) #,code=txt)
                            wdg.setEnabled(0)
                            self.ui.l_statusbar.setText('Loading...'+os.path.basename(filename))
                            qtw.QApplication.processEvents()
                            wdg.setText(txt)
                            wdg.lastText = txt
                            wdg.tabTitle = self.getTitle(filename)

                            wdg.modTime = os.path.getmtime(filename)
                            wdg.setEnabled(1)
                            self.ui.l_statusbar.setText('')
                            
                            # Update Run Command
                            if filename in self.workspaces[self.currentWorkspace]['runCommands']:
                                wdg.runCommand = self.workspaces[self.currentWorkspace]['runCommands'][filename]
                            
                            # Update tab text (needed for now after first creation)
                            wdg.titleSuffix=''
                            self.setTitle(wdg)
                            for t in self.fileD[wdg.id]['tabs']:
                                t.setTitle(wdg.tabTitle+wdg.titleSuffix)
                            
                            self.Events.fileOpened.emit(wdg)
                            self.editorSetFocus()

        return txt != None

    def getFileId(self,filename):
        file_id = None
        if filename == None:
            pass
        elif os.name =='nt': # Check lower path name for windows
            for f in self.fileD:
                if self.fileD[f]['filename'] != None and os.path.abspath(self.fileD[f]['filename']).lower() == os.path.abspath(filename).lower():
                    file_id = f
        else:
            for f in self.fileD:
                if self.fileD[f]['filename'] != None and os.path.abspath(self.fileD[f]['filename']) == os.path.abspath(filename):
                    file_id = f
        
        if file_id == None:
            self.fileCount += 1
            file_id = self.fileCount
            self.fileD[file_id]={'filename':filename,'tabs':[],'editor':None, 'run_cmd':None}
        
        return file_id
                
    def isFileOpen(self,filename):
        # Check if file open and return tab index
        fileopen = -1
        for file_id in self.fileOpenD:
            wdg = self.fileOpenD[file_id]
            if wdg.filename != None and os.path.abspath(wdg.filename).lower() == os.path.abspath(filename).lower():
                
                # Check if in current workspace and add tab if not
                #  REMOVE THIS SECTION eventually
                wksp_tab = self.TabWindow.tabs.currentWidget()
                if wksp_tab != None:
                    if not file_id in wksp_tab.tabD:
                        print('isfileOpen: adding widget function moved')

                fileopen = file_id
                break
        return fileopen
    
    def getTitle(self,filename):
        title = os.path.basename(filename)
        if self.settings['tabWindow']['view_folder']:
            title = os.path.split(os.path.dirname(filename))[1]+'/'+title
        elif filename.endswith('.py') and title=='__init__.py':
            title = os.path.split(os.path.dirname(filename))[1]+'/init'
        elif filename.endswith('.js') and title=='main.js':
            title = os.path.split(os.path.dirname(filename))[1]+'/main.js'
        elif title.startswith('index.'):
            title = os.path.split(os.path.dirname(filename))[1]+'/'+title
        return title
    
    def setTitle(self,widget):
        w = self.ui.fr_tab.width()
        self.ui.l_filename.setText(widget.title+widget.titleSuffix)
        
        # Show Full Path
        if self.settings['showPath']:
            if widget.type == 'file':
                if self.theme == 'light':
                    clr = 'rgb(30,52,85)'
                else:
                    clr = 'rgb(194,232,249)'

                w = w-70-self.ui.l_filename.fontMetrics().width(widget.title+widget.titleSuffix)
                title = self.ui.l_filename.fontMetrics().elidedText(os.path.split(widget.filename)[0]+'/',0,w)
                parts = title.split('/')
                # Build Title path links
                prev_part = ''
                initial_part = ''
                if parts[0] == '':
                    prev_part = '/'
                    initial_part = '/'
                txt = ''+initial_part
                for pt in parts:
                    if pt != '':
                        lnk = prev_part
                        if prev_part != initial_part:
                            lnk += '/'
                        lnk += pt
                        txt += '<a href="'+lnk+'" style="color:'+clr+';text-decoration:none;">'+pt+'</a>'
                        txt += '/'
                        prev_part = lnk
                        
                self.ui.l_title_prefix.setText(txt)
            else:
                self.ui.l_title_prefix.setText('')
    
        # Set Tooltip
        if widget.filename != None:
            self.ui.fr_tab.setToolTip(widget.filename)
        elif widget.type == 'file_new':
            self.ui.fr_tab.setToolTip('New File (unsaved)')
        else:
            self.ui.fr_tab.setToolTip(widget.title)
    
    def getIconPath(self,filename):
        ext = os.path.splitext(str(filename))[1][1:]
        lang = ext

        if ext in self.settings['extensions']:
            lang = self.settings['extensions'][ext]
        ipth = self.iconPath+'files/source.png'
        fipth = self.iconPath+'files/'+str(lang)+'.png'
        if os.path.exists(fipth):
            ipth = fipth
        elif filename != None:
            ext = os.path.splitext(filename)[1][1:]
            if os.path.exists(self.iconPath+'files/'+ext+'.png'):
                ipth = self.iconPath+'files/'+ext+'.png'
        
        return ipth

    def titleLinkClicked(self,url):
        pth = url
        if os.path.isdir(pth):
            if not pth.endswith('/'): pth += '/'
            try:
                dirlist = sorted(os.listdir(os.path.abspath(pth)))
            except:
                dirlist = []
            
            menu = qtw.QMenu('menu')
            menu.addAction(qtg.QIcon(self.iconPath+'folder_go.png'),'View &Folder')
            menu.addAction(qtg.QIcon(self.iconPath+'file_open.png'),'&Open File')
            menu.addSeparator()
            
            # Add Files
            if dirlist != []:
                for f in sorted(dirlist, key=lambda s: s.lower()):
                    ext = os.path.splitext(f)[1][1:]
                    if not os.path.isdir(pth+f) and not f.startswith('.') and ext in self.settings['extensions']:
                        ipth = self.getIconPath(pth+f)
                        menu.addAction(qtg.QIcon(ipth),f)
            
            resp = menu.exec_(qtw.QApplication.desktop().cursor().pos())
            if resp != None:
                txt = str(resp.text())
                if txt == '&Open File':
                    self.openFile(path=pth)
                elif txt == 'View &Folder':
                    self.openFileExternal(pth)
                else:
                    self.openFile(os.path.join(pth,txt))


    #---Main/Editor Widget
    def addMainWidget(self,wdg,title,typ='file',**kargs):
        '''Add a widget to the main stackedwidget (where the editors go)'''
        
        # Setup Default Settings
        wdgD = {
            'filename':None,
            'icon':None,
            'lastText':'',
            'lang':None,
            'viewOnly':1,
            'editor':None,
            'pluginRightVisible':0,
            'typ':typ,
            'runCommand':None,
        }
        
        for ky in wdgD:
            if ky in kargs:
                wdgD[ky]=kargs[ky]
        
        # Create new widget parameters
        file_id = self.getFileId(wdgD['filename'])
        sw_ind = self.ui.sw_main.count()
        
        wdg.filename = wdgD['filename']
        wdg.lastText=''
        wdg.title = title
        wdg.tabTitle = title
        wdg.titleSuffix = ''
        wdg.id = file_id
        wdg.lang = wdgD['lang']
        wdg.viewOnly = wdgD['viewOnly']
        wdg.pluginEditor = wdgD['editor']
        wdg.runCommand = self.fileD[file_id]['run_cmd']
        wdg.pluginRightVisible=wdgD['pluginRightVisible']
        wdg.modTime = None
        wdg.icon = wdgD['icon']
        wdg.type = wdgD['typ']
        
        self.fileOpenD[file_id]=wdg
        
        self.ui.sw_main.insertWidget(sw_ind,wdg)
        self.changeTab(file_id)
        
        return file_id
    
    def addEditorWidget(self,lang=None,title='New',filename=None,editor=None,code=''):
        wdg = None
        typ = 'file'
        
        if filename == None and title=='New': 
            title = 'New '+lang
            typ = 'file_new'
        
        if not editor in self.Editors: # If editor not available - then find one
            editor = None
        
        editor_lang = lang
        
        if editor == None:
            if lang in self.settings['prog_lang']:
                editor = self.settings['prog_lang'][lang]['editor']
                # Check if other language specified in settings
                if 'editor_lang' in self.settings['prog_lang'][lang]:
                    editor_lang = self.settings['prog_lang'][lang]['editor_lang']
            if editor == None:
                if lang == 'webview':
                    editor = 'webview'
                elif lang == 'settings':
                    editor = 'settings'
                else:
                    editor = self.settings['prog_lang']['default']['editor']
                    
                    if lang not in self.editorD[editor]:
                        for e in self.editorD:
                            if lang in self.editorD[e]:
                                editor = e
                                break
        
        # Check for editor_lang specified in settings
        if lang in self.settings['prog_lang']:
            # Check if other language specified in settings
            if 'editor_lang' in self.settings['prog_lang'][lang]:
                editor_lang = self.settings['prog_lang'][lang]['editor_lang']
        
        kargs = {
            'lang':editor_lang,
            'filename':filename,
        }
        
        # Load Editors
        if editor == 'webview':
            from plugins.webview import webview
            wdg = webview.WebView(self)
        else:
            if not editor in self.settings['activeEditors']:
                editor = self.settings['prog_lang']['default']['editor']
            wdg = self.Editors[editor].getWidget(**kargs)
        
        file_id=self.addMainWidget(wdg,title,filename=filename,viewOnly=0,lang=lang,typ=typ)
        wdg.pluginEditor = editor
        wdg.pluginRightVisible=0

        # Add Icon
        ipth = self.iconPath+'/files/source.png'
        icn = qtg.QPixmap(ipth)
        ipth = self.iconPath+'files/'+str(lang)+'.png'
        if os.path.exists(ipth):
            icn = qtg.QPixmap(ipth)
        elif filename != None:
            ext = os.path.splitext(filename)[1][1:]
            if os.path.exists(self.iconPath+'files/'+ext+'.png'):
                icn = qtg.QPixmap(self.iconPath+'files/'+ext+'.png')
        elif os.path.exists(self.editorPath+editor+'/'+editor+'.png'):
            ipth = self.iconPath+'/files/source.png'
        self.ui.b_tabicon.setIcon(qtg.QIcon(icn))
        wdg.icon = qtg.QIcon(icn)
        wdg.pic = icn
        
        # Insert tab in workspace
        new_workspace = self.currentWorkspace == None
        self.addWorkspaceEditor(wdg.id,wdg.title,wdg.filename)

        # Set wordwrap if in settings
        qtw.QApplication.processEvents()
        if lang in self.settings['prog_lang']:
            if 'wordwrap' in self.settings['prog_lang'][lang]:
                wdg.wordwrapmode = int(self.settings['prog_lang'][lang]['wordwrap'])
                self.editorWordWrap()
        
            # Set Autocomplete Toggle
            if 'autocomplete' in self.settings['prog_lang'][lang]:
                wdg.autocomplete = int(self.settings['prog_lang'][lang]['autocomplete'])
                if 'toggleAutoComplete' in dir(wdg):
                    wdg.toggleAutoComplete()
        
        # Emit Signals
        self.Events.editorAdded.emit(wdg)

        if 'editorTextChanged' in dir(wdg):
            wdg.Events.editorChanged.connect(self.editorTextChanged)
        if 'visibleLinesChanged' in dir(wdg) and self.settings['visibleLineTracking']:
            wdg.Events.visibleLinesChanged.connect(self.visibleLinesChanged)

        if new_workspace:
            self.changeTab(file_id)

        return wdg

    def currentEditor(self):
        return self.ui.sw_main.currentWidget()
    
    def getEditorWidget(self,file_id):
        if file_id in self.fileOpenD:
            return self.fileOpenD[file_id]
        else:
            return None

    def changeTab(self,file_id=None):
        if file_id == None:
            wdg = self.currentEditor()
            if wdg != None:
                file_id = wdg.id
        
        self.ui.l_statusbar.setText('')

        if file_id in self.fileOpenD:
            wdg = self.fileOpenD[file_id]
            self.ui.sw_main.setCurrentWidget(wdg)
            
            # Update Titlebar
            self.setTitle(wdg)
            if wdg.filename != None:
                self.ui.fr_tab.setToolTip(wdg.filename)
            elif wdg.type == 'file_new':
                self.ui.fr_tab.setToolTip('New File (unsaved)')
            else:
                self.ui.fr_tab.setToolTip(wdg.title)
            try:
                self.ui.b_tabicon.setIcon(wdg.icon)
            except:
                pass
                
            # Show/Hide top bar items
            lang = wdg.lang
            self.ui.b_closetab.show()
            if self.toolbar_mode:
                self.ui.fr_toolbar.show()
            self.ui.fr_topleft.show()
            self.ui.b_back.hide()
            
            # Check workspace 
            wksps = self.getFileWorkspace(file_id)
            if not self.currentWorkspace in wksps and wksps != []:
                self.workspaceChange(wksps[0])
            
        else:
            # Blank file
            wdg = None
            lang = None
            
            self.ui.b_closetab.hide()
            self.ui.fr_toolbar.hide()
            self.ui.fr_topleft.hide()
            self.ui.l_filename.setText('')
            self.ui.l_title_prefix.setText('')
            self.ui.b_tabicon.setIcon(qtg.QIcon())

        # Enable Run
##        run_enabled=0
##        if lang in self.settings['run']:
##            run_enabled = lang in self.settings['run']
##        self.ui.b_run.setEnabled(run_enabled)
        self.ui.b_run.setEnabled(1)

        # Enable Compile
##        compile_enabled=0
##        if lang in self.settings['compile']:
##            compile_enabled = lang in self.settings['compile']
##        self.ui.b_compile.setEnabled(compile_enabled)
        self.ui.b_compile.setEnabled(1)

        # Disable buttons based on function availability
        btnD = [
##            ['indent',self.ui.b_indent],
##            ['unindent',self.ui.b_unindent],
            ['find',self.ui.fr_find],
##            ['toggleComment',self.ui.b_comment],
            ['getText',self.ui.b_save],
            ['toggleWordWrap',self.editorMenu.wordwrapAction],
            ['toggleWhitespace',self.editorMenu.whitespaceAction],
            ['getText',self.editorMenu.statsAction],
        ]
        for btn in btnD:
            btn[1].setEnabled(btn[0] in dir(wdg))
        
        # Enable/Disable Editor Menu
        try:
            self.editorMenu.menuSaveAction.setEnabled('getText' in dir(wdg))
            self.editorMenu.menuSaveAsAction.setEnabled('getText' in dir(wdg))
            self.editorMenu.editMenu.setEnabled('getText' in dir(wdg))
            self.editorMenu.runMenu.setEnabled('getText' in dir(wdg))
            self.editorMenu.viewMenu.setEnabled('getText' in dir(wdg))
            self.editorMenu.closeFileAction.setEnabled('getText' in dir(wdg))
        except:
            pass
        
        
        pluginRightVisible=0 # Hide Right side (default)
        if wdg != None:
            # Hide left bar if app
            if wdg.type == 'app':
                # Apps
                if wdg.title == 'Home':
                    # Show filebrowser with home
                    if 'filebrowser' in self.pluginD:
                        self.pluginD['filebrowser'].toggle()
                else:
                    self.toggleLeftPlugin(show=0)
                self.ui.tab_right.setVisible(0)
                    
            else:
                # Update recent tabs list
                if wdg.id in self.recentTabs:
                    self.recentTabs.remove(wdg.id)
                self.recentTabs.append(wdg.id)
                
                self.last_editable_file = wdg.id

                if self.previousEditorType == 'app':
                    # Editors
                    self.toggleLeftPlugin(show=1)
                
                # Toggle Right Plugin
                pluginRightVisible = wdg.pluginRightVisible
                if self.ui.tab_right.currentWidget() != None:
                    if self.ui.tab_right.currentWidget().pinned:
                        pluginRightVisible=self.ui.tab_right.isVisible()
                
                if pluginRightVisible != self.ui.tab_right.isVisible():
                    self.toggleRightPlugin()
        
            if wdg.viewOnly:
                # View widget only - hide toolbars
                self.ui.fr_toolbar.hide()
                self.ui.fr_topleft.hide()
                self.ui.b_closetab.hide()
                # Back if current tab
                if self.last_editable_file != None and self.last_editable_file in self.fileOpenD:
                    self.ui.b_back.show()
            
            self.previousEditorType = wdg.type
            self.Events.editorTabChanged.emit(wdg)
        else:
            # Show filebrowser if blank
            if 'filebrowser' in self.pluginD:
                self.pluginD['filebrowser'].toggle()
        
        self.editorSetFocus()
        
        # Fix to stop webengine from freezing (https://bugreports.qt.io/browse/QTBUG-68566)
        qtw.QApplication.processEvents()
        cur_size = self.currentEditor().size()
        self.currentEditor().resize(cur_size.width()+1,cur_size.height()+1)
        self.currentEditor().resize(cur_size.width(),cur_size.height())
    
    def closeTab(self,file_id=-1,remove_from_workspace=1):
        ok = 1
        
        if file_id == -1 or file_id == False:
            if self.currentEditor() != None:
                file_id = self.currentEditor().id
        
        if file_id >-1 and file_id in self.fileOpenD:
            wdg = self.fileOpenD[file_id]
            filename = wdg.filename
            
            # Check Save
            if 'getText' in dir(wdg):
                ok = self.checkSave(wdg)
                    
            if ok:
                # Emit close signal
                self.Events.editorTabClosed.emit(wdg)
                
                self.fileOpenD.pop(file_id)

                # Remove Widget
                self.ui.sw_main.removeWidget(wdg)
                
                if file_id in self.recentTabs:
                    self.recentTabs.remove(file_id)
                
                wdg.deleteLater()
                del wdg
        
        # Remove from workspace tabs
        if ok and remove_from_workspace:
            for t in self.fileD[file_id]['tabs'][:]:
                t.close()
                self.fileD[file_id]['tabs'].remove(t)
            
            # Go to Last Editor Widget
            self.changeTab()
        
        return ok

    def editorTextChanged(self,wdg=None,check_text=1,changed=None):
        # Indicate if text changed
        if wdg == None:
            wdg = self.currentEditor()
        upd_title = 0
        sfx = ''
        if changed != None:
            if changed:
                sfx = '*'
        else:
            try:
                if wdg.lastText != wdg.getText():
                    sfx = '*'
            except:
                self.ui.l_statusbar.setText('Error: text changed signal')

        if sfx != wdg.titleSuffix:
            upd_title = 1
            wdg.titleSuffix = sfx
            if wdg == self.currentEditor():
                self.setTitle(wdg)
                
##        try:
##            if not check_text:
##                sfx = '*'
##            elif wdg.lastText != wdg.getText():
##                sfx = '*'
##            else:
##                sfx = ''
##            if sfx != wdg.titleSuffix:
##                upd_title = 1
##                wdg.titleSuffix = sfx
##                self.setTitle(wdg)
##        except:
##            self.ui.l_statusbar.setText('Error: text changed signal')
        
            # Update tab display title
            if upd_title and wdg.id != None:
                for t in self.fileD[wdg.id]['tabs']:
                    try:
                        t.setTitle(wdg.tabTitle+wdg.titleSuffix)
                    except:
                        print('error setting title',wdg.tabTitle+wdg.titleSuffix)

    def checkSave(self,wdg):
        ok = 0
        if wdg.viewOnly:
            ok = 1
        else:
            if 'getText' in dir(wdg):
                try:
                    txt = wdg.getText()
                    if wdg.lastText != txt:
                        resp = qtw.QMessageBox.warning(self,'Save File',"Do you want to save the file <b>"+wdg.title+"</b>?",qtw.QMessageBox.Yes | qtw.QMessageBox.No | qtw.QMessageBox.Cancel)
                        if resp == qtw.QMessageBox.Yes:
                            self.editorSave(wdg)
                            ok =1
                        elif resp == qtw.QMessageBox.No:
                            ok =1
                    else:
                        ok =1 
                except:
                    ok=1
                    self.ui.l_statusbar.setText('Error: checking save')
            else: # Ignore checksave if no getText
                ok=1
        return ok

    #---Editor Tools
    def saveClick(self,checked):
        self.editorSave()
    
    def editorSave(self,wdg=None):
        if wdg == None:
            wdg = self.currentEditor()
        
        if wdg != None:
            
            if wdg.type == 'app':
                # Check if app has save
                if 'appSave' in dir(wdg):
                    wdg.appSave()
            else:
                # File Save
                if wdg.filename != None:
                    filename = wdg.filename
                else:
                    fileext = ''
                    # Don't show extensions for now (not working in Linux)
                    if os.name =='nt':
                        for e in self.settings['extensions']:
                            if self.settings['extensions'][e]==wdg.lang:
                                fileext+=wdg.lang+' (*.'+e+");;"
                    fileext += "All (*.*)"
                    
                    filename = qtw.QFileDialog.getSaveFileName(self,"Save Code",self.currentPath,fileext)[0]
                    if filename=='':
                        filename=None
                    else:
                        wdg.filename = os.path.abspath(str(filename))
                        wdg.title = os.path.basename(wdg.filename)
                        wdg.tabTitle = self.getTitle(wdg.filename)
                        self.ui.l_filename.setToolTip(wdg.filename)
                        wdg.type = 'file'

                if filename != None:
                    self.Events.editorBeforeSave.emit(wdg)
                    try:
                        txt = wdg.getText()
                        if txt == None:
                            qtw.QMessageBox.warning(self,'Error Saving','Scope could not save the file')
                            return
                        if self.saveEnabled:
                            with codecs.open(wdg.filename,'w','utf8') as f:
                                f.write(txt)
                        wdg.lastText = txt
                        wdg.modTime = os.path.getmtime(filename)
                        self.ui.l_statusbar.setText('Saved: '+wdg.title)#+' at '+datetime.datetime.now().ctime(),3000)
                        wdg.titleSuffix = ''
                        self.setTitle(wdg)
                        
                        # Save Signal
                        self.Events.editorSaved.emit(wdg)
                        
                    except:
                        qtw.QMessageBox.warning(self,'Error Saving','There was an error saving this file.  Make sure it is not open elsewhere and you have write access to it.  You may want to copy the text, paste it in another editor to not lose your work.<br><br><b>Error:</b><br>'+str(sys.exc_info()[1]))
                        self.ui.l_statusbar.setText('Error Saving: '+filename)
                        txt = ''
                
                    # Update tabs
                    for t in self.fileD[wdg.id]['tabs']:
                        t.setTitle(wdg.tabTitle+wdg.titleSuffix)
                        t.item.setToolTip(wdg.filename)
                    
                    # Check for syntax Errors
                    if wdg.lang == 'python' and self.settings['syntaxChecking']:
                        status = self.pySyntaxCheck(txt)
                        if status != '':
                            self.ui.l_statusbar.setText('Saved: '+wdg.title+' &nbsp; &nbsp; '+status)
            
    def editorSaveAs(self):
        wdg = self.currentEditor()
        txt = wdg.getText()
        fileext = ''
        if wdg.filename != None:
            pth = wdg.filename
        else:
            pth = self.currentPath
        
        if os.name =='nt':
            for e in self.settings['extensions']:
                if self.settings['extensions'][e]==wdg.lang:
                    fileext+=wdg.lang+' (*.'+e+");;"
        fileext += "All (*.*)"
        
        filename = qtw.QFileDialog.getSaveFileName(self,"Save Code",pth,fileext)[0]
        if filename!='':
            filename = os.path.abspath(str(filename))
            with codecs.open(filename,'w','utf-8') as f:
                f.write(txt)
            self.openFile(filename,editor=wdg.pluginEditor)
                
    def editorFind(self):
        wdg = self.currentEditor()
        if 'find' in dir(wdg):
            txt = self.ui.le_find.text()
            wdg.find(txt)

    def editorRun(self,wdg=None,justset=0):
        if wdg == None or isinstance(wdg,bool):
            wdg = self.currentEditor()
        
        runcmd = None
        if wdg.runCommand != None:
            runcmd = wdg.runCommand
        elif wdg.lang in self.settings['run']:
            runcmd = self.settings['run'][wdg.lang]['cmd']
        
        if runcmd != None:
            runstart = runcmd.split(' ')[0]
            if runstart == 'preview':
                # Preview
                if 'preview' in self.pluginD:
                    self.pluginD['preview'].widget.previewRun(wdg,justset=justset)
                    self.toggleRightPlugin(show=1)
                    self.ui.tab_right.setCurrentWidget(self.pluginD['preview'].widget)
                else:
                    qtw.QMessageBox.warning(self,'No Preview Plugin','The Preview plugin is not available.<br><br>Please add it to the activePlugins settings')
            elif runstart in self.runCommands:
                # Custom plugin command
                self.runCommands[runstart](wdg)
            else:
                ok = self.checkSave(wdg)
                filename = str(wdg.filename)
                if ok and filename != 'None':
                    # Otherwise run in output
                    self.pluginD['output'].widget.runProcess(runcmd,wdg,justset=justset)
        else:
            # just set
            self.pluginD['output'].widget.runProcess('',wdg,justset=1)
    
    def editorRunSetCmd(self,wdg=None):
        self.editorRun(wdg,justset=1)
    
    def editorCompile(self,wdg=None,justset=0):
        if wdg == None or isinstance(wdg,bool):
            wdg = self.currentEditor()
        if wdg.lang in self.settings['compile']:
            # Run Command
            ok = self.checkSave(wdg)
            filename = str(wdg.filename)
            if ok and filename != 'None':
                # Otherwise run in output
                runD = self.settings['compile'][wdg.lang]
                nwfile = None
                if runD['new_ext'] != None:
                    nwfile = ''.join(filename.split('.')[:-1]) + '.'+runD['new_ext']
                self.pluginD['output'].widget.runProcess(runD['cmd'],wdg,typ='compile',args={'new_file':nwfile},justset=justset)
        else:
            # just set
            self.pluginD['output'].widget.runProcess('',wdg,typ='compile',justset=1)
        
    def editorCompileSetCmd(self,wdg=None):
        self.editorCompile(wdg=None,justset=1)

    def editorToggleComment(self):
        wdg = self.currentEditor()
        if 'toggleComment' in dir(wdg):
            wdg.toggleComment()
            
    def editorToggleWhitespace(self):
        wdg = self.currentEditor()
        if 'toggleWhitespace' in dir(wdg):
            wdg.toggleWhitespace()
            
    def editorIndent(self):
        wdg = self.currentEditor()
        if 'indent' in dir(wdg):
            wdg.indent()

    def editorUnindent(self):
        wdg = self.currentEditor()
        if 'unindent' in dir(wdg):
            wdg.unindent()

    def editorWordWrap(self):
        wdg = self.currentEditor()
        if 'toggleWordWrap' in dir(wdg):
            wdg.toggleWordWrap()

    def editorGoto(self,line=None):
        wdg = self.currentEditor()
        if 'gotoLine' in dir(wdg):
            if line == None:
                txt = self.ui.le_goto.text()
                try:
                    line = int(str(txt))
                except:
                    pass
            try:
                wdg.gotoLine(line-1)
            except:
                pass
    
    def editorPrint(self):
        wdg = self.currentEditor()
        from PyQt5.QtPrintSupport import QPrinter, QPrintDialog
        printer = QPrinter()
        printer.setOutputFileName(wdg.filename+'.pdf')
        pdlg = QPrintDialog(printer,self)
        resp = pdlg.exec_()
        if resp:
            if 'getText' in dir(wdg):
                txt = wdg.getText()
                te = qtw.QTextEdit()
                
                te.setFontFamily('Courier')
                te.setFontPointSize(8)
                te.setText(txt)
            elif 'print_' in dir(wdg):
                te = wdg
            try:
                te.print_(printer)
            except:
                qtw.QMessageBox.warning(self,'Cannot Print','There was an error printing this document')
    
    def editorStats(self, check_words=True, pycheck=True):
        wdg = self.currentEditor()
        stxt = ''
        
        # Get Cursor Position
        if 'getCursorPosition' in dir(wdg):
            row,col = wdg.getCursorPosition()
            stxt = 'Row: %d &nbsp; &nbsp; Column: %d &nbsp; &nbsp; ' %(row+1,col+1)
        
        # Get number of words and lines in document
        txt = None
        if 'getText' in dir(wdg):
            txt = wdg.getText()
        if check_words and txt != None:
            
            lines = len(txt.splitlines())
            words = len(txt.split())
            
            stxt += 'Lines: %d &nbsp; &nbsp; Words: %d &nbsp; &nbsp; ' %(lines,words)
        
        status = '<span style="color:rgb(150,150,150);">'+stxt+'</span>'
        
        if txt != None and pycheck and self.settings['syntaxChecking']:
            status = self.pySyntaxCheck() + status
        
        self.ui.l_statusbar.setText(status)
    
    def editorSetFocus(self):
        if self.currentEditor() != None:
            self.currentEditor().setFocus()
    
    def colorPicker(self):
        dclr = [255,255,255,255]
        dclr = [255,255,255,255]
        # Check for selected text
        if 'getSelectedText' in dir(self.currentEditor()):
            txt=self.currentEditor().getSelectedText()
            # Check for rgb color
            sel_clrs = txt.split(',')
            if len(sel_clrs) >2:
                try:
                    sclr=[]
                    for c in sel_clrs:
                        sclr.append(int(str(c)))
                    if len(sel_clrs) == 3: sclr.append(255)
                    dclr = sclr
                except:
                    print('invalid color',sel_clrs)
        
        clrdlg = qtw.QColorDialog(self)
        clr=clrdlg.getColor(qtg.QColor(dclr[0],dclr[1],dclr[2],dclr[3]),self,'Select color',qtw.QColorDialog.ShowAlphaChannel)
        if clr.isValid():
            r,g,b,a = clr.getRgb()
            atxt=apfx=''
            if a < 255:
                atxt=',%0.2g' %(a/255.0)
            txt = '%d,%d,%d%s' %(r,g,b,atxt)
            if 'insertText' in dir(self.currentEditor()):
                self.currentEditor().insertText(txt)
            self.editorSetFocus()

##    def editorInsertPrint(self):
##        '''insert print() or console.log'''
##        wdg = self.currentEditor()
##        if wdg.lang == 'python':
##            wdg.insertText('print(')
##        elif wdg.lang in ['javascript','html','handlebars']:
##            wdg.insertText('console.log(')
    
    #---Left Toolbar
    def showNewMenu(self):
        # Show menu to the right of the toolbar
        s = self.ui.b_new.size()
        bpos = self.ui.b_new.mapToGlobal(qtc.QPoint(s.width(),0))
        self.newMenu.exec_(bpos)

    def showWorkspaceMenu(self):
        bpos = self.cursor().pos()
        self.workspaceMenu.exec_(bpos)

    def addLeftBarButton(self,icon,tooltip='',click_function=None,loc='app'):
        '''Add a button on the left toolbar'''
        btn = qtw.QPushButton()
        btn.setIcon(icon)
        btn.setIconSize(qtc.QSize(32,32))
        btn.setToolTip(tooltip)
        btn.setAutoFillBackground(True)
        
        if loc == 'leftab':
            layout = self.ui.fr_left_tab_btn.layout()
            layout.insertWidget(layout.count(),btn)
        else:
            layout = self.ui.fr_left_apps.layout()
            layout.insertWidget(layout.count()-1,btn)
        
        if click_function != None:
            btn.clicked.connect(click_function)
        
        return btn

    #---Plugins
    def loadPlugin(self,plug):
        curdir = os.path.abspath('.')

        if not os.path.exists(self.pluginPath+plug):
            raise Exception('Plugin not found')
        else:
            
            pmod = importlib.import_module('plugins.'+plug+'.scope_plugin')
            Plugin = pmod.Plugin(self)
            
            os.chdir(self.pluginPath+plug)
        
            if 'load' in dir(Plugin):
                Plugin.load()
        
            title = Plugin.title
            loc = Plugin.location
            icn = qtg.QIcon()
            if os.path.exists(self.pluginPath+plug+'/icon.png'):
                icn = qtg.QIcon(self.pluginPath+plug+'/icon.png')

            # Check settings for location
            if plug in self.settings['plugins'] and 'location' in self.settings['plugins'][plug]:
                loc = self.settings['plugins'][plug]['location']
            
            if loc in ['left','right','bottom'] and Plugin.widget == None:
                pluginWidget = Plugin.loadWidget()
                
            tabtext = title
            
            if loc == 'left':
                if not self.settings['pluginLeft']['showTabText']: tabtext=''
                ti = self.ui.tab_left.addWidget(pluginWidget)
                # Add left button
                icon = qtg.QIcon(self.pluginPath+plug+'/icon.png')
                Plugin.btn = self.addLeftBarButton(icon,tooltip=Plugin.title,loc='leftab')
                Plugin.btn.clicked.connect(pluginWidget.togglePlugin)
                Plugin.btn.setCheckable(1)
                Plugin.btn.setAutoExclusive(1)
                
                # Context menu
                if hasattr(Plugin,'contextMenu'):
                    Plugin.btn.contextMenuEvent = Plugin.contextMenu
                
            elif loc=='right':
                if not self.settings['pluginRight']['showTabText']: tabtext=''
                if not hasattr(pluginWidget,'pinned'):
                    pluginWidget.pinned = 0
                ti = self.ui.tab_right.addTab(pluginWidget,icn,tabtext)
                self.ui.tab_right.setTabToolTip(ti,title)
            elif loc == 'bottom':
                if not self.settings['pluginBottom']['showTabText']: tabtext=''
                self.ui.sw_bottom.addWidget(pluginWidget)
                ti=self.ui.tabbar_bottom.addTab(icn,tabtext)
                self.ui.tabbar_bottom.setTabToolTip(ti,title)

            self.pluginD[plug]=Plugin
            os.chdir(curdir)

    def loadPluginSettings(self,plugin):
        ppth = os.path.join(self.pluginPath,plugin+'/plugin.json')
        pD = {}
        if os.path.exists(ppth):

            with open(ppth,'r') as f:
                plugD = json.load(f,object_pairs_hook=OrderedDict)['settings']
            
            pD = OrderedDict()
            for ky in plugD:
                pD[ky] = plugD[ky]
            
                if plugin in self.settings['plugins']:
                    if ky in self.settings['plugins'][plugin]:
                        pD[ky] = self.settings['plugins'][plugin][ky]
        
        return pD
        
    #---   Left Plugins
    def toggleLeftPlugin(self,nothing=0,show=None,check_button=1):
        if show != None:
            self.leftPluginVisible = show
        else:
            self.leftPluginVisible = self.ui.fr_left.isHidden()
        self.ui.fr_left.setVisible(self.leftPluginVisible)
        if check_button:
            if self.leftPluginVisible:
                self.ui.fr_left_tab_btn.layout().itemAt(self.ui.tab_left.currentIndex()+1).widget().setChecked(1)
            else:
                self.ui.b_left_toggle.setChecked(1)
    
    def showLeftPlugin(self,plug):
        if self.ui.fr_left.isHidden():
            self.toggleLeftPlugin(show=1,check_button=0)
        i=self.ui.tab_left.indexOf(self.pluginD[plug].widget)
        self.ui.tab_left.setCurrentIndex(i)
        self.pluginD[plug].btn.setChecked(1)
    
    def nextLeftPlugin(self):
        self.ui.fr_left.setVisible(1)
        i=self.ui.tab_left.currentIndex()
        i+=1
        if i >= self.ui.tab_left.count():
            i=0
        self.ui.tab_left.setCurrentIndex(i)
        self.ui.fr_left_tab_btn.layout().itemAt(i+1).widget().setChecked(1)
        
    #---   Bottom Plugins
    def pluginBottomChange(self,ind):
        if self.ui.sw_bottom.currentIndex() != 0:
            self.prevPlugin = self.ui.sw_bottom.currentIndex()
        self.ui.sw_bottom.setCurrentIndex(ind)
        self.ui.sw_bottom.setHidden(not ind)
    
    def toggleBottomPlugin(self):
        if self.ui.tabbar_bottom.currentIndex() == 0:
            if self.prevPlugin==0:self.prevPlugin=1
            self.ui.tabbar_bottom.setCurrentIndex(self.prevPlugin)
            if self.ui.fr_bottom.isHidden():
                self.ui.fr_bottom.setHidden(0)
        else:
            self.ui.tabbar_bottom.setCurrentIndex(0)
    
    def nextBottomPlugin(self):
        i=self.ui.tabbar_bottom.currentIndex()
        i+=1
        if i>= self.ui.tabbar_bottom.count():
            i=0
        self.ui.tabbar_bottom.setCurrentIndex(i)

    #---   Right Plugins
    def toggleRightPlugin(self,*args,**kargs):
        show = self.ui.tab_right.isHidden()
        if 'show' in kargs:
            show = kargs['show']
        
        self.ui.tab_right.setVisible(show)

        if self.ui.tab_right.isVisible() and self.ui.sw_main.isHidden():
            self.toggleRightPluginFull()
        
        # Update editor settings
        if self.currentEditor() != None and not self.ui.tab_right.currentWidget().pinned:
            self.currentEditor().pluginRightVisible=self.ui.tab_right.isVisible()
        
        if 'leftToggle' in self.settings['pluginRight']:
            if self.settings['pluginRight']['leftToggle']:
                self.toggleLeftPlugin(show=self.ui.tab_right.isHidden())

    def toggleRightPluginFull(self):
        if self.ui.tab_right.isVisible():
            self.ui.sw_main.setVisible(self.ui.sw_main.isHidden())

    def rightPluginChange(self, ind):
        # Update editor settings
        if self.currentEditor() != None and not self.ui.tab_right.currentWidget().pinned:
            self.currentEditor().pluginRightVisible=self.ui.tab_right.isVisible()

    #---Webview Preview
    def webviewPreview(self,html,burl=None):
        openfile = self.isFileOpen('preview')
        if openfile==-1:
            wdg = self.addEditorWidget('webview','Preview','preview')
            wdg.page().setLinkDelegationPolicy(QtWebKit.QWebPage.DelegateAllLinks)
            wdg.pic = qtg.QPixmap(self.iconPath+'page_preview.png')
            wdg.icon = qtg.QIcon(wdg.pic)
            self.ui.b_tabicon.setIcon(wdg.icon)
    
        else:
            self.changeTab(openfile)
            qtw.QApplication.processEvents()
            wdg = self.currentEditor()
        
        if burl != None:
            if os.name =='nt':
                pfx="file:///"
            else:
                pfx="file://"
            burl = qtc.QUrl(pfx+os.path.abspath(os.path.dirname(burl)).replace('\\','/')+'/')

        wdg.setText(html,burl)
        wdg.viewOnly = 1
        wdg.modTime = None
    
    def showHome(self):
        if self.HomeWidget != None:
            self.HomeWidget.toggleHome()
    
    def showHelp(self):
        if self.HomeWidget != None:
            self.HomeWidget.toggleHome()
            self.HomeWidget.webview.page().runJavaScript("document.getElementById('l_show_about').click()")
    
    #---Tab Window / Window Switcher
    def toggleTabWindow(self):
        if self.TabWindow.hidden_mode: self.TabWindow.toggle()
    
    def createTabWindow(self):
        from . import tabWindow
        hidden_mode=1
        self.TabWindow = tabWindow.TabWindow(parent=self,hidden_mode=hidden_mode)
        if not hidden_mode:
            self.ui.fr_switcher.layout().addWidget(self.TabWindow.tabs,0,0,0,7)
            self.ui.fr_switcher.setMinimumHeight(90)
            self.ui.fr_switcher.setMaximumHeight(90)
            self.TabWindow.tabs.setTabPosition(qtw.QTabWidget.North)
        else:
            self.ui.fr_switcher.hide()
            
    #---Shortcuts
    def findFocus(self):
        if self.ui.fr_topbar.isHidden():
            self.ui.fr_topbar.setVisible(1)
        self.ui.le_find.setFocus()
        self.ui.le_find.selectAll()
        
    def gotoFocus(self):
        if self.ui.fr_topbar.isHidden():
            self.ui.fr_topbar.setVisible(1)
        self.ui.le_goto.setFocus()
        self.ui.le_goto.selectAll()
        
    def nextRecentTab(self):
        self.TabWindow.toggle()
        self.TabWindow.nextRecentTab()
    
    def nextTab(self):
        self.TabWindow.toggle()
        self.TabWindow.nextTab()
        
    def previousTab(self):
        self.TabWindow.toggle()
        self.TabWindow.previousTab()
    
    #---Workspace
    def workspaceSave(self,wksp=None):
        if wksp == None:
            wksp = self.currentWorkspace
        # Save the current Workspace
        if wksp != None:
            if wksp not in self.workspaces or self.workspaces[wksp]['type'] != 'blank':
                if not os.path.exists(self.settingPath+'/workspaces'):
                    os.mkdir(self.settingPath+'/workspaces')
                
                wD={'files':[],'basefolder':None,'lastOpenFile':None,'runCommands':{}}
                # Save workspace files
                if wksp in self.workspaces:
                    
                    if 'runCommands' in self.workspaces[wksp]:
                        wD['runCommands'] = self.workspaces[wksp]['runCommands']
                    
                    li_wdg = self.workspaces[wksp]['widget']
                    for i in range(li_wdg.count()):
                        itm = li_wdg.itemWidget(li_wdg.item(i))
                        file_id = itm.id
                        
                        fD = {}
                        if file_id in self.fileOpenD:
                            wdg = self.fileOpenD[file_id]
                            editor = wdg.pluginEditor
                            run_command = wdg.runCommand
                        else:
                            editor = itm.pluginEditor
                            run_command = itm.run_command
                        
                        fD = {'filename':itm.filename,'editor':editor}
                        if run_command != None:
##                            fD['run_cmd'] = run_command
                            wD['runCommands'][itm.filename] = run_command
                        
                        if itm.filename != None:
                            wD['files'].append(fD)

                    if self.currentEditor() != None and not self.currentEditor().viewOnly:
                        wD['lastOpenFile']=self.currentEditor().filename
                
                    # Save workspace dir
                    wD['basefolder']=self.workspaces[wksp]['basefolder']
                
                # Emit before workspace save signal
                self.Events.workspaceBeforeSave.emit(wksp,wD)
                
                # Save settings
                with open(self.settingPath+'/workspaces/'+wksp,'w') as f:
                    f.write(json.dumps(wD))
    
    def workspaceOpen(self,wksp,show_tabs=1):
        # Load workspace
        if wksp in self.workspaces: # and wksp != None:
            self.Events.workspaceChanged.emit(wksp)
            self.TabWindow.show()
            self.TabWindow.tabs.setCurrentWidget(self.workspaces[wksp]['widget'])
            self.currentPath = self.workspaces[wksp]['basefolder']
        else:
            self.workspaceCount +=1
            
            wtype = 'workspace'
            # Create Blank Workspace
            if wksp == None:
                wtype='blank'
                wksp = 'workspace '+str(self.workspaceCount)
                self.workspaces[wksp]={'files':[],'basefolder':None,'lastOpenFile':None}
            else:
                # Open existing workspace settings
                with open(self.settingPath+'/workspaces/'+wksp,'r') as f:
                    wD = json.loads(f.read())
                self.workspaces[wksp]=wD.copy()
            
            self.currentWorkspace=wksp
            wD = self.workspaces[wksp]

            # Add workspace to tab window
            wD['widget'] = self.TabWindow.addWorkspace(wksp)
            wD['type'] = wtype
            
            # Load Files
            last_file = None
            last_editor = None
            last_fid = None
            if 'lastOpenFile' in wD:
                last_file = wD['lastOpenFile']
            if not 'runCommands' in wD:
                wD['runCommands']={}
            for f in wD['files']:
                if f not in [None,'None','']:
                    if type(f) == type({}):
                        fid = self.getFileId(f['filename'])
                        self.fileD[fid]['editor'] = f['editor']
                        run_cmd = None
                        if f['filename'] in wD['runCommands']:
                            run_cmd =  wD['runCommands'][f['filename']]
                        # Temporarily load old run cmd location (versions 1.6.5 and before)  ------>
                        #---todo: remove run_cmd eventually
                        elif 'run_cmd' in f:
                            run_cmd = f['run_cmd']
                        # -----<
                            
                        self.fileD[fid]['run_cmd'] = run_cmd
                        self.addWorkspaceEditor(fid,self.getTitle(f['filename']),f['filename'],f['editor'])
                        
                        if last_file == None:
                            last_file = f['filename']
                        elif last_file == f['filename']:
                            last_editor = f['editor']
                            last_fid = fid
            
            # Current Directory
            self.currentPath = wD['basefolder']
            
            # Goto lastopen file
            if last_file != None:
                self.openFile(last_file,editor=last_editor)
                # Set Current Tab active without opening
##                self.TabWindow.selectEditorTab(last_fid)
            
            if show_tabs:
                self.toggleTabWindow()

            self.Events.workspaceOpened.emit(wksp)
    
        self.workspaceMenu.saveWact.setDisabled(0)
        self.workspaceMenu.renameWact.setDisabled(0)
        self.workspaceMenu.closeWact.setDisabled(0)
    
    def workspaceChange(self, wksp, change_tab=1):
        if wksp == None:
            self.currentWorkspace = None
            self.Events.workspaceChanged.emit('None')
            self.setWindowTitle('Scope')
        else:
            self.currentWorkspace = wksp
            self.currentPath = self.workspaces[wksp]['basefolder']
            # set current file to current file in workspace
            if 'widget' in self.workspaces[wksp] and change_tab:
                wwdg = self.workspaces[wksp]['widget']
                if self.TabWindow.tabs.currentWidget() != wwdg:
                    self.TabWindow.tabs.setCurrentWidget(wwdg)

            self.Events.workspaceChanged.emit(self.currentWorkspace)
            self.setWindowTitle('Scope | '+self.currentWorkspace)
    
    def addWorkspaceEditor(self,file_id,title,filename,editor=''):
        if self.currentWorkspace == None:
            self.workspaceOpen(None,show_tabs=0)
        tab = self.TabWindow.tabs.currentWidget().addEditorTab(file_id,title,filename,editor)
        if not tab in self.fileD[file_id]['tabs']:
            self.fileD[file_id]['tabs'].append(tab)
        if tab.run_command == None:
            tab.run_command = self.fileD[file_id]['run_cmd']
    
    def workspaceNew(self):
        # New Workspace
        resp,ok = qtw.QInputDialog.getText(self,'New Workspace','<b>Enter Workspace Name</b><br><font style="color:#bbb">(leave blank for a temporary workspace)</font>')
        if ok:
            if resp == '':
                self.workspaceOpen(None,show_tabs=1)
            else:
                self.workspaceSave(str(resp))
                self.workspaceOpen(str(resp))
                self.workspaceMenu.loadMenu()
                
                return str(resp)
    
    def workspaceRename(self,wksp=None):
        if os.path.exists(self.settingPath+'/workspaces'):
            if wksp == None:
                resp,ok = qtw.QInputDialog.getItem(self,'Rename Workspace','Select the workspace to rename',qtc.QStringList(sorted(os.listdir(self.settingPath+'/workspaces'))),editable=0)
                if ok: wksp = str(resp)
            if wksp != None:
                owskp=wksp
                pth = self.settingPath+'/workspaces/'+owskp
                resp,ok = qtw.QInputDialog.getText(self,'Rename Workspace','Enter Workspace Name',qtw.QLineEdit.Normal,str(owskp))
                if ok and not resp == '':
                    # save and close workspace if open
                    if owskp in self.workspaces:
                        self.workspaceClose(owskp)
                    npth = self.settingPath+'/workspaces/'+str(resp)
                    os.rename(pth,npth)
                    self.workspaceMenu.loadMenu()
        else:
            qtw.QMessageBox.warning(self,'No Workspaces','There are no workspaces to rename')
    
    def workspaceDelete(self,wksp=None):
        if os.path.exists(self.settingPath+'/workspaces'):
            ok = 0
            if wksp == None:
                resp,ok = qtw.QInputDialog.getItem(self,'Delete Workspace','Select the workspace to delete',qtc.QStringList(sorted(os.listdir(self.settingPath+'/workspaces'))),editable=0)
                wksp = str(resp)
            else:
                resp = qtw.QMessageBox.warning(self,'Delete Workspace','Do you want to delete the workspace?<br><br>This will only delete the workspace settings in Scope and not actually delete any files.',qtw.QMessageBox.Yes | qtw.QMessageBox.No)
                if resp == qtw.QMessageBox.Yes:
                    ok = 1
                
            if ok:
                if wksp in self.workspaces:
                    self.workspaceClose(wksp)
                os.remove(self.settingPath+'/workspaces/'+wksp)
            
            # Update Menu
            self.workspaceMenu.loadMenu()
            
        else:
            qtw.QMessageBox.warning(self,'No Workspaces','There are no workspaces to delete')
    
    def workspaceClose(self,wksp=None,askSave=0,openStart=0):
        wk_ok = 1
        if wksp == None:
            wksp = self.currentWorkspace
        
        # Save current workspace
        fl_ok = 1
        if wksp != None:
            self.workspaceSave()
            
            # Close 
            wksp_tabs = list(self.workspaces[wksp]['widget'].tabD.keys())
            for fid in wksp_tabs[:]:
                if fid in self.fileOpenD.keys():
                    fl_ok = self.closeTab(fid,remove_from_workspace=0)
                    if not fl_ok:
                        break
                
        # Close open files
        if wk_ok:
            pass
            #---todo: Check if not in other workspace and keep open
        
        ok=fl_ok*wk_ok
        
        if ok:
            self.Events.workspaceClosed.emit(wksp)
            self.workspaces.pop(str(wksp))
            if len(self.workspaces) == 0:
                self.workspaceMenu.saveWact.setDisabled(1)
                self.workspaceMenu.closeWact.setDisabled(1)
            
        return ok

    def getFileWorkspace(self,file_id):
        wksps = []
        
        for wk in self.workspaces:
            if 'widget' in self.workspaces[wk]:
                if  file_id in self.workspaces[wk]['widget'].tabD:
                    wksps.append(wk)
        return wksps

    #---Settings
    def loadSettings(self):
        # Create settings directory
        if not os.path.exists(self.settingPath):
            os.mkdir(self.settingPath)
        
        if not os.path.exists(self.settingPath+'/scope.json'):
            import shutil
            shutil.copyfile(os.path.abspath(os.path.dirname(__file__))+'/default_settings.json',self.settingPath+'/scope.json')
        
        self.settings_filename = self.settingPath+'/scope.json'
        dflt_path = os.path.abspath(os.path.abspath(os.path.dirname(__file__))+'/default_settings.json')
        
        # Default Settings
        with open(dflt_path,'r') as setf:
            config = json.load(setf)
        
        mysettings={}
        try:
            with open(self.settings_filename,'r') as setf:
                mysettings = json.load(setf)
        except:
            err = str(sys.exc_info()[1])
            qtw.QMessageBox.warning(self,'Settings Load Failed','There is something wrong with the settings file and it failed to load.<br><br>Using default settings<br><br><i>Compare your settings with the scope/default_settings.json</i><br><br><b>Error:</b>'+err)
        
        self.settings = config.copy()
        self.settings.update(mysettings)

        # Configure Settings
        self.settings['run']={}
        self.settings['compile']={}
        for l in self.settings['prog_lang']:
            ok = 1
            #~ self.settings['run_preview'][l]=0
            # Remove default languages if not in user config
            
            if 'prog_lang' in self.settings:
                if l not in self.settings['prog_lang']:
                    self.settings['prog_lang'].pop(l)
                    ok = 0
                    
            if ok:
                # Make sure editor in settings
                if not 'editor' in self.settings['prog_lang'][l]:
                    self.settings['prog_lang'][l]['editor']=None
            
                # add run to settings
                if 'run' in self.settings['prog_lang'][l]:
                    self.settings['run'][l]={'cmd':self.settings['prog_lang'][l]['run']}
                
                # add compile to settings
                if 'compile' in self.settings['prog_lang'][l]:
                    self.settings['compile'][l]={'cmd':self.settings['prog_lang'][l]['compile']['cmd'],'new_ext':None}
                    if 'new_ext' in self.settings['prog_lang'][l]['compile']:
                        self.settings['compile'][l]['new_ext'] = self.settings['prog_lang'][l]['compile']['new_ext']
                # Add fave to settings by default
                if 'fave' not in self.settings['prog_lang'][l]:
                    self.settings['prog_lang'][l]['fave']=1
                else:
                    self.settings['prog_lang'][l]['fave']=int(self.settings['prog_lang'][l]['fave'])
        
        self.settings['checkFileChanges'] = int(self.settings['checkFileChanges'])
        
        self.Events.settingsLoaded.emit()
    
    def openSettings(self):
        self.pluginD['settings'].addSettingsWidget()

    def saveSettings(self):
        if self.fullscreen_mode:
            self.toggleFullscreen()
            qtw.QApplication.processEvents()
        
        # Save Workspace
        if self.currentWorkspace != None and self.settings['save_workspace_on_close']:
            self.workspaceSave()

    def setTheme(self,theme):
        self.theme = theme
        if theme == 'light':
            style_path = self.settings['lighttheme']
        else:
            style_path = self.settings['darktheme']
        if not os.path.exists(os.path.abspath(style_path)):
            style_path = 'style/dark.css'
            qtw.QMessageBox.warning(self,'Error Loading Style','The stylesheet is not a valid path:<br><br>'+os.path.abspath(style_path))
            
        with open(style_path,'r') as f:
            style = f.read()
        self.setStyleSheet(style)
        self.stylePath = os.path.abspath(style_path)
    
    def toggleTheme(self):
        if self.theme == 'dark':
            self.theme = 'light'
        else:
            self.theme = 'dark'
        self.setTheme(self.theme)
        self.Events.themeChanged.emit(self.theme)

    #---Other Functions
    def checkFileChanges(self,nothing_message=0):
            chngs = 0
            close_tabs = []
            for file_id in self.fileOpenD:
                wdg = self.fileOpenD[file_id]
                if wdg.filename != None and wdg.modTime != None:
                    if not os.path.exists(wdg.filename) and file_id not in self.fileCheck_ignore:
                        resp = qtw.QMessageBox.warning(self,'File Does not exist',str(wdg.filename)+' does not exist anymore.<br><<br>Do you want to keep the file open?',qtw.QMessageBox.Yes | qtw.QMessageBox.No)
                        if resp == qtw.QMessageBox.No:
                            close_tabs.append(file_id)
                        else:
                            self.fileCheck_ignore.append(file_id)
                        chngs = 1
                    elif os.path.exists(wdg.filename) and os.path.getmtime(wdg.filename) > wdg.modTime:
                        resp = qtw.QMessageBox.warning(self,'File Modified',str(wdg.filename)+' has been modified.<br><<br>Do you want to reload it?',qtw.QMessageBox.Yes | qtw.QMessageBox.No)
                        
                        chngs=1
                        if resp == qtw.QMessageBox.Yes:
                            qtw.QApplication.processEvents()
                            with codecs.open(wdg.filename,'r','utf-8') as f:
                                txt = f.read()
                            wdg.setText(txt)
                        wdg.modTime = os.path.getmtime(wdg.filename)
            
            if close_tabs != []:
                close_tabs.reverse()
                for i in close_tabs:
                    self.closeTab(i)
            if not chngs and nothing_message:
                qtw.QMessageBox.warning(self,'No Changes','No external changes to current open files were found')

    def openFileExternal(self,path=None,program=None):
        '''Open the path external to Scope with the OS default'''

        if path == None:
            wdg = self.currentEditor()
            if wdg != None:
                path = wdg.filename
        
        if path != None:
            # Check for url
            if path.startswith('http://') or path.startswith('https://'):
                import webbrowser
                webbrowser.open(path)
            else:
                if os.name=='nt':path = path.replace('/','\\')
                dpth = os.path.dirname(path)
##                if os.path.isdir(path) and program != None:
                if program != None:
                    # Use specified file browser
                    subprocess.Popen([program,path],cwd=dpth)
                else:
                    # use default filebrowser
    ##                curdir = os.path.abspath('.')
    ##                if os.path.isdir(os.path.dirname(path)):
    ##                    os.chdir(os.path.dirname(path))
                    if os.name == 'nt':
    ##                    subprocess.Popen(path,shell=True,cwd=dpth)
                        os.startfile(path)
                    elif os.name=='posix':
                        subprocess.Popen(['xdg-open', path],cwd=dpth)
                    elif os.name=='mac':
                        subprocess.Popen(['open', path],cwd=dpth)
    ##                os.chdir(curdir)

    def launchInConsole(self,cmd,path=None):
        if path == None:
            wksp_pth = None
            if self.currentWorkspace != None:
                wksp_pth = self.workspaces[self.currentWorkspace]['basefolder']
            else:
                try:
                    wksp_pth = self.settings['plugins']['filebrowser']['defaultPath']
                except:
                    pass
            
            if wksp_pth == None: 
                if self.currentEditor().filename != None:
                    wksp_pth = os.path.dirname(self.currentEditor().filename)
                    
            path = wksp_pth
        
##        if self.settings['console'] != '':
        console = self.settings['console'] 
        
        
        if os.name =='nt':
            if console == '': console = 'cmd'
            if path != None: path = '/K "cd /d '+path.replace('/','\\')+'"'
            os.system('start '+console+' '+cmd+' '+path)
        else:
            if console == '': console = 'gnome-terminal'
            if cmd !='': cmd = '--tab -e "'+cmd+'"'
            if path != None: path = '--working-directory='+path
            os.system(console + " "+path+' '+cmd)

    def backToEditor(self):
        '''Back to editor button (for no editor main widgets)'''
        if self.last_editable_file != None and self.last_editable_file in self.fileOpenD:
            self.changeTab(self.last_editable_file)
        else:
            self.toggleTabWindow()

    def pySyntaxCheck(self,txt=None):
        '''Use pyflakes to check for syntax errors'''
        wdg = self.currentEditor()
        if txt == None:
            txt = wdg.getText()
            
        status = ''
        if wdg.lang == 'python' and txt != None:
            from site_pkg.pyflakes import scope_reporter
            errors = scope_reporter.check_file(self,txt)
            if errors:
                status = '<span style="color:rgb(255,125,102);">Syntax Error on <a style="color:rgb(224,64,59)" href="goto:{0}">line: {0}</a> </span> &nbsp; &nbsp; '.format(errors)
        
        return status

    def statusBarClick(self,url):
        lnk = str(url).split(':')
        if lnk[0] == 'goto':
            self.editorGoto(int(lnk[1]))

def runui(open_file=None):
##    os.chdir(os.path.abspath(os.path.dirname(__file__)))
    app = qtw.QApplication(sys.argv)

    minimal = 0
    if 'minimal' in sys.argv:
        minimal = 1

    scopeApp = Scope(minimal=minimal)
    os.chdir('../')
    qtw.QApplication.processEvents()
    scopeApp.load()
    scopeApp.show()
    qtw.QApplication.processEvents()
    
    if qt_debug_port > 0:
        print('debugging on http://127.0.0.1:%s' %os.environ['QTWEBENGINE_REMOTE_DEBUGGING'])
    
    # Open File if exists
    if open_file != None:
        scopeApp.openFile(open_file)
    
    #---Startup
    if os.path.exists(os.path.join(scopeApp.settingPath,'startup.py')):
        sys.path.append(scopeApp.settingPath)
        import startup
        startup.run(scopeApp)

    sys.exit(app.exec_())

##if __name__ == "__main__":
##    runui(dev_mode=1)
